<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/account_po.inc");

include_once("pl/account_po.inc");

include_once("it/account_po.inc");

include_once("ca/account_po.inc");

include_once("pt/account_po.inc");

include_once("es/account_po.inc");

include_once("de/account_po.inc");

include_once("ru/account_po.inc");

?>