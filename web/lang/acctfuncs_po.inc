<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/acctfuncs_po.inc");

include_once("pl/acctfuncs_po.inc");

include_once("it/acctfuncs_po.inc");

include_once("ca/acctfuncs_po.inc");

include_once("pt/acctfuncs_po.inc");

include_once("es/acctfuncs_po.inc");

include_once("de/acctfuncs_po.inc");

include_once("ru/acctfuncs_po.inc");

include_once("fr/acctfuncs_po.inc");

?>