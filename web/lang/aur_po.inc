<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/aur_po.inc");

include_once("pl/aur_po.inc");

include_once("it/aur_po.inc");

include_once("ca/aur_po.inc");

include_once("pt/aur_po.inc");

include_once("es/aur_po.inc");

include_once("de/aur_po.inc");

include_once("ru/aur_po.inc");

include_once("fr/aur_po.inc");

?>