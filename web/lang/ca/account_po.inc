<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["Use this form to update your account."] = "Utilitzeu aquest formulari per editar el vostre compte.";

$_t["ca"]["Leave the password fields blank to keep your same password."] = "Deixeu els camps de contrasenya en blanc si voleu conservar-la.";

$_t["ca"]["You are not allowed to access this area."] = "No esteu autoritzat per a accedir a aquesta àrea.";

$_t["ca"]["Could not retrieve information for the specified user."] = "No s'ha pogut obtenir la informació de l'usuari especificat.";

$_t["ca"]["Use this form to search existing accounts."] = "Utilitzeu aquest formulari per a cercar comptes existents.";

$_t["ca"]["You do not have permission to edit this account."] = "No teniu permís per a editar aquest compte.";

$_t["ca"]["Use this form to create an account."] = "Utilitzeu aquest formulari per a crear un compte.";

$_t["ca"]["You must log in to view user information."] = "Heu d'identificar-vos per a veure la inforació de l'usuari.";

?>