<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["Missing a required field."] = "Manca un camp requerit.";

$_t["ca"]["Search'"] = "Cerca'";

$_t["ca"]["The account, %h%s%h, has been successfully created."] = "El compte, %h%s%h, s'ha creat satisfactòriament.";

$_t["ca"]["Error trying to modify account, %h%s%h: %s."] = "S'ha produït un error en intentar modificar el compte, %h%s%h: %s.";

$_t["ca"]["The email address is invalid."] = "L'adreça del correu-e no és vàlida.";

$_t["ca"]["Error trying to create account, %h%s%h: %s."] = "S'ha produït un error en intentar crear el compte, %h%s%h: %s.";

$_t["ca"]["The username, %h%s%h, is already in use."] = "El nom d'usuari, %h%s%h, està ja en ús.";

$_t["ca"]["Account Type"] = "Tipus de compte";

$_t["ca"]["The account, %h%s%h, has been successfully modified."] = "El compte, %h%s%h, s'ha modificat satisfactòriament.";

$_t["ca"]["Account Suspended"] = "El compte s'ha suspès";

$_t["ca"]["Status"] = "Estat";

$_t["ca"]["New Package Notify"] = "Notificació en nous paquets";

$_t["ca"]["IRC Nick"] = "Nom d'usuari IRC";

$_t["ca"]["Trusted user"] = "Usuari de Confiança";

$_t["ca"]["No results matched your search criteria."] = "No s'ha trobat cap coindidència amb els criteris de cerca.";

$_t["ca"]["Normal user"] = "Usuari normal";

$_t["ca"]["Never"] = "Mai";

$_t["ca"]["User"] = "Usuari";

$_t["ca"]["Active"] = "Actiu";

$_t["ca"]["Last Voted"] = "Últim vot";

$_t["ca"]["Real Name"] = "Nom real";

$_t["ca"]["Edit Account"] = "Edita compte";

$_t["ca"]["Password fields do not match."] = "Els camps de contrasenya no coincideixen.";

$_t["ca"]["Language"] = "Idioma";

$_t["ca"]["A Trusted User cannot assign Developer status."] = "Un Usuari de Confiança no pot assignar l'estat de desenvolupador.";

$_t["ca"]["The address, %h%s%h, is already in use."] = "L'adreça, %h%s%h, està ja en ús.";

$_t["ca"]["No more results to display."] = "No hi ha més resultats per mostrar.";

$_t["ca"]["Type"] = "Tipus";

$_t["ca"]["Click on the Home link above to login."] = "Feu clic en l'enllaç Inici de dalt per identificar-vos.";

$_t["ca"]["Sort by"] = "Ordenat per";

$_t["ca"]["Re-type password"] = "Escriu altre cop la contrasenya";

$_t["ca"]["Language is not currently supported."] = "L'idioma no està suportat actualment.";

$_t["ca"]["Any type"] = "Qualsevol tipus";

$_t["ca"]["Last vote"] = "Últim vot";

$_t["ca"]["Suspended"] = "Suspès";

$_t["ca"]["Trusted User"] = "Usuari de Confiança";

$_t["ca"]["Missing User ID"] = "Manca l'identificador de l'usuari";

$_t["ca"]["Developer"] = "Desenvolupador";

$_t["ca"]["View this user's packages"] = "Visualitza els paquets d'aquest usuari";

?>