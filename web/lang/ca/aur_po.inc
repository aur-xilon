<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["%s: %sAn ArchLinux project%s"] = "%s: %sUn projecte d'Archlinux%s";

$_t["ca"]["Logout"] = "Surt";

$_t["ca"]["Discussion"] = "Discussió";

$_t["ca"]["Bugs"] = "Errors";

$_t["ca"]["Accounts"] = "Compte";

$_t["ca"]["Home"] = "Inici";

$_t["ca"]["Packages"] = "Paquets";

?>
