<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["Reset"] = "Restaura";

$_t["ca"]["Username"] = "Nom d'usuari";

$_t["ca"]["Email Address"] = "Adreça de correu-e";

$_t["ca"]["Less"] = "Menys";

$_t["ca"]["Clear"] = "Neteja";

$_t["ca"]["required"] = "requerit";

$_t["ca"]["Update"] = "Actualitza";

$_t["ca"]["Submit"] = "Envia";

$_t["ca"]["Password"] = "Contrasenya";

$_t["ca"]["Create"] = "Crea";

$_t["ca"]["More"] = "Més";

?>