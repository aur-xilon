<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["Your session id is invalid."] = "El vostre identificador de sessió no és vàlid.";

$_t["ca"]["If this problem persists, please contact the site administrator."] = "Si el problema persisteix, si u plau contacteu amb l'administrador del lloc.";

?>