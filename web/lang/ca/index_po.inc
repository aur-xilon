<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["Statistics"] = "Estadístiques";

$_t["ca"]["Remember to vote for your favourite packages!"] = "Recordeu votar els vostres paquets preferits!";

$_t["ca"]["Error looking up username, %s."] = "S'ha produït un error en cercar l'usuari, %s.";

$_t["ca"]["Packages in unsupported"] = "Paquets a \"unsupported\"";

$_t["ca"]["The most popular packages will be provided as binary packages in [community]."] = "Els paquets més populars es distribuiran com a binaris a [community].";

$_t["ca"]["Trusted Users"] = "Usuaris de Confiança";

$_t["ca"]["You must supply a username."] = "Heu de proporcionar un nom d'usuari.";

$_t["ca"]["Packages added or updated in the past 7 days"] = "Paquets afegits o actualitzats en els últims 7 dies";

$_t["ca"]["Email discussion about the AUR takes place on the %sTUR Users List%s."] = "La discussió en correu-e sobre AUR és a la %sLlista d'Usuaris TUR%s.";

$_t["ca"]["Packages in unsupported and flagged as safe"] = "Paquets a \"unsupported\" i marcats com Segurs";

$_t["ca"]["Though we can't vouch for their contents, we provide a %hlist of user repositories%h for your convenience."] = "Encara que no pugam respondre als seus continguts, subministrem una %hllista de repositoris d'usuaris%h per a la vostra conveniència.";

$_t["ca"]["Packages in [community]"] = "Paquets a [community]";

$_t["ca"]["Recent Updates"] = "Actualitzacions recents";

$_t["ca"]["Your account has been suspended."] = "El vostre compte s'ha suspès.";

$_t["ca"]["Username:"] = "Nom d'usuari:";

$_t["ca"]["Error trying to generate session id."] = "S'ha produït un error en intentar generar un identificador de sessió.";

$_t["ca"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information."] = "Benvingut a l'AUR! Si us plau llegiu la %hGuia d'usuari de l'AUR%h i la %hGuia de TU de l'AUR%h per a més informació.";

$_t["ca"]["Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "Els PKGBUILDs enviats han de seguir els %hEstàndards d'empaquetament d'Arch%h sinó seran esborrats!";

$_t["ca"]["Login"] = "Entra";

$_t["ca"]["If you have feedback about the AUR, please leave it in %hFlyspray%h."] = "Si teniu cap comentari sobre l'AUR, si us plau deixeu-lo en el %hFlyspray%h.";

$_t["ca"]["You must supply a password."] = "Heu de subministrar una contrasenya.";

$_t["ca"]["Password:"] = "Contrasenya:";

$_t["ca"]["Registered Users"] = "Usuaris registrats";

$_t["ca"]["Logged-in as: %h%s%h"] = "Identificat com: %h%s%h";

$_t["ca"]["Incorrect password for username, %s."] = "Contrasenya incorrecta per a l'usuari, %s.";

?>
