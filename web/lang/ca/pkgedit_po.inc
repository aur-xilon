<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["Missing package ID."] = "Manca l'identificador del paquet.";

$_t["ca"]["Invalid category ID."] = "L'identificador de la categoria no és vàlid.";

$_t["ca"]["Enter your comment below."] = "Introduïu el vostre comentari a continuació.";

$_t["ca"]["You are not allowed to delete this comment."] = "No esteu autoritzat per esborrar aquest comentari.";

$_t["ca"]["Missing comment ID."] = "Manca l'identificador del comentari.";

$_t["ca"]["Package category updated."] = "S'ha actualitzat la categoria del paquet.";

$_t["ca"]["You must be logged in before you can edit package information."] = "Heu d'identificar-vos abans d'editar qualsevol informació de paquet.";

$_t["ca"]["Comment has been deleted."] = "S'ha esborrat el comentari.";

$_t["ca"]["You've found a bug if you see this...."] = "Heu trobat un error si veieu açò.";

$_t["ca"]["Comment has been added."] = "S'ha afegit el comentari.";

$_t["ca"]["Select new category"] = "Seleccioneu nova categoria";

?>