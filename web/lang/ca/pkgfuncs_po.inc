<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["Category"] = "Categoria";

$_t["ca"]["Votes"] = "Vots";

$_t["ca"]["Delete comment"] = "Esborra comentari";

$_t["ca"]["First Submitted"] = "Primer enviament";

$_t["ca"]["Tarball"] = "Arxiu TAR";

$_t["ca"]["Be careful! The above files may contain malicious code that can damage your system."] = "Aneu amb compte! Els fitxers poden contenir codi maliciós que pot danyar el vostre sistema.";

$_t["ca"]["Voted"] = "Votat";

$_t["ca"]["Location"] = "Localització";

$_t["ca"]["Flag Safe"] = "Marca com Segur";

$_t["ca"]["Go"] = "Vés";

$_t["ca"]["Unflag Out-of-date"] = "Desmarca No-Actualitzat";

$_t["ca"]["Go back to %hpackage details view%h."] = "Torna a la %hvista de detalls del paquet%h.";

$_t["ca"]["Error retrieving package details."] = "No s'han pogut obtenir els detalls del paquet.";

$_t["ca"]["Description"] = "Descripció";

$_t["ca"]["My Packages"] = "Els meus paquets";

$_t["ca"]["Safe"] = "Segur";

$_t["ca"]["Sort order"] = "Ordena en sentit";

$_t["ca"]["Ascending"] = "Ascendent";

$_t["ca"]["Keywords"] = "Paraules clau";

$_t["ca"]["No New Comment Notification"] = "Cap notificació de nous comentaris";

$_t["ca"]["Dependencies"] = "Dependències";

$_t["ca"]["Descending"] = "Descendent";

$_t["ca"]["Per page"] = "Per pàgina";

$_t["ca"]["Package Listing"] = "Llista de paquets";

$_t["ca"]["Package details could not be found."] = "No s'han pogut trobar els detalls del paquet.";

$_t["ca"]["Package Details"] = "Detalls del paquet";

$_t["ca"]["Error retrieving package list."] = "S'ha produït un error en obtenir la llista de paquets.";

$_t["ca"]["Files"] = "Fitxers";

$_t["ca"]["None"] = "Cap";

$_t["ca"]["Name"] = "Nom";

$_t["ca"]["Last Updated"] = "Última actualització";

$_t["ca"]["The above files have been verified (by %s) and are safe to use."] = "Els fitxers s'han comprovat (per %s) i són segurs.";

$_t["ca"]["Unflag Package Safe To Use"] = "Desmarca Paquet Segur";

$_t["ca"]["Go back to %hsearch results%h."] = "Torna a %hResultat de cerca%h.";

$_t["ca"]["Age"] = "Antiguitat";

$_t["ca"]["Comments"] = "Comentaris";

$_t["ca"]["O%hrphan"] = "O%hrfe";

$_t["ca"]["orphan"] = "orfe";

$_t["ca"]["Un-Vote"] = "Lleva vot";

$_t["ca"]["change category"] = "Canvia la categoria";

$_t["ca"]["UnNotify"] = "Lleva notificació";

$_t["ca"]["Delete Packages"] = "Esborra paquet";

$_t["ca"]["Maintainer"] = "Mantenidor";

$_t["ca"]["Add Comment"] = "Afegeix comentari";

$_t["ca"]["Comment by: %h%s%h on %h%s%h"] = "Comentari per: %h%s%h a %h%s%h";

$_t["ca"]["Flag Out-of-date"] = "Marca com No-Actualitzat";

$_t["ca"]["Manage"] = "Gestiona";

$_t["ca"]["Sort by"] = "Ordena per";

$_t["ca"]["Flag Package Safe To Use"] = "Marca com Segur";

$_t["ca"]["Actions"] = "Accions";

$_t["ca"]["Unflag Safe"] = "Desmarca Segur";

$_t["ca"]["Sources"] = "Fonts";

$_t["ca"]["Yes"] = "Sí";

$_t["ca"]["Search Criteria"] = "Criteri de cerca";

$_t["ca"]["Notify"] = "Notifica";

$_t["ca"]["O%hut-of-Date"] = "N%ho-Actualitzat";

$_t["ca"]["Vote"] = "Vota";

$_t["ca"]["Adopt Packages"] = "Apròpia paquets";

$_t["ca"]["New Comment Notification"] = "Notificació de nou comentari";

$_t["ca"]["Disown Packages"] = "Desapròpia paquets";

$_t["ca"]["Orphans"] = "Orfes";

$_t["ca"]["Any"] = "Cap";

$_t["ca"]["No packages matched your search criteria."] = "No s'ha trobat cap coincidència amb el teu criteri de cerca.";

$_t["ca"]["Search by"] = "Cerca per";

?>
