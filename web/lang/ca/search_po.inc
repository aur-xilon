<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["None of the selected packages could be deleted."] = "Cap dels paquets seleccionats s'han pogut esborrar.";

$_t["ca"]["Your votes have been removed from the selected packages."] = "Els vostres vots s'han suprimit dels paquets seleccionats.";

$_t["ca"]["Couldn't flag package safe."] = "No s'ha pogut marcar el paquet com Segur.";

$_t["ca"]["You did not select any packages to un-vote for."] = "No heu seleccionat cap paquet per llevar-li el vot.";

$_t["ca"]["The selected packages have been unflagged."] = "Els paquets seleccionats s'han desmarcat.";

$_t["ca"]["You did not select any packages to adopt."] = "No heu seleccionat cap paquet per apropiar-se'n.";

$_t["ca"]["You must be logged in before you can flag packages."] = "Heu d'identificar-vos abans de marcar paquets.";

$_t["ca"]["You must be logged in before you can get notifications on comments."] = "Heu d'identificar-vos abans de rebre notificacions dels comentaris.";

$_t["ca"]["You must be logged in before you can vote for packages."] = "Heu d'identificar-vos abans de votar paquets.";

$_t["ca"]["The selected packages have been flagged out-of-date."] = "Els paquets seleccionats s'han marcat com No-Actualitzats.";

$_t["ca"]["The selected packages have been deleted."] = "Els paquets seleccionats s'han esborrat.";

$_t["ca"]["You did not select any packages to vote for."] = "No heu seleccionat cap paquet per votar.";

$_t["ca"]["You must be logged in before you can disown packages."] = "Heu d'identificar-vos abans de desapropiàr-se paquets.";

$_t["ca"]["Error trying to retrieve package details."] = "S'ha produït un error en obtenir els detalls del paquet.";

$_t["ca"]["The selected packages have been adopted."] = "Els paquets seleccionats s'han apropiat.";

$_t["ca"]["You have been removed from the comment notification list."] = "Heu sigut esborrat de la llista de notificacions de comentaris.";

$_t["ca"]["Your votes have been cast for the selected packages."] = "Els vostres vots s'han enviat per als paquets seleccionats.";

$_t["ca"]["The selected packages have been unflagged safe."] = "Els paquets seleccionats s'han desmarcat de Segurs.";

$_t["ca"]["You must be logged in before you can cancel notification on comments."] = "Heu d'identificar-vos abans de cancel·lar les notificacions en comentaris.";

$_t["ca"]["You must be logged in before you can adopt packages."] = "Heu d'identificar-vos abans d'apropiar-se paquets.";

$_t["ca"]["You have been added to the comment notification list."] = "Heu sigut afegit a la llista de notificacions de comentaris.";

$_t["ca"]["You did not select any packages to disown."] = "No heu seleccionat cap paquet per desapropiar-se'n.";

$_t["ca"]["You must be logged in before you can un-vote for packages."] = "Heu d'identificar-vos abans de llevar el vot als paquets.";

$_t["ca"]["You must be logged in before you can unflag packages."] = "Heu d'identificar-vos abans de desmarcar paquets.";

$_t["ca"]["You did not select any packages to unflag."] = "No heu seleccionat cap paquet per a desmarcar.";

$_t["ca"]["Couldn't unflag package safe."] = "No s'ha pogut desmarcar el paquet Segur.";

$_t["ca"]["You did not select any packages to delete."] = "No heu seleccionat cap paquet per a esborrar.";

$_t["ca"]["Couldn't add to notification list."] = "No s'ha pogut afegir a la llista de notificació.";

$_t["ca"]["You did not select any packages to flag."] = "No heu seleccionat cap paquet per a marcar.";

$_t["ca"]["The selected packages have been disowned."] = "Els paquets seleccionats han sigut desapropiats.";

$_t["ca"]["The selected packages have been flagged safe."] = "Els paquets seleccionats han sigut marcats com Segurs.";

$_t["ca"]["Couldn't remove from notification list."] = "No s'ha pogut esborrar de la llista de notificació.";

?>