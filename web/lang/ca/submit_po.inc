<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["Missing build function in PKGBUILD."] = "Manca la funció \"build\" al PKGBUILD.";

$_t["ca"]["Could not change directory to %s."] = "No s'ha pogut canviar el directori a %s.";

$_t["ca"]["No"] = "No";

$_t["ca"]["Missing pkgdesc variable in PKGBUILD."] = "Manca la variable \"pkgdesc\" al PKGBUILD.";

$_t["ca"]["Error trying to upload file - please try again."] = "S'ha produït un error en pujar l'arxiu - si us plau proveu altre cop.";

$_t["ca"]["Error exec'ing the mv command."] = "S'ha produït un error en executar el comandament \"mv\".";

$_t["ca"]["You must create an account before you can upload packages."] = "Heu de crear un compte per poder pujar paquets.";

$_t["ca"]["Package upload successful."] = "El paquets s'ha pujat satisfactòriament.";

$_t["ca"]["Overwrite existing package?"] = "Voleu sobreescriure el paquet existent?";

$_t["ca"]["Binary packages and filelists are not allowed for upload."] = "Els arxius binaris i les llistes de fitxers no s'accepten per a pujar.";

$_t["ca"]["You did not specify a package name."] = "No heu especificat cap nom de paquet.";

$_t["ca"]["Error trying to unpack upload - PKGBUILD does not exist."] = "S'ha produït un error en intentar desempaquetar la pujada - El PKGBUILD no existeix.";

$_t["ca"]["Could not create incoming directory: %s."] = "No s'ha pogut crear el directori de recepció: %s.";

$_t["ca"]["Upload package file"] = "Puja arxiu del paquet";

$_t["ca"]["Package Location"] = "Localització del paquet";

$_t["ca"]["Missing url variable in PKGBUILD."] = "Manca la variable \"URL\" al PKGBUILD.";

$_t["ca"]["Package names do not match."] = "Els noms del paquet no coincideixen.";

$_t["ca"]["Package Category"] = "Categoria del paquet";

$_t["ca"]["Could not change to directory %s."] = "No s'ha pogut canviar al directori %s.";

$_t["ca"]["You did not tag the 'overwrite' checkbox."] = "No s'ha pogut marcat la casella \"Sobreescriu\".";

$_t["ca"]["Invalid name: only lowercase letters are allowed."] = "Nom no vàlid: sols es permet lletres minúscules.";

$_t["ca"]["Missing pkgver variable in PKGBUILD."] = "Manca la variable \"pkgver\" al PKGBUILD.";

$_t["ca"]["Package name"] = "Nom del paquet";

$_t["ca"]["Upload"] = "Puja";

$_t["ca"]["Missing md5sums variable in PKGBUILD."] = "Manca la variable \"md5sums\" al PKGBUILD.";

$_t["ca"]["Missing pkgrel variable in PKGBUILD."] = "Manca la variable \"pkgrel\" al PKGBUILD.";

$_t["ca"]["Missing pkgname variable in PKGBUILD."] = "Manca la variable \"pkgname\" al PKGBUILD.";

$_t["ca"]["Error - No file uploaded"] = "S'ha produït un error - El fitxer no s'ha pujat";

$_t["ca"]["Package URL is missing a protocol (ie. http:// ,ftp://)"] = "Manca el protocol a la URL del paquet (per exemple: http:// , ftp://)";

$_t["ca"]["You are not allowed to overwrite the %h%s%h package."] = "No esteu autoritzat a sobreescriure el paquet %h%s%h.";

$_t["ca"]["Select Location"] = "Seleccioneu localització";

$_t["ca"]["Select Category"] = "Seleccioneu categoria";

$_t["ca"]["Comment"] = "Comentari";

$_t["ca"]["Could not create directory %s."] = "No s'ha pogut crear el directori %s.";

$_t["ca"]["Unknown file format for uploaded file."] = "No es coneix el format del fitxer pujat.";

$_t["ca"]["Missing source variable in PKGBUILD."] = "Manca la variable \"source\" al PKGBUILD.";

$_t["ca"]["Could not re-tar"] = "No s'ha pogut tornar a crear l'arxiu TAR.";

$_t["ca"]["Sorry, uploads are not permitted by this server."] = "Ho sentim, les pujades no estan permesses en aquest servidor.";

$_t["ca"]["You must supply a comment for this upload/change."] = "Heu de subministrar un comentari per a aquest/a canvi/pujada.";

$_t["ca"]["Yes"] = "Sí";

?>