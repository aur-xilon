<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["Select your language here: %h%s%h, %h%s%h, %h%s%h, %h%s%h."] = "Seleccioneu el vostre idioma ací: %h%s%h, %h%s%h, %h%s%h, %h%s%h";

$_t["ca"]["Hello, world!"] = "Hola, món!";

$_t["ca"]["Hello, again!"] = "Hola, altre cop!";

$_t["ca"]["My current language tag is: '%s'."] = "La meua etiqueta actual d'idioma és: '%s'.";

?>