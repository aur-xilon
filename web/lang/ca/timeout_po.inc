<?php
# Catalan (Català) translation
# Translator: Sergio Jovani Guzman <moret.sjg@gmail.com>

include_once("translator.inc");
global $_t;

$_t["ca"]["Click on the Home link above to log in."] = "Feu clic a l'enllaç Inici de dalt per a identificar-vos.";

$_t["ca"]["Your session has timed out.  You must log in again."] = "La vostra sessió a expirat. Heu d'identificar-vos de nou.";

?>