<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/common_po.inc");

include_once("pl/common_po.inc");

include_once("it/common_po.inc");

include_once("ca/common_po.inc");

include_once("pt/common_po.inc");

include_once("es/common_po.inc");

include_once("de/common_po.inc");

include_once("ru/common_po.inc");

include_once("fr/common_po.inc");

?>