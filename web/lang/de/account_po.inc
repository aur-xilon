<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["Use this form to update your account."] = "Hier kannst Du Deine Benutzerdaten ändern.";

$_t["de"]["Leave the password fields blank to keep your same password."] = "Lasse die Passwort-Felder leer, um Dein aktuelles Passwort beizubehalten.";

$_t["de"]["You are not allowed to access this area."] = "Du darfst diesen Bereich nicht betreten.";

$_t["de"]["Could not retrieve information for the specified user."] = "Konnte keine Informationen zum angegebenen Benutzer abrufen.";

$_t["de"]["Use this form to search existing accounts."] = "Benutze dieses Formular, um nach vorhandenen Konten zu suchen.";

$_t["de"]["You do not have permission to edit this account."] = "Du hast keine Berechtigung, dieses Konto zu ändern.";

$_t["de"]["Use this form to create an account."] = "Benutze dieses Formular, um ein neues Konto zu erstellen.";

$_t["de"]["You must log in to view user information."] = "Du mußt Dich anmelden, um Benutzer-Informationen zu sehen.";

?>