<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["Missing a required field."] = "Ein benötigtes Feld fehlt.";

$_t["de"]["Search'"] = "Suche'";

$_t["de"]["The account, %h%s%h, has been successfully created."] = "Das Konto %h%s%h wurde erfolgreich angelegt.";

$_t["de"]["Error trying to modify account, %h%s%h: %s."] = "Fehler beim Ändern des Kontos %h%s%h: %s";

$_t["de"]["The email address is invalid."] = "Die E-Mail-Adresse ist ungültig.";

$_t["de"]["Error trying to create account, %h%s%h: %s."] = "Fehler beim Erstellen des Kontos %h%s%h: %s.";

$_t["de"]["The username, %h%s%h, is already in use."] = "Der Benutzername %h%s%h ist bereits vergeben.";

$_t["de"]["Account Type"] = "Konto-Typ";

$_t["de"]["The account, %h%s%h, has been successfully modified."] = "Das Konto %h%s%h wurde erfolgreich geändert.";

$_t["de"]["Account Suspended"] = "Konto gesperrt";

$_t["de"]["Status"] = "Status";

$_t["de"]["New Package Notify"] = "Benachrichtigung bei neuen Paketen";

$_t["de"]["IRC Nick"] = "IRC-Name";

$_t["de"]["Trusted user"] = "Vertrauenswürdiger Benutzer (TU)";

$_t["de"]["No results matched your search criteria."] = "Die Suche ergab leider kein Ergebnis.";

$_t["de"]["Normal user"] = "Normaler Benutzer";

$_t["de"]["Never"] = "Niemals";

$_t["de"]["User"] = "Benutzer";

$_t["de"]["Active"] = "Aktiv";

$_t["de"]["Last Voted"] = "Zuletzt abgestimmt";

$_t["de"]["Real Name"] = "Wirklicher Name";

$_t["de"]["Edit Account"] = "Konto bearbeiten";

$_t["de"]["Password fields do not match."] = "Passwort-Felder sind unterschiedlich.";

$_t["de"]["View this user's packages"] = "Alle Pakete dieses Benutzers";

$_t["de"]["Language"] = "Sprache";

$_t["de"]["A Trusted User cannot assign Developer status."] = "Ein Vertrauenswürdiger Benutzer (TU) kann keinen Entwickler-Status erteilen.";

$_t["de"]["The address, %h%s%h, is already in use."] = "Die Adresse %h%s%h wird bereits benutzt.";

$_t["de"]["No more results to display."] = "Keine weiteren Ergebnisse.";

$_t["de"]["Type"] = "Typ";

$_t["de"]["Click on the Home link above to login."] = "Klicke auf \"Start\", um Dich anzumelden.";

$_t["de"]["Sort by"] = "Sortieren nach";

$_t["de"]["Re-type password"] = "Bestätige das Passwort";

$_t["de"]["Language is not currently supported."] = "Diese Sprache wird momentan noch nicht unterstützt.";

$_t["de"]["Any type"] = "Irgendein Typ";

$_t["de"]["Last vote"] = "Letzte Stimme";

$_t["de"]["Suspended"] = "Gesperrt";

$_t["de"]["Trusted User"] = "Vertrauenswürdiger Benutzer (TU)";

$_t["de"]["Missing User ID"] = "Benutzer-ID fehlt";

$_t["de"]["Developer"] = "Entwickler";

?>