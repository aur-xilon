<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["%s: %sAn ArchLinux project%s"] = "%s: %sEin ArchLinux Projekt%s";

$_t["de"]["Logout"] = "Abmelden";

$_t["de"]["Discussion"] = "Diskussion";

$_t["de"]["Bugs"] = "Fehler";

$_t["de"]["Accounts"] = "Konten";

$_t["de"]["Home"] = "Startseite";

$_t["de"]["Packages"] = "Pakete";

$_t["de"]["My Packages"] = "Meine Pakete";

?>