<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer


include_once("translator.inc");
global $_t;

$_t["de"]["Reset"] = "Zurücksetzen";

$_t["de"]["Username"] = "Benutzername";

$_t["de"]["Email Address"] = "E-Mail-Adresse";

$_t["de"]["Less"] = "Weniger";

$_t["de"]["Clear"] = "Leeren";

$_t["de"]["required"] = "Notwendig";

$_t["de"]["Update"] = "Aktualisieren";

$_t["de"]["Submit"] = "Abschicken";

$_t["de"]["Password"] = "Passwort";

$_t["de"]["Create"] = "Erstellen";

$_t["de"]["More"] = "Mehr";

?>