<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["Your session id is invalid."] = "Deine Sitzungs-ID ist ungültig";

$_t["de"]["If this problem persists, please contact the site administrator."] = "Sollte dieses Problem ständig auftreten, benachrichtige bitte den Administrator dieser Seite";

?>