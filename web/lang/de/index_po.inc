<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["Statistics"] = "Statistiken";

$_t["de"]["Remember to vote for your favourite packages!"] = "Denke daran, für Deine bevorzugten Pakete zu stimmen!";

$_t["de"]["Error looking up username, %s."] = "Benutzer %s nicht gefunden.";

$_t["de"]["Packages in unsupported"] = "Pakete in \"unsupported\"";

$_t["de"]["The most popular packages will be provided as binary packages in [community]."] = "Die beliebtesten Pakete werden als Binär-Pakete in [community] bereitgestellt.";

$_t["de"]["Trusted Users"] = "Vertrauenswürdige Benutzer (TU)";

$_t["de"]["You must supply a username."] = "Du mußt einen Benutzernamen eingeben.";

$_t["de"]["Packages added or updated in the past 7 days"] = "Pakete, die in den letzten 7 Tagen hinzugefügt oder geändert wurden";

$_t["de"]["Email discussion about the AUR takes place on the %sTUR Users List%s."] = "E-Mail-Diskussionen über das AUR finden in der %sTUR Users Liste%s statt";

$_t["de"]["Packages in unsupported and flagged as safe"] = "Pakete in \"unsupported\", die als sicher markiert sind";

$_t["de"]["Though we can't vouch for their contents, we provide a %hlist of user repositories%h for your convenience."] = "Obwohl wir für deren Inhalte nicht garantieren können, stellen wir eine %hListe der Benutzer-Repositorien%h bereit.";

$_t["de"]["Packages in [community]"] = "Pakete in [community]";

$_t["de"]["Recent Updates"] = "Letzte Aktualisierungen";

$_t["de"]["Your account has been suspended."] = "Dein Konto wurde gesperrt.";

$_t["de"]["Username:"] = "Benutzername:";

$_t["de"]["Error trying to generate session id."] = "Fehler beim Erstellen der Sitzungs-ID.";

$_t["de"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information."] = "Willkommen beim AUR! Für weitergehende Informationen lies bitte das %hAUR Benutzerhandbuch%h und die %hAUR TU Richtlinien%h.";

$_t["de"]["Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "Deine PKGBUILDs <strong>müssen</strong> dem %hArch Paket Standard%h entsprechen. Andernfalls werden sie gelöscht!";

$_t["de"]["Login"] = "Anmelden";

$_t["de"]["If you have feedback about the AUR, please leave it in %hFlyspray%h."] = "Rückmeldungen zum AUR kannst Du uns im %hFlyspray%h zukommen lassen.";

$_t["de"]["You must supply a password."] = "Du mußt ein Passwort eingeben.";

$_t["de"]["Password:"] = "Passwort";

$_t["de"]["Registered Users"] = "Registrierte Benutzer";

$_t["de"]["Logged-in as: %h%s%h"] = "Angemeldet als: %h%s%h";

$_t["de"]["Incorrect password for username, %s."] = "Falsches Passwort für Benutzername %s.";

$_t["de"]["Safe"] = "Sicher";

$_t["de"]["Out-of-date"] = "Veraltet";

$_t["de"]["User Statistics"] = "Benutzerstatistiken";

$_t["de"]["Flagged as safe by me"] = "Von mir als sicher markiert";

$_t["de"]["Flagged as safe"] = "Als sicher markiert";

$_t["de"]["My Statistics"] = "Meine Statistiken";

?>