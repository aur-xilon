<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["You have been successfully logged out."] = "Du wurdest erfolgreich abgemeldet.";

?>