<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["Missing package ID."] = "Paket-ID fehlt.";

$_t["de"]["Invalid category ID."] = "Kategorie-ID ungültig.";

$_t["de"]["Enter your comment below."] = "Füge Deinen Kommentar hier ein.";

$_t["de"]["You are not allowed to delete this comment."] = "Du darfst diesen Kommentar nicht löschen.";

$_t["de"]["Missing comment ID."] = "Kommentar-ID fehlt.";

$_t["de"]["Package category updated."] = "Die Kategorie des Pakets wurde aktualisiert.";

$_t["de"]["You must be logged in before you can edit package information."] = "Du mußt Dich anmelden, um Paket-Informationen zu bearbeiten.";

$_t["de"]["Comment has been deleted."] = "Kommentar wurde gelöscht.";

$_t["de"]["You've found a bug if you see this...."] = "Wenn Du dies ließt, hast Du einen Fehler gefunden...";

$_t["de"]["Comment has been added."] = "Kommentar wurde hinzugefügt.";

$_t["de"]["Select new category"] = "Wähle neue Kategorie.";

?>