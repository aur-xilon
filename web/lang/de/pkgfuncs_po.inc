<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["Category"] = "Kategorie";

$_t["de"]["Search by"] = "Suche nach";

$_t["de"]["Delete comment"] = "Entferne Kommentar";

$_t["de"]["orphan"] = "Verwaist";

$_t["de"]["Votes"] = "Stimmen";

$_t["de"]["First Submitted"] = "Zuerst eingereicht am";

$_t["de"]["Tarball"] = "Tar-Datei";

$_t["de"]["Be careful! The above files may contain malicious code that can damage your system."] = "Sei vorsichtig! Die obengenannten Dateien könnten Code enthalten, der Dein System beschädigt.";

$_t["de"]["Voted"] = "Abgestimmt";

$_t["de"]["Location"] = "Ort";

$_t["de"]["Flag Safe"] = "Als \"Sicher\" markieren";

$_t["de"]["Go"] = "Gehe zu";

$_t["de"]["Unflag Out-of-date"] = "Markierung \"Veraltet\" entfernen";

$_t["de"]["Go back to %hpackage details view%h."] = "Zurück zur %hAnsicht der Paket-Details%h";

$_t["de"]["Error retrieving package details."] = "Fehler beim Aufrufen der Paket-Details.";

$_t["de"]["Description"] = "Beschreibung";

$_t["de"]["My Packages"] = "Meine Pakete";

$_t["de"]["Safe"] = "Sicher";

$_t["de"]["Sort order"] = "Neu ordnen";

$_t["de"]["Ascending"] = "Aufsteigend";

$_t["de"]["Keywords"] = "Schlagwörter";

$_t["de"]["No New Comment Notification"] = "Keine neue Kommentar-Benachrichtigung";

$_t["de"]["Dependencies"] = "Abhängigkeiten";

$_t["de"]["Descending"] = "Absteigend";

$_t["de"]["Per page"] = "Pro Seite";

$_t["de"]["Package Listing"] = "Paket-Liste";

$_t["de"]["Package details could not be found."] = "Paket-Details konnten nicht gefunden werden.";

$_t["de"]["Package Details"] = "Paket-Details";

$_t["de"]["Error retrieving package list."] = "Fehler beim Aufrufen der Paket-Liste.";

$_t["de"]["Files"] = "Dateien";

$_t["de"]["Name"] = "Name";

$_t["de"]["Last Updated"] = "Letzte Aktualisierung";

$_t["de"]["The above files have been verified (by %s) and are safe to use."] = "Die obenstehenden Dateien wurden (von %s) bestätigt, und können sicher benutzt werden.";

$_t["de"]["Unflag Package Safe To Use"] = "Markierung \"Sicher\" entfernen";

$_t["de"]["Go back to %hsearch results%h."] = "Zurück zu %hSuchergebnis%h.";

$_t["de"]["Age"] = "Alter";

$_t["de"]["Comments"] = "Kommentare";

$_t["de"]["Submitter"] = "Eingereicht von";

$_t["de"]["Un-Vote"] = "Abwählen";

$_t["de"]["change category"] = "Kategorie wechseln";

$_t["de"]["UnNotify"] = "Nicht mehr benachrichtigen";

$_t["de"]["Delete Packages"] = "Pakete entfernen";

$_t["de"]["Maintainer"] = "Betreuer";

$_t["de"]["Add Comment"] = "Kommentar hinzufügen";

$_t["de"]["Comment by: %h%s%h on %h%s%h"] = "Kommentar von: %h%s%h am %h%s%h";

$_t["de"]["Flag Out-of-date"] = "Als \"Veraltet\" markieren";

$_t["de"]["Manage"] = "Verwalten";

$_t["de"]["Sort by"] = "Sortieren nach";

$_t["de"]["Flag Package Safe To Use"] = "Paket als \"sicher\" markieren";

$_t["de"]["Actions"] = "Aktionen";

$_t["de"]["Unflag Safe"] = "Markierung \"sicher\" entfernen";

$_t["de"]["Sources"] = "Quellen";

$_t["de"]["Yes"] = "Ja";

$_t["de"]["Search Criteria"] = "Suchkriterien";

$_t["de"]["Notify"] = "Benachrichtigen";

$_t["de"]["O%hut-of-Date"] = "V%heraltet";

$_t["de"]["Vote"] = "Abstimmen";

$_t["de"]["Adopt Packages"] = "Pakete übernehmen";

$_t["de"]["New Comment Notification"] = "Neue Kommentar-Benachrichtigung";

$_t["de"]["Disown Packages"] = "Betreuung der Pakete freigeben";

$_t["de"]["Orphans"] = "Verwaiste Pakete";

$_t["de"]["Any"] = "Alle";

$_t["de"]["No packages matched your search criteria."] = "Keine Pakete entsprachen Deinen Suchkriterien";

$_t["de"]["Status"] = "Status";

$_t["de"]["Leave the password fields blank to keep your same password."] = "Lasse die Passwortfelder frei, um Dein aktuelles Passwort beizubehalten.";

$_t["de"]["unknown"] = "unbekannt";

$_t["de"]["You have been successfully logged out."] = "Du wurdest erfolgreich abgemeldet.";

$_t["de"]["You must log in to view user information."] = "Du mußt Dich anmelden, um Benutzerinformationen anzusehen.";

$_t["de"]["License"] = "Lizenz";

$_t["de"]["Could not retrieve information for the specified user."] = "Konnte keine Informationen für den angegebenen Benutzer laden.";

$_t["de"]["You do not have permission to edit this account."] = "Du hast keine Berechtigung dieses Konto zu ändern.";

$_t["de"]["Use this form to search existing accounts."] = "Benutze dieses Formular um vorhandene Konten zu suchen.";

$_t["de"]["All"] = "Alle";

$_t["de"]["Use this form to create an account."] = "Benutze dieses Formular um ein Konto zu erstellen.";

$_t["de"]["Use this form to update your account."] = "Benutze dieses Formular um Dein Konto zu aktualisieren.";

$_t["de"]["You are not allowed to access this area."] = "Es ist Dir nicht erlaubt diesen Bereich zu betreten.";

$_t["de"]["Unsafe"] = "Unsicher";

?>