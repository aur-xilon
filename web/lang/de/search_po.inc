<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["None of the selected packages could be deleted."] = "Keines der markierten Pakete konnte gelöscht werden.";

$_t["de"]["Your votes have been removed from the selected packages."] = "Deine Stimmen wurden von den markierten Paketen entfernt.";

$_t["de"]["Couldn't flag package safe."] = "Konnte Paket nicht als \"Sicher\" markieren.";

$_t["de"]["You did not select any packages to un-vote for."] = "Du hast kein Paket zum Abwählen ausgewählt.";

$_t["de"]["The selected packages have been unflagged."] = "Die Markierungen der gewählten Pakete wurden entfernt.";

$_t["de"]["You did not select any packages to adopt."] = "Du hast kein Paket zum Übernehmen gewählt.";

$_t["de"]["You must be logged in before you can flag packages."] = "Du mußt Dich anmelden, um Pakete markieren zu können.";

$_t["de"]["You must be logged in before you can get notifications on comments."] = "Du mußt Dich anmelden, um über Kommentare benachrichtigt zu werden.";

$_t["de"]["You must be logged in before you can vote for packages."] = "Du mußt dich anmelden, um für ein Paket stimmen zu können.";

$_t["de"]["The selected packages have been flagged out-of-date."] = "Die gewählten Pakete wurden als \"Veraltet\" markiert.";

$_t["de"]["The selected packages have been deleted."] = "Die gewählten Pakete wurden gelöscht.";

$_t["de"]["You did not select any packages to vote for."] = "Du hast kein Paket zum Wählen ausgewählt.";

$_t["de"]["You must be logged in before you can disown packages."] = "Du mußt Dich anmelden, um die Betreuung eines Paketes abzugeben.";

$_t["de"]["Error trying to retrieve package details."] = "Ein Fehler ist während des Empfangens der Paket-Details entstanden.";

$_t["de"]["The selected packages have been adopted."] = "Das gewählte Paket wurde übernommen.";

$_t["de"]["You have been removed from the comment notification list."] = "Du wurdest von der Kommentar-Benachrichtigungsliste entfernt.";

$_t["de"]["Your votes have been cast for the selected packages."] = "Deine Stimmen wurden für die gewählten Pakete gezählt.";

$_t["de"]["The selected packages have been unflagged safe."] = "Die Markierung \"Sicher\" der gewählten Pakete wurde entfernt.";

$_t["de"]["You must be logged in before you can cancel notification on comments."] = "Du mußt Dich anmelden, um die Kommentar-Benachrichtigung aufzuheben.";

$_t["de"]["You must be logged in before you can adopt packages."] = "Du mußt Dich anmelden, um Pakete übernehmen zu können.";

$_t["de"]["You have been added to the comment notification list."] = "Du wurdest der Kommentar-Benachrichtigungsliste hinzugefügt.";

$_t["de"]["You did not select any packages to disown."] = "Du hast kein Paket gewählt, dessen Betreuung Du abgeben willst.";

$_t["de"]["You must be logged in before you can un-vote for packages."] = "Du mußt Dich anmelden, um Pakete abwählen zu können.";

$_t["de"]["You must be logged in before you can unflag packages."] = "Du mußt Dich anmelden, um die Markierung von Paketen entfernen zu können.";

$_t["de"]["You did not select any packages to unflag."] = "Du hast kein Paket für das Entfernen der Markierung ausgewählt.";

$_t["de"]["Couldn't unflag package safe."] = "Konnte die Markierung \"Sicher\" von dem Paket nicht entfernen.";

$_t["de"]["You did not select any packages to delete."] = "Du hast kein Paket zum Löschen ausgewählt.";

$_t["de"]["Couldn't add to notification list."] = "Konnte nichts zur Benachrichtigungsliste hinzufügen.";

$_t["de"]["You did not select any packages to flag."] = "Du hast kein Paket zum Markieren ausgewählt.";

$_t["de"]["The selected packages have been disowned."] = "Die Betreuung der gewählten Pakete wurde freigegeben.";

$_t["de"]["The selected packages have been flagged safe."] = "Die gewählten Pakete wurde als \"Sicher\" markiert.";

$_t["de"]["Couldn't remove from notification list."] = "Konnte nichts von der Benachrichtigungsliste entfernen.";

?>