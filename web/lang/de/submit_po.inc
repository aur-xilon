<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["Missing build function in PKGBUILD."] = "Funktion \"build\" fehlt im PKGBUILD.";

$_t["de"]["Could not change directory to %s."] = "Konnte nicht in das Verzeichnis %s wechseln.";

$_t["de"]["No"] = "Nein";

$_t["de"]["Missing pkgdesc variable in PKGBUILD."] = "pkgdesc-Variable fehlt im PKGBUILD.";

$_t["de"]["Error trying to upload file - please try again."] = "Beim Hochladen der Datei ist ein Fehler aufgetreten - Bitte erneut versuchen.";

$_t["de"]["Error exec'ing the mv command."] = "Ein Fehler ist bei der Ausführung des Kommandos \"mv\" (Verschieben) aufgetreten.";

$_t["de"]["You must create an account before you can upload packages."] = "Du mußt ein Konto anlegen, bevor Du Dateien hochladen kannst.";

$_t["de"]["Package upload successful."] = "Paket erfolreich hochgeladen.";

$_t["de"]["Overwrite existing package?"] = "Existierendes Paket überschreiben?";

$_t["de"]["Binary packages and filelists are not allowed for upload."] = "Binärpakete und Dateilisten dürfen nicht hochgeladen werden.";

$_t["de"]["You did not specify a package name."] = "Du hast keinen Paketnamen angegeben.";

$_t["de"]["Error trying to unpack upload - PKGBUILD does not exist."] = "Fehler beim Entpacken der hochgeladenen Datei - PKGBUILD existiert nicht.";

$_t["de"]["Could not create incoming directory: %s."] = "Konnte Eingangs-Verzeichnis nicht erstellen: %s.";

$_t["de"]["Upload package file"] = "Hochladen der Paket-Datei";

$_t["de"]["Package Location"] = "Ort des Pakets";

$_t["de"]["Missing url variable in PKGBUILD."] = "URL-Variable fehlt im PKGBUILD.";

$_t["de"]["Package names do not match."] = "Paketnamen stimmen nicht überein.";

$_t["de"]["Package Category"] = "Paket-Kategorie";

$_t["de"]["Could not change to directory %s."] = "Konnte nicht in das Verzeichnis %s wechseln";

$_t["de"]["You did not tag the 'overwrite' checkbox."] = "Du hast die Option \"Überschreiben\" nicht gewählt.";

$_t["de"]["Invalid name: only lowercase letters are allowed."] = "Ungültiger Name: Es dürfen nur Kleinbuchstaben verwendet werden.";

$_t["de"]["Missing pkgver variable in PKGBUILD."] = "pkgver-Variable fehlt im PKGBUILD.";

$_t["de"]["Package name"] = "Paketname";

$_t["de"]["Upload"] = "Hochladen";

$_t["de"]["Missing md5sums variable in PKGBUILD."] = "md5sum-Variable fehlt im PKGBUILD.";

$_t["de"]["Missing pkgrel variable in PKGBUILD."] = "pkgrel-Variable fehlt im PKGBUILD.";

$_t["de"]["Missing pkgname variable in PKGBUILD."] = "pkgname-Variable fehlt im PKGBUILD.";

$_t["de"]["Error - No file uploaded"] = "Fehler - Keine Datei hochgeladen.";

$_t["de"]["Package URL is missing a protocol (ie. http:// ,ftp://)"] = "Der Paket-URL fehlt ein Protokoll (z.B. http:// oder ftp://).";

$_t["de"]["You are not allowed to overwrite the %h%s%h package."] = "Es ist Dir nicht erlaubt, das %h%s%h Paket zu überschreiben.";

$_t["de"]["Select Location"] = "Wähle einen Ort";

$_t["de"]["Select Category"] = "Wähle eine Kategorie";

$_t["de"]["Comment"] = "Kommentar";

$_t["de"]["Could not create directory %s."] = "Konnte das Verzeichnis %s nicht erstellen.";

$_t["de"]["Unknown file format for uploaded file."] = "Unbekanntes Dateiformat der hochgeladenen Datei.";

$_t["de"]["Missing source variable in PKGBUILD."] = "Fehlende Quell-Variable in PKGBUILD.";

$_t["de"]["Could not re-tar"] = "Konnte nicht erneut den tar-Befehl ausführen.";

$_t["de"]["Sorry, uploads are not permitted by this server."] = "Tut mir Leid, dieser Server erlaubt kein Hochladenen von Dateien.";

$_t["de"]["You must supply a comment for this upload/change."] = "Du mußt die Änderungen mit einem Kommentar versehen.";

$_t["de"]["Yes"] = "Ja";

$_t["de"]["Missing license variable in PKGBUILD."] = "Lizenz-Variable in PKGBUILD fehlt.";

$_t["de"]["Missing arch variable in PKGBUILD."] = "Arch-Variable in PKGBUILD fehlt.";

?>