<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["Select your language here: %h%s%h, %h%s%h, %h%s%h, %h%s%h."] = "Wähle hier deine Sprache aus: %h%s%h, %h%s%h, %h%s%h, %h%s%h.";

$_t["de"]["Hello, world!"] = "Hallo, Welt!";

$_t["de"]["Hello, again!"] = "Nochmals Hallo!";

$_t["de"]["My current language tag is: '%s'."] = "Meine momentan gewählte Sprache ist: '%s'";

?>