<?php
# German (Deutsch) translation
# Translators: Pierre Schmitz <pierre@archlinux.de>, Matthias Gorissen <siquame@web.de>, Lukas Kropatschek, Niclas Pfeifer

include_once("translator.inc");
global $_t;

$_t["de"]["Click on the Home link above to log in."] = "Klicke auf den obenstehenden Startknopf, um dich anzumelden.";

$_t["de"]["Your session has timed out.  You must log in again."] = "Deine Sitzung ist abgelaufen. Du mußt dich erneut einloggen.";

?>