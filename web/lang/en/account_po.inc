<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["en"]["Use this form to update your account."] = "Use this form to update your account.";

$_t["en"]["Leave the password fields blank to keep your same password."] = "Leave the password fields blank to keep your same password.";

$_t["en"]["You are not allowed to access this area."] = "You are not allowed to access this area.";

$_t["en"]["Could not retrieve information for the specified user."] = "Could not retrieve information for the specified user.";

$_t["en"]["Use this form to search existing accounts."] = "Use this form to search existing accounts.";

$_t["en"]["You do not have permission to edit this account."] = "You do not have permission to edit this account.";

$_t["en"]["Use this form to create an account."] = "Use this form to create an account.";

$_t["en"]["You must log in to view user information."] = "You must log in to view user information.";

?>