<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["en"]["Missing a required field."] = "Missing a required field.";

$_t["en"]["Search'"] = "Search'";

$_t["en"]["The account, %h%s%h, has been successfully created."] = "The account, %h%s%h, has been successfully created.";

$_t["en"]["Error trying to modify account, %h%s%h: %s."] = "Error trying to modify account, %h%s%h: %s.";

$_t["en"]["The email address is invalid."] = "The email address is invalid.";

$_t["en"]["Error trying to create account, %h%s%h: %s."] = "Error trying to create account, %h%s%h: %s.";

$_t["en"]["The username, %h%s%h, is already in use."] = "The username, %h%s%h, is already in use.";

$_t["en"]["Account Type"] = "Account Type";

$_t["en"]["The account, %h%s%h, has been successfully modified."] = "The account, %h%s%h, has been successfully modified.";

$_t["en"]["Account Suspended"] = "Account Suspended";

$_t["en"]["Status"] = "Status";

$_t["en"]["New Package Notify"] = "New Package Notify";

$_t["en"]["IRC Nick"] = "IRC Nick";

$_t["en"]["Trusted user"] = "Trusted user";

$_t["en"]["No results matched your search criteria."] = "No results matched your search criteria.";

$_t["en"]["Normal user"] = "Normal user";

$_t["en"]["Never"] = "Never";

$_t["en"]["User"] = "User";

$_t["en"]["Active"] = "Active";

$_t["en"]["Last Voted"] = "Last Voted";

$_t["en"]["Real Name"] = "Real Name";

$_t["en"]["Edit Account"] = "Edit Account";

$_t["en"]["Password fields do not match."] = "Password fields do not match.";

$_t["en"]["Language"] = "Language";

$_t["en"]["A Trusted User cannot assign Developer status."] = "A Trusted User cannot assign Developer status.";

$_t["en"]["The address, %h%s%h, is already in use."] = "The address, %h%s%h, is already in use.";

$_t["en"]["No more results to display."] = "No more results to display.";

$_t["en"]["Type"] = "Type";

$_t["en"]["Click on the Home link above to login."] = "Click on the Home link above to login.";

$_t["en"]["Sort by"] = "Sort by";

$_t["en"]["Re-type password"] = "Re-type password";

$_t["en"]["Language is not currently supported."] = "Language is not currently supported.";

$_t["en"]["Any type"] = "Any type";

$_t["en"]["Last vote"] = "Last vote";

$_t["en"]["Suspended"] = "Suspended";

$_t["en"]["Trusted User"] = "Trusted User";

$_t["en"]["Missing User ID"] = "Missing User ID";

$_t["en"]["Developer"] = "Developer";

$_t["en"]["View this user's packages"] = "View this user's packages";

?>