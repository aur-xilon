<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["en"]["Home"] = "Home";

$_t["en"]["%s: An ArchLinux project"] = "%s: An ArchLinux project";

$_t["en"]["Packages"] = "Packages";

$_t["en"]["Accounts"] = "Accounts";

$_t["en"]["Logout"] = "Logout";

$_t["en"]["%s: %sAn ArchLinux project%s"] = "%s: %sAn ArchLinux project%s";

$_t["en"]["Discussion"] = "Discussion";

$_t["en"]["Bugs"] = "Bugs";

?>