<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["en"]["Reset"] = "Reset";

$_t["en"]["Username"] = "Username";

$_t["en"]["Email Address"] = "Email Address";

$_t["en"]["Less"] = "Less";

$_t["en"]["Clear"] = "Clear";

$_t["en"]["required"] = "required";

$_t["en"]["Update"] = "Update";

$_t["en"]["Submit"] = "Submit";

$_t["en"]["Password"] = "Password";

$_t["en"]["Create"] = "Create";

$_t["en"]["More"] = "More";

?>