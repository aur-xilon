<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["en"]["You must supply a password."] = "You must supply a password.";

$_t["en"]["You must supply a username."] = "You must supply a username.";

$_t["en"]["After that, this can be filled in with more meaningful text."] = "After that, this can be filled in with more meaningful text.";

$_t["en"]["Logged-in as: %h%s%h"] = "Logged-in as: %h%s%h";

$_t["en"]["Your account has been suspended."] = "Your account has been suspended.";

$_t["en"]["Password:"] = "Password:";

$_t["en"]["Username:"] = "Username:";

$_t["en"]["Welcome to the AUR! If you're a newcomer, you may want to read the %hGuidelines%h."] = "Welcome to the AUR! If you're a newcomer, you may want to read the %hGuidelines%h.";

$_t["en"]["This is where the intro text will go."] = "This is where the intro text will go.";

$_t["en"]["Error trying to generate session id."] = "Error trying to generate session id.";

$_t["en"]["For now, it's just a place holder."] = "For now, it's just a place holder.";

$_t["en"]["It's more important to get the login functionality finished."] = "It's more important to get the login functionality finished.";

$_t["en"]["Error looking up username, %s."] = "Error looking up username, %s.";

$_t["en"]["Login"] = "Login";

$_t["en"]["Though we can't vouch for their contents, we provide a %hlist of user repositories%h for your convenience."] = "Though we can't vouch for their contents, we provide a %hlist of user repositories%h for your convenience.";

$_t["en"]["If you have feedback about the AUR, please leave it in %hFlyspray%h."] = "If you have feedback about the AUR, please leave it in %hFlyspray%h.";

$_t["en"]["Incorrect password for username, %s."] = "Incorrect password for username, %s.";

$_t["en"]["Latest Packages:"] = "Latest Packages:";

$_t["en"]["Discussion about the AUR takes place on the %sTUR Users List%s."] = "Discussion about the AUR takes place on the %sTUR Users List%s.";

$_t["en"]["Email discussion about the AUR takes place on the %sTUR Users List%s."] = "Email discussion about the AUR takes place on the %sTUR Users List%s.";

$_t["en"]["Recent Updates"] = "Recent Updates";

$_t["en"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information. Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information. Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!";

$_t["en"]["Community"] = "Community";

$_t["en"]["Package Counts"] = "Package Counts";

$_t["en"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information."] = "Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information.";

$_t["en"]["Unsupported"] = "Unsupported";

$_t["en"]["Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!";

$_t["en"]["Statistics"] = "Statistics";

$_t["en"]["My Statistics"] = "My Statistics";

$_t["en"]["Flagged as safe by me"] = "Flagged as safe by me";

$_t["en"]["User Statistics"] = "User Statistics";

$_t["en"]["Registered Users"] = "Registered Users";

$_t["en"]["Trusted Users"] = "Trusted Users";

$_t["en"]["Packages in unsupported"] = "Packages in unsupported";

$_t["en"]["Packages in unsupported and flagged as safe"] = "Packages in unsupported and flagged as safe";

$_t["en"]["Packages in [community]"] = "Packages in [community]";

$_t["en"]["Remember to vote for your favourite packages! The most popular packages are provided as binary packages in [community]."] = "Remember to vote for your favourite packages! The most popular packages are provided as binary packages in [community].";

$_t["en"]["Remember to vote for your favourite packages!"] = "Remember to vote for your favourite packages!";

$_t["en"]["The most popular packages will be provided as binary packages in [community]."] = "The most popular packages will be provided as binary packages in [community].";

$_t["en"]["Packages added or updated in the past 7 days"] = "Packages added or updated in the past 7 days";

$_t["en"]["Out-of-date"] = "Out-of-date";

?>