<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["en"]["Missing package ID."] = "Missing package ID.";

$_t["en"]["Invalid category ID."] = "Invalid category ID.";

$_t["en"]["Enter your comment below."] = "Enter your comment below.";

$_t["en"]["You are not allowed to delete this comment."] = "You are not allowed to delete this comment.";

$_t["en"]["Missing comment ID."] = "Missing comment ID.";

$_t["en"]["Package category updated."] = "Package category updated.";

$_t["en"]["You must be logged in before you can edit package information."] = "You must be logged in before you can edit package information.";

$_t["en"]["Comment has been deleted."] = "Comment has been deleted.";

$_t["en"]["You've found a bug if you see this...."] = "You've found a bug if you see this....";

$_t["en"]["Comment has been added."] = "Comment has been added.";

$_t["en"]["Select new category"] = "Select new category";

?>