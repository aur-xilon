<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["en"]["Category"] = "Category";

$_t["en"]["Votes"] = "Votes";

$_t["en"]["Comment by: %h%s%h on %h%s%h"] = "Comment by: %h%s%h on %h%s%h";

$_t["en"]["Location"] = "Location";

$_t["en"]["Delete comment"] = "Delete comment";

$_t["en"]["Go"] = "Go";

$_t["en"]["Unflag Out-of-date"] = "Unflag Out-of-date";

$_t["en"]["Go back to %hpackage details view%h."] = "Go back to %hpackage details view%h.";

$_t["en"]["Error retrieving package details."] = "Error retrieving package details.";

$_t["en"]["Description"] = "Description";

$_t["en"]["My Packages"] = "My Packages";

$_t["en"]["Keywords"] = "Keywords";

$_t["en"]["Dependencies"] = "Dependencies";

$_t["en"]["Disown Packages"] = "Disown Packages";

$_t["en"]["Package details could not be found."] = "Package details could not be found.";

$_t["en"]["Package Details"] = "Package Details";

$_t["en"]["Error retrieving package list."] = "Error retrieving package list.";

$_t["en"]["Files"] = "Files";

$_t["en"]["None"] = "None";

$_t["en"]["Name"] = "Name";

$_t["en"]["Per page"] = "Per page";

$_t["en"]["Go back to %hsearch results%h."] = "Go back to %hsearch results%h.";

$_t["en"]["No packages matched your search criteria."] = "No packages matched your search criteria.";

$_t["en"]["O%hrphan"] = "O%hrphan";

$_t["en"]["orphan"] = "orphan";

$_t["en"]["Un-Vote"] = "Un-Vote";

$_t["en"]["change category"] = "change category";

$_t["en"]["UnNotify"] = "UnNotify";

$_t["en"]["Delete Packages"] = "Delete Packages";

$_t["en"]["Maintainer"] = "Maintainer";

$_t["en"]["Add Comment"] = "Add Comment";

$_t["en"]["Tarball"] = "Tarball";

$_t["en"]["Flag Out-of-date"] = "Flag Out-of-date";

$_t["en"]["Manage"] = "Manage";

$_t["en"]["Sort by"] = "Sort by";

$_t["en"]["Sort order"] = "Sort order";

$_t["en"]["Ascending"] = "Ascending";

$_t["en"]["Descending"] = "Descending";

$_t["en"]["Actions"] = "Actions";

$_t["en"]["Sources"] = "Sources";

$_t["en"]["Search Criteria"] = "Search Criteria";

$_t["en"]["Notify"] = "Notify";

$_t["en"]["O%hut-of-Date"] = "O%hut-of-Date";

$_t["en"]["Vote"] = "Vote";

$_t["en"]["Adopt Packages"] = "Adopt Packages";

$_t["en"]["Yes"] = "Yes";

$_t["en"]["Package Listing"] = "Package Listing";

$_t["en"]["Orphans"] = "Orphans";

$_t["en"]["Any"] = "Any";

$_t["en"]["Voted"] = "Voted";

$_t["en"]["No New Comment Notification"] = "No New Comment Notification";

$_t["en"]["New Comment Notification"] = "New Comment Notification";

$_t["en"]["Comments"] = "Comments";

$_t["en"]["The above files have been verified (by %s) and are safe to use."] = "The above files have been verified (by %s) and are safe to use.";

$_t["en"]["Be careful! The above files may contain malicious code that can damage your system."] = "Be careful! The above files may contain malicious code that can damage your system.";

$_t["en"]["Flag Safe"] = "Flag Safe";

$_t["en"]["Flag Package Safe To Use"] = "Flag Package Safe To Use";

$_t["en"]["Unflag Safe"] = "Unflag Safe";

$_t["en"]["Unflag Package Safe To Use"] = "Unflag Package Safe To Use";

$_t["en"]["Safe"] = "Safe";

$_t["en"]["Age"] = "Age";

$_t["en"]["First Submitted"] = "First Submitted";

$_t["en"]["Last Updated"] = "Last Updated";

$_t["en"]["Search by"] = "Search by";

$_t["en"]["Submitter"] = "Submitter";

$_t["en"]["All"] = "All";

$_t["en"]["Unsafe"] = "Unsafe";

$_t["en"]["Status"] = "Status";

$_t["en"]["License"] = "License";

$_t["en"]["unknown"] = "unknown";

$_t["en"]["Required by"] = "Required by";

?>
