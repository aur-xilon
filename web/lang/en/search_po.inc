<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["en"]["None of the selected packages could be deleted."] = "None of the selected packages could be deleted.";

$_t["en"]["Your votes have been removed from the selected packages."] = "Your votes have been removed from the selected packages.";

$_t["en"]["You did not select any packages to un-vote for."] = "You did not select any packages to un-vote for.";

$_t["en"]["The selected packages have been unflagged."] = "The selected packages have been unflagged.";

$_t["en"]["You did not select any packages to adopt."] = "You did not select any packages to adopt.";

$_t["en"]["You must be logged in before you can flag packages."] = "You must be logged in before you can flag packages.";

$_t["en"]["You must be logged in before you can get notifications on comments."] = "You must be logged in before you can get notifications on comments.";

$_t["en"]["You must be logged in before you can vote for packages."] = "You must be logged in before you can vote for packages.";

$_t["en"]["The selected packages have been flagged out-of-date."] = "The selected packages have been flagged out-of-date.";

$_t["en"]["The selected packages have been deleted."] = "The selected packages have been deleted.";

$_t["en"]["You did not select any packages to vote for."] = "You did not select any packages to vote for.";

$_t["en"]["You must be logged in before you can disown packages."] = "You must be logged in before you can disown packages.";

$_t["en"]["Error trying to retrieve package details."] = "Error trying to retrieve package details.";

$_t["en"]["The selected packages have been adopted."] = "The selected packages have been adopted.";

$_t["en"]["You have been removed from the comment notification list."] = "You have been removed from the comment notification list.";

$_t["en"]["Your votes have been cast for the selected packages."] = "Your votes have been cast for the selected packages.";

$_t["en"]["You must be logged in before you can cancel notification on comments."] = "You must be logged in before you can cancel notification on comments.";

$_t["en"]["You must be logged in before you can adopt packages."] = "You must be logged in before you can adopt packages.";

$_t["en"]["You have been added to the comment notification list."] = "You have been added to the comment notification list.";

$_t["en"]["You did not select any packages to disown."] = "You did not select any packages to disown.";

$_t["en"]["You must be logged in before you can un-vote for packages."] = "You must be logged in before you can un-vote for packages.";

$_t["en"]["You must be logged in before you can unflag packages."] = "You must be logged in before you can unflag packages.";

$_t["en"]["You did not select any packages to unflag."] = "You did not select any packages to unflag.";

$_t["en"]["You did not select any packages to delete."] = "You did not select any packages to delete.";

$_t["en"]["Couldn't add to notification list."] = "Couldn't add to notification list.";

$_t["en"]["You did not select any packages to flag."] = "You did not select any packages to flag.";

$_t["en"]["The selected packages have been disowned."] = "The selected packages have been disowned.";

$_t["en"]["Couldn't remove from notification list."] = "Couldn't remove from notification list.";

$_t["en"]["The selected packages have been flagged safe."] = "The selected packages have been flagged safe.";

$_t["en"]["Couldn't flag package safe."] = "Couldn't flag package safe.";

$_t["en"]["The selected packages have been unflagged safe."] = "The selected packages have been unflagged safe.";

$_t["en"]["Couldn't unflag package safe."] = "Couldn't unflag package safe.";

?>