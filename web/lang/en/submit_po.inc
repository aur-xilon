<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["en"]["Missing build function in PKGBUILD."] = "Missing build function in PKGBUILD.";

$_t["en"]["Could not change directory to %s."] = "Could not change directory to %s.";

$_t["en"]["No"] = "No";

$_t["en"]["Missing pkgdesc variable in PKGBUILD."] = "Missing pkgdesc variable in PKGBUILD.";

$_t["en"]["Error trying to upload file - please try again."] = "Error trying to upload file - please try again.";

$_t["en"]["Error exec'ing the mv command."] = "Error exec'ing the mv command.";

$_t["en"]["You must create an account before you can upload packages."] = "You must create an account before you can upload packages.";

$_t["en"]["Package upload successful."] = "Package upload successful.";

$_t["en"]["Overwrite existing package?"] = "Overwrite existing package?";

$_t["en"]["You did not specify a package name."] = "You did not specify a package name.";

$_t["en"]["Error trying to unpack upload - PKGBUILD does not exist."] = "Error trying to unpack upload - PKGBUILD does not exist.";

$_t["en"]["Could not create incoming directory: %s."] = "Could not create incoming directory: %s.";

$_t["en"]["Upload package file"] = "Upload package file";

$_t["en"]["Package Location"] = "Package Location";

$_t["en"]["Missing url variable in PKGBUILD."] = "Missing url variable in PKGBUILD.";

$_t["en"]["Package names do not match."] = "Package names do not match.";

$_t["en"]["Package Category"] = "Package Category";

$_t["en"]["Could not change to directory %s."] = "Could not change to directory %s.";

$_t["en"]["You did not tag the 'overwrite' checkbox."] = "You did not tag the 'overwrite' checkbox.";

$_t["en"]["Invalid name: only lowercase letters are allowed."] = "Invalid name: only lowercase letters are allowed.";

$_t["en"]["Missing pkgver variable in PKGBUILD."] = "Missing pkgver variable in PKGBUILD.";

$_t["en"]["Package name"] = "Package name";

$_t["en"]["Upload"] = "Upload";

$_t["en"]["Missing md5sums variable in PKGBUILD."] = "Missing md5sums variable in PKGBUILD.";

$_t["en"]["Missing pkgrel variable in PKGBUILD."] = "Missing pkgrel variable in PKGBUILD.";

$_t["en"]["Missing pkgname variable in PKGBUILD."] = "Missing pkgname variable in PKGBUILD.";

$_t["en"]["Error - No file uploaded"] = "Error - No file uploaded";

$_t["en"]["You are not allowed to overwrite the %h%s%h package."] = "You are not allowed to overwrite the %h%s%h package.";

$_t["en"]["Select Location"] = "Select Location";

$_t["en"]["Select Category"] = "Select Category";

$_t["en"]["Comment"] = "Comment";

$_t["en"]["Could not create directory %s."] = "Could not create directory %s.";

$_t["en"]["Unknown file format for uploaded file."] = "Unknown file format for uploaded file.";

$_t["en"]["Missing source variable in PKGBUILD."] = "Missing source variable in PKGBUILD.";

$_t["en"]["Sorry, uploads are not permitted by this server."] = "Sorry, uploads are not permitted by this server.";

$_t["en"]["You must supply a comment for this upload/change."] = "You must supply a comment for this upload/change.";

$_t["en"]["Yes"] = "Yes";

$_t["en"]["Package URL is missing a protocol (ie. http:// ,ftp://)"] = "Package URL is missing a protocol (ie. http:// ,ftp://)";

$_t["en"]["Could not re-tar"] = "Could not re-tar";

$_t["en"]["Binary packages and filelists are not allowed for upload."] = "Binary packages and filelists are not allowed for upload.";

$_t["en"]["Missing arch variable in PKGBUILD."] = "Missing arch variable in PKGBUILD.";

$_t["en"]["Missing license variable in PKGBUILD."] = "Missing license variable in PKGBUILD.";



?>