<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["en"]["Select your language here: %h%s%h, %h%s%h, %h%s%h, %h%s%h."] = "Select your language here: %h%s%h, %h%s%h, %h%s%h, %h%s%h.";

$_t["en"]["Hello, world!"] = "Hello, world!";

$_t["en"]["Hello, again!"] = "Hello, again!";

$_t["en"]["My current language tag is: '%s'."] = "My current language tag is: '%s'.";

?>