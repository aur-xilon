<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["Use this form to update your account."] = "Use este formulario para actualizar su cuenta.";

$_t["es"]["Leave the password fields blank to keep your same password."] = "Deje en blanco los campos de la contraseña para conservarla.";

$_t["es"]["You are not allowed to access this area."] = "No está autorizado a acceder a esta área.";

$_t["es"]["Could not retrieve information for the specified user."] = "No se pudo obtener la información del usuario especificado.";

$_t["es"]["Use this form to search existing accounts."] = "Use este formulario para buscar cuentas existentes.";

$_t["es"]["You do not have permission to edit this account."] = "No tiene permiso para editar esta cuenta.";

$_t["es"]["Use this form to create an account."] = "Use este formulario para crear una cuenta.";

$_t["es"]["You must log in to view user information."] = "Debe identificarse para ver la información del usuario.";

?>