<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["Missing a required field."] = "Falta un campo obligatorio.";

$_t["es"]["Search'"] = "Buscar'";

$_t["es"]["The account, %h%s%h, has been successfully created."] = "La cuenta, %h%s%h, se creó correctamente.";

$_t["es"]["Error trying to modify account, %h%s%h: %s."] = "Error al intentar modificar la cuenta, %h%s%h: %s.";

$_t["es"]["The email address is invalid."] = "La dirección de email no es válida.";

$_t["es"]["Error trying to create account, %h%s%h: %s."] = "Error al intentar crear la cuenta, %h%s%h: %s.";

$_t["es"]["The username, %h%s%h, is already in use."] = "El nombre de usuario, %h%s%h, ya está en uso.";

$_t["es"]["Account Type"] = "Tipo de cuenta";

$_t["es"]["The account, %h%s%h, has been successfully modified."] = "La cuenta, %h%s%h, se ha modificado correctamente.";

$_t["es"]["Account Suspended"] = "Cuenta suspendida";

$_t["es"]["Status"] = "Estado";

$_t["es"]["New Package Notify"] = "Aviso de nuevos paquetes";

$_t["es"]["IRC Nick"] = "Nick del IRC";

$_t["es"]["Trusted user"] = "Usuario de confianza";

$_t["es"]["No results matched your search criteria."] = "No se encontraron resultados que coincidan con su criterio de búsqueda.";

$_t["es"]["Normal user"] = "Usuario normal";

$_t["es"]["Never"] = "Nunca";

$_t["es"]["User"] = "Usuario";

$_t["es"]["Active"] = "Activo";

$_t["es"]["Last Voted"] = "Último voto";

$_t["es"]["Real Name"] = "Nombre real";

$_t["es"]["Edit Account"] = "Editar cuenta";

$_t["es"]["Password fields do not match."] = "Los campos de la contraseña no coinciden.";

$_t["es"]["View this user's packages"] = "Ver los paquetes de este usuario";

$_t["es"]["Language"] = "Idioma";

$_t["es"]["A Trusted User cannot assign Developer status."] = "Un usuario de confianza no puede asignar el estado de desarrollador.";

$_t["es"]["The address, %h%s%h, is already in use."] = "La dirección, %h%s%h, ya está en uso.";

$_t["es"]["No more results to display."] = "No hay más resultados que mostrar.";

$_t["es"]["Type"] = "Tipo";

$_t["es"]["Click on the Home link above to login."] = "Pulse en el enlace Inicio situado en la parte superior para identificarse.";

$_t["es"]["Sort by"] = "Ordenar por";

$_t["es"]["Re-type password"] = "Reescriba la contraseña";

$_t["es"]["Language is not currently supported."] = "El idioma no está soportado actualmente.";

$_t["es"]["Any type"] = "Cualquier tipo";

$_t["es"]["Last vote"] = "Último voto";

$_t["es"]["Suspended"] = "Suspendido";

$_t["es"]["Trusted User"] = "Usuario de Confianza";

$_t["es"]["Missing User ID"] = "Falta el identificador de usuario";

$_t["es"]["Developer"] = "Desarrollador";

?>