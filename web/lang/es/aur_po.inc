<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["%s: %sAn ArchLinux project%s"] = "%s: %sUn proyecto de ArchLinux%s";

$_t["es"]["Logout"] = "Salir";

$_t["es"]["Discussion"] = "Debate";

$_t["es"]["Bugs"] = "Bugs";

$_t["es"]["Accounts"] = "Cuentas";

$_t["es"]["Home"] = "Inicio";

$_t["es"]["Packages"] = "Paquetes";

$_t["es"]["My Packages"] = "Mis paquetes";

?>