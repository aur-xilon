<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["Reset"] = "Limpiar";

$_t["es"]["Username"] = "Nombre de usuario";

$_t["es"]["Email Address"] = "Dirección de email";

$_t["es"]["Less"] = "Menos";

$_t["es"]["Clear"] = "Limpiar";

$_t["es"]["required"] = "obligatorio";

$_t["es"]["Update"] = "Actualizar";

$_t["es"]["Submit"] = "Enviar";

$_t["es"]["Password"] = "Contraseña";

$_t["es"]["Create"] = "Crear";

$_t["es"]["More"] = "Más";

?>
