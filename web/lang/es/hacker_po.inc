<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["Your session id is invalid."] = "Su identificador de sesión no es válido.";

$_t["es"]["If this problem persists, please contact the site administrator."] = "Si el problema persiste, por favor contacte con el administrador del sitio.";

?>