<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["Statistics"] = "Estadísticas";

$_t["es"]["Remember to vote for your favourite packages!"] = "¡Recuerde votar sus paquetes favoritos!";

$_t["es"]["Error looking up username, %s."] = "Error al buscar el usuario, %s.";

$_t["es"]["Packages in unsupported"] = "Paquetes en [unsupported]";

$_t["es"]["The most popular packages will be provided as binary packages in [community]."] = "Los paquetes más populares se distribuirán como paquetes binarios en [community].";

$_t["es"]["Trusted Users"] = "Usuarios de Confianza";

$_t["es"]["You must supply a username."] = "Debe proporcionar un nombre de usuario.";

$_t["es"]["Packages added or updated in the past 7 days"] = "Paquetes añadidos o actualizados en los últimos 7 días";

$_t["es"]["Email discussion about the AUR takes place on the %sTUR Users List%s."] = "Los debates sobre el AUR tienen lugar en la %sLista de correo de los Usuarios TUR%s.";

$_t["es"]["Packages in unsupported and flagged as safe"] = "Paquetes en [unsupported] y marcados como seguros";

$_t["es"]["Though we can't vouch for their contents, we provide a %hlist of user repositories%h for your convenience."] = "Aunque no podemos responder por sus contenidos, suministramos para su comodidad una %hlista de repositorios de usuarios%h.";

$_t["es"]["Packages in [community]"] = "Paquetes en [community]";

$_t["es"]["Recent Updates"] = "Actualizaciones recientes";

$_t["es"]["Your account has been suspended."] = "Su cuenta ha sido suspendida.";

$_t["es"]["Username:"] = "Nombre de usuario:";

$_t["es"]["Error trying to generate session id."] = "Error al intentar crear un identificador de sesión.";

$_t["es"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information."] = "¡Bienvenido al AUR! Por favor lea la %hGuía AUR del Usuario%h y la %hGuía TU del AUR%h para más información.";

$_t["es"]["Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "¡Los PKGBUILDS enviados <b>deben</b> cumplir las %hNormas de empaquetado de Arch%h sino serán eliminados!";

$_t["es"]["Login"] = "Entrar";

$_t["es"]["If you have feedback about the AUR, please leave it in %hFlyspray%h."] = "Si tiene algún comentario sobre el AUR, por favor déjelo en el %hFlyspray%h.";

$_t["es"]["You must supply a password."] = "Debe suministrar una contraseña.";

$_t["es"]["Password:"] = "Contraseña:";

$_t["es"]["Registered Users"] = "Usuarios Registrados";

$_t["es"]["Logged-in as: %h%s%h"] = "Identificado como: %h%s%h";

$_t["es"]["Incorrect password for username, %s."] = "Contraseña incorrecta para el usuario, %s.";

$_t["es"]["Safe"] = "Seguros";

$_t["es"]["Out-of-date"] = "Desactualizados";

$_t["es"]["User Statistics"] = "Estadísticas del Usuario";

?>
