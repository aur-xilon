<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["Missing package ID."] = "Falta el identificador del paquete.";

$_t["es"]["Invalid category ID."] = "El identificador de la categoría no es válido.";

$_t["es"]["Enter your comment below."] = "Introduzca su comentario a continuación.";

$_t["es"]["You are not allowed to delete this comment."] = "No está autorizado a borrar este comentario.";

$_t["es"]["Missing comment ID."] = "Falta el identificador del comentario.";

$_t["es"]["Package category updated."] = "Se ha actualizado la categoría del paquete.";

$_t["es"]["You must be logged in before you can edit package information."] = "Debe identificarse antes de editar la información del paquete.";

$_t["es"]["Comment has been deleted."] = "Comentario borrado.";

$_t["es"]["You've found a bug if you see this...."] = "Si ve esto es que ha encontrado un bug....";

$_t["es"]["Comment has been added."] = "Se ha añadido el comentario.";

$_t["es"]["Select new category"] = "Seleccione una nueva categoría";

?>