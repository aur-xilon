<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["Category"] = "Categoría";

$_t["es"]["Search by"] = "Buscar por";

$_t["es"]["Delete comment"] = "Borrar comentario";

$_t["es"]["Votes"] = "Votos";

$_t["es"]["First Submitted"] = "Primer envío";

$_t["es"]["Tarball"] = "Archivo TAR";

$_t["es"]["Be careful! The above files may contain malicious code that can damage your system."] = "¡Tenga cuidado! Los siguientes ficheros pueden contenter código malévolo que podría dañar su sistema.";

$_t["es"]["Voted"] = "Votado";

$_t["es"]["Location"] = "Ubicación";

$_t["es"]["Flag Safe"] = "Marcar como seguro";

$_t["es"]["Go"] = "Ir";

$_t["es"]["Unflag Out-of-date"] = "Desmarcar Desactualizado";

$_t["es"]["Go back to %hpackage details view%h."] = "Volver a la %hvista detallada del paquete%h.";

$_t["es"]["Error retrieving package details."] = "Error al recuperar los detalles del paquete.";

$_t["es"]["Description"] = "Descripción";

$_t["es"]["My Packages"] = "Mis paquetes";

$_t["es"]["Safe"] = "Seguros";

$_t["es"]["Sort order"] = "Ordenar en sentido";

$_t["es"]["Ascending"] = "Ascendente";

$_t["es"]["Keywords"] = "Palabras clave";

$_t["es"]["No New Comment Notification"] = "Ninguna notificación de nuevo comentario";

$_t["es"]["Dependencies"] = "Dependencias";

$_t["es"]["Descending"] = "Descendente";

$_t["es"]["Per page"] = "Por página";

$_t["es"]["Package Listing"] = "Lista de paquetes";

$_t["es"]["Package details could not be found."] = "Los detalles del paquete no se han podido encontrar.";

$_t["es"]["Package Details"] = "Detalles del paquete";

$_t["es"]["Error retrieving package list."] = "Error al recuperar la lista de paquetes.";

$_t["es"]["Files"] = "Ficheros";

$_t["es"]["None"] = "Ninguno";

$_t["es"]["Name"] = "Nombre";

$_t["es"]["Last Updated"] = "Última actualización";

$_t["es"]["The above files have been verified (by %s) and are safe to use."] = "Los ficheros de arriba han sido verificados (por %s) y son seguros.";

$_t["es"]["Unflag Package Safe To Use"] = "Desmarcar Paquete Seguro";

$_t["es"]["Go back to %hsearch results%h."] = "Volver a %hresultados de la busqueda%h.";

$_t["es"]["Age"] = "Antigüedad";

$_t["es"]["Comments"] = "Comentarios";

$_t["es"]["O%hrphan"] = "H%huérfano";

$_t["es"]["orphan"] = "huérfano";

$_t["es"]["Un-Vote"] = "Quitar Voto";

$_t["es"]["change category"] = "cambiar categoría";

$_t["es"]["UnNotify"] = "Quitar notificación";

$_t["es"]["Delete Packages"] = "Borrar Paquetes";

$_t["es"]["Maintainer"] = "Mantenedor";

$_t["es"]["Add Comment"] = "Añadir Comentario";

$_t["es"]["Comment by: %h%s%h on %h%s%h"] = "Comentario de: %h%s%h el %h%s%h";

$_t["es"]["Flag Out-of-date"] = "Marcar como Desactualizado";

$_t["es"]["Manage"] = "Gestionar";

$_t["es"]["Sort by"] = "Ordenar por";

$_t["es"]["Flag Package Safe To Use"] = "Marcar paquete como Seguro";

$_t["es"]["Actions"] = "Acciones";

$_t["es"]["Unflag Safe"] = "Desmarcar Seguro";

$_t["es"]["Sources"] = "Fuentes";

$_t["es"]["Yes"] = "Sí";

$_t["es"]["Search Criteria"] = "Criterio de Búsqueda";

$_t["es"]["Notify"] = "Notificar";

$_t["es"]["O%hut-of-Date"] = "D%hesactualizado";

$_t["es"]["Vote"] = "Votar";

$_t["es"]["Adopt Packages"] = "Adoptar Paquetes";

$_t["es"]["New Comment Notification"] = "Notificación de nuevo comentario";

$_t["es"]["Disown Packages"] = "Abandonar Paquetes";

$_t["es"]["Orphans"] = "Huérfanos";

$_t["es"]["Any"] = "Cualquiera";

$_t["es"]["No packages matched your search criteria."] = "Ningún paquete coincide con su criterio de búsqueda.";

$_t["es"]["Status"] = "Estado";

$_t["es"]["Leave the password fields blank to keep your same password."] = "Deje en blanco los campos de la contraseña si quiere conservarla.";

$_t["es"]["unknown"] = "desconocido";

$_t["es"]["You have been successfully logged out."] = "Ha abandonado la sesión correctamente.";

$_t["es"]["You must log in to view user information."] = "Debe identificarse para ver la información del usuario.";

$_t["es"]["License"] = "Licencia";

$_t["es"]["Could not retrieve information for the specified user."] = "No se pudo obtener la información del usuario especificado.";

$_t["es"]["You do not have permission to edit this account."] = "No tiene permiso para editar esta cuenta.";

$_t["es"]["Use this form to search existing accounts."] = "Use este formulario para buscar las cuentas existentes.";

$_t["es"]["Submitter"] = "Contribuidor";

$_t["es"]["All"] = "Todos";

$_t["es"]["Use this form to create an account."] = "Use este formulario para crear una cuenta.";

$_t["es"]["Use this form to update your account."] = "Use este formulario para actualizar su cuenta.";

$_t["es"]["You are not allowed to access this area."] = "No está autorizado a acceder a esta área.";

$_t["es"]["Unsafe"] = "Inseguros";

?>
