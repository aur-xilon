<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["None of the selected packages could be deleted."] = "Ninguno de los paquetes seleccionados pudo ser borrado.";

$_t["es"]["Your votes have been removed from the selected packages."] = "Sus votos han sido eliminados de los paquetes seleccionados.";

$_t["es"]["Couldn't flag package safe."] = "No se pudo marcar el paquete como seguro.";

$_t["es"]["You did not select any packages to un-vote for."] = "No seleccionó ningún paquete para quitarle el voto.";

$_t["es"]["The selected packages have been unflagged."] = "Los paquetes seleccionados han sido desmarcados.";

$_t["es"]["You did not select any packages to adopt."] = "No ha seleccionado ningún paquete para ser adoptado.";

$_t["es"]["You must be logged in before you can flag packages."] = "Debe identificarse antes de poder marcar paquetes.";

$_t["es"]["You must be logged in before you can get notifications on comments."] = "Debe identificarse antes de poder recibir notificaciones sobre comentarios.";

$_t["es"]["You must be logged in before you can vote for packages."] = "Debe identificarse antes de poder votar paquetes.";

$_t["es"]["The selected packages have been flagged out-of-date."] = "Los paquetes seleccionados han sido marcados como desactualizados.";

$_t["es"]["The selected packages have been deleted."] = "Los paquetes seleccionados se han borrado.";

$_t["es"]["You did not select any packages to vote for."] = "No seleccionó ningún paquete para votarlo.";

$_t["es"]["You must be logged in before you can disown packages."] = "Debe identificarse antes de poder abandonar paquetes.";

$_t["es"]["Error trying to retrieve package details."] = "Error al intentar recuperar los detalles del paquete.";

$_t["es"]["The selected packages have been adopted."] = "Los paquetes seleccionados han sido adoptados.";

$_t["es"]["You have been removed from the comment notification list."] = "Ha sido borrado de la lista de notificación de comentarios.";

$_t["es"]["Your votes have been cast for the selected packages."] = "Sus votos han sido computados para los paquetes seleccionados.";

$_t["es"]["The selected packages have been unflagged safe."] = "Los paquetes seleccionados han sido desmarcados como seguros.";

$_t["es"]["You must be logged in before you can cancel notification on comments."] = "Debe identificarse antes de poder cancelar las notificaciones sobre comentarios.";

$_t["es"]["You must be logged in before you can adopt packages."] = "Debe indentificarse antes de poder adoptar paquetes.";

$_t["es"]["You have been added to the comment notification list."] = "Ha sido añadido a la lista de notificaciones de comentarios.";

$_t["es"]["You did not select any packages to disown."] = "No seleccionó ningún paquete para ser abandonado.";

$_t["es"]["You must be logged in before you can un-vote for packages."] = "Debe identificarse antes de poder quitar votos a los paquetes";

$_t["es"]["You must be logged in before you can unflag packages."] = "Debe identificarse antes de poder desmarcar paquetes.";

$_t["es"]["You did not select any packages to unflag."] = "No seleccionó ningún paquete para desmarcar.";

$_t["es"]["Couldn't unflag package safe."] = "No se pudo desmarcar el paquete como seguro.";

$_t["es"]["You did not select any packages to delete."] = "No seleccionó ningún paquete para borrar.";

$_t["es"]["Couldn't add to notification list."] = "No se pudo añadir a la lista de notificaciones.";

$_t["es"]["You did not select any packages to flag."] = "No seleccionó ningún paquete para marcar.";

$_t["es"]["The selected packages have been disowned."] = "Los paquetes seleccionados han sido abandonados.";

$_t["es"]["The selected packages have been flagged safe."] = "Los paquetes seleccionados han sido marcados como seguros.";

$_t["es"]["Couldn't remove from notification list."] = "No se pudo borrar de la lista de notificaciones.";

?>