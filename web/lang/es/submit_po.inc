<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["Missing build function in PKGBUILD."] = "Falta la función \"build\" en el PKGBUILD.";

$_t["es"]["Could not change directory to %s."] = "No se pudo cambiar el directorio a %s.";

$_t["es"]["No"] = "No";

$_t["es"]["Missing pkgdesc variable in PKGBUILD."] = "Falta la variable \"pkgdesc\" en el PKGBUILD.";

$_t["es"]["Error trying to upload file - please try again."] = "Error al intentar subir el fichero - por favor vuelva a intentarlo.";

$_t["es"]["Error exec'ing the mv command."] = "Error al ejecutar el comando \"mv\".";

$_t["es"]["You must create an account before you can upload packages."] = "Debe crear una cuenta antes de poder subir paquetes.";

$_t["es"]["Package upload successful."] = "Paquete subido correctamente.";

$_t["es"]["Overwrite existing package?"] = "¿Sobreescribir el paquete existente?";

$_t["es"]["Binary packages and filelists are not allowed for upload."] = "La subida de paquetes binarios y del fichero \"filelist\" no está permitida.";

$_t["es"]["You did not specify a package name."] = "No especificó el nombre del paquete.";

$_t["es"]["Error trying to unpack upload - PKGBUILD does not exist."] = "Error al intentar desempaquetar - no existe el fichero PKGBUILD.";

$_t["es"]["Could not create incoming directory: %s."] = "No se pudo crear el directorio de recepción: %s.";

$_t["es"]["Upload package file"] = "Subir archivo del paquete";

$_t["es"]["Package Location"] = "Ubicación del paquete";

$_t["es"]["Missing url variable in PKGBUILD."] = "Falta la variable \"url\" en el PKGBUILD.";

$_t["es"]["Package names do not match."] = "Los nombres del paquete no coinciden.";

$_t["es"]["Package Category"] = "Categoría del paquete";

$_t["es"]["Could not change to directory %s."] = "No se pudo cambiar al directorio %s.";

$_t["es"]["You did not tag the 'overwrite' checkbox."] = "No marcó la casilla de 'sobreescribir'.";

$_t["es"]["Invalid name: only lowercase letters are allowed."] = "Nombre no válido: sólo se permiten letras minúsculas.";

$_t["es"]["Missing pkgver variable in PKGBUILD."] = "Falta la variable \"pkgver\" en el PKGBUILD.";

$_t["es"]["Package name"] = "Nombre del paquete";

$_t["es"]["Upload"] = "Subir";

$_t["es"]["Missing md5sums variable in PKGBUILD."] = "Falta la variable \"md5sums\" en el PKGBUILD.";

$_t["es"]["Missing pkgrel variable in PKGBUILD."] = "Falta la variable \"pkgrel\" en el PKGBUILD.";

$_t["es"]["Missing pkgname variable in PKGBUILD."] = "Falta la variable \"pkgname\" en el PKGBUILD.";

$_t["es"]["Error - No file uploaded"] = "Error - El fichero no se ha subido";

$_t["es"]["Package URL is missing a protocol (ie. http:// ,ftp://)"] = "Falta el protocolo en la URL del paquete (es decir, http:// ,ftp://)";

$_t["es"]["You are not allowed to overwrite the %h%s%h package."] = "No está autorizado a sobreescribir el paquete %h%s%h.";

$_t["es"]["Select Location"] = "Seleccionar Ubicación";

$_t["es"]["Select Category"] = "Seleccionar Categoría";

$_t["es"]["Comment"] = "Comentario";

$_t["es"]["Could not create directory %s."] = "No se pudo crear el directorio %s.";

$_t["es"]["Unknown file format for uploaded file."] = "Formato de archivo desconocido en el fichero subido.";

$_t["es"]["Missing source variable in PKGBUILD."] = "Falta la variable \"source\" en el PKGBUILD.";

$_t["es"]["Could not re-tar"] = "No se pudo volver a crear el archivo TAR";

$_t["es"]["Sorry, uploads are not permitted by this server."] = "Perdón, las subidas no están permitidas en este servidor.";

$_t["es"]["You must supply a comment for this upload/change."] = "Debe suministrar un comentario para esta subida/cambio.";

$_t["es"]["Yes"] = "Sí";

?>