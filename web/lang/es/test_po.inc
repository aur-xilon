<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["Select your language here: %h%s%h, %h%s%h, %h%s%h, %h%s%h."] = "Seleccione su idioma aquí: %h%s%h, %h%s%h, %h%s%h, %h%s%h.";

$_t["es"]["Hello, world!"] = "¡Hola, mundo!";

$_t["es"]["Hello, again!"] = "¡Hola, otra vez!";

$_t["es"]["My current language tag is: '%s'."] = "Mi identificador actual del idioma es: '%s'.";

?>