<?php
# Spanish (Español) translation
# Translator: Víctor Martínez Romanos <vmromanos@gmail.com>

include_once("translator.inc");
global $_t;

$_t["es"]["Click on the Home link above to log in."] = "Pulse en el enlace de Inicio situado en la parte superior para identificarse.";

$_t["es"]["Your session has timed out.  You must log in again."] = "Su sesión ha expirado. Debe identificarse de nuevo.";

?>