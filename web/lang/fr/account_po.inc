<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["Use this form to update your account."] = "Utilisez ce formulaire pour mettre à jour votre compte.";

$_t["fr"]["Leave the password fields blank to keep your same password."] = "Laissez les champs du mot de passe vides pour conserver le même mot de passe.";

$_t["fr"]["You are not allowed to access this area."] = "Vous n'êtes pas autorisé(e) à accèder à cet espace.";

$_t["fr"]["Could not retrieve information for the specified user."] = "Impossible de retrouver les informations de l'utilisateur spécifié.";

$_t["fr"]["Use this form to search existing accounts."] = "Utilisez ce formulaire pour rechercher des comptes existants.";

$_t["fr"]["You do not have permission to edit this account."] = "Vous n'avez pas l'autorisation d'éditer ce compte.";

$_t["fr"]["Use this form to create an account."] = "Utilisez ce formulaire pour créer un compte.";

$_t["fr"]["You must log in to view user information."] = "Vous devez vous authentifier pour voir les informations de l'utilisateur.";

?>
