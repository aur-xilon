<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["Missing a required field."] = "Il manque un champ requis.";

$_t["fr"]["Search'"] = "Rechercher'";

$_t["fr"]["The account, %h%s%h, has been successfully created."] = "Le compte, %h%s%h, a été créé avec succès.";

$_t["fr"]["Error trying to modify account, %h%s%h: %s."] = "Erreur en essayant de modifier le compte, %h%s%h: %s.";

$_t["fr"]["The email address is invalid."] = "L'adresse email n'est pas valide.";

$_t["fr"]["Error trying to create account, %h%s%h: %s."] = "Erreur en essayant de créer le compte, %h%s%h: %s.";

$_t["fr"]["The username, %h%s%h, is already in use."] = "Le nom d'utilisateur, %h%s%h, est déjà utilisé.";

$_t["fr"]["Account Type"] = "Type de Compte";

$_t["fr"]["The account, %h%s%h, has been successfully modified."] = "Le compte, %h%s%h, a été modifié avec succès.";

$_t["fr"]["Account Suspended"] = "Compte Suspendu";

$_t["fr"]["Status"] = "Etat";

$_t["fr"]["New Package Notify"] = "Annonce d'un nouveau paquet";

$_t["fr"]["IRC Nick"] = "Pseudo IRC";

$_t["fr"]["Trusted user"] = "Utilisateur de confiance";

$_t["fr"]["No results matched your search criteria."] = "Aucun résultat ne correspond à vos critères de recherche.";

$_t["fr"]["Normal user"] = "Utilisateur normal";

$_t["fr"]["Never"] = "Jamais";

$_t["fr"]["User"] = "Utilisateur";

$_t["fr"]["Active"] = "Actif";

$_t["fr"]["Last Voted"] = "Voté Dernièrement";

$_t["fr"]["Real Name"] = "Nom Réel";

$_t["fr"]["Edit Account"] = "Editer le Compte";

$_t["fr"]["Password fields do not match."] = "Les champs du mot de passe ne correspondent pas.";

$_t["fr"]["View this user's packages"] = "Visualiser les paquets de cet utilisateur.";

$_t["fr"]["Language"] = "Langue";

$_t["fr"]["A Trusted User cannot assign Developer status."] = "Un Utilisateur de Confiance ne peut pas assigner le status de Développeur.";

$_t["fr"]["The address, %h%s%h, is already in use."] = "L'adresse, %h%s%h, est déjà utilisée.";

$_t["fr"]["No more results to display."] = "Plus de résultats à afficher.";

$_t["fr"]["Type"] = "Type";

$_t["fr"]["Click on the Home link above to login."] = "Cliquez sur le lien ci-dessus pour vous authentifier.";

$_t["fr"]["Sort by"] = "Trier par";

$_t["fr"]["Re-type password"] = "Retapez le mot de passe";

$_t["fr"]["Language is not currently supported."] = "Cette langue n'est pas supportée pour le moment.";

$_t["fr"]["Any type"] = "Tout type";

$_t["fr"]["Last vote"] = "Dernier vote";

$_t["fr"]["Suspended"] = "Suspendu";

$_t["fr"]["Trusted User"] = "Utilisateur de Confiance";

$_t["fr"]["Missing User ID"] = "ID d'utilisateur manquant";

$_t["fr"]["Developer"] = "Développeur";

?>