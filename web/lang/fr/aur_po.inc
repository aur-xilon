<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["My Packages"] = "Mes paquets";

$_t["fr"]["%s: %sAn ArchLinux project%s"] = "%s: %sUn projet Archlinux%s";

$_t["fr"]["Logout"] = "Déconnexion";

$_t["fr"]["Discussion"] = "Discussion";

$_t["fr"]["Bugs"] = "Bugs";

$_t["fr"]["Accounts"] = "Comptes";

$_t["fr"]["Home"] = "Accueil";

$_t["fr"]["Packages"] = "Paquets";

?>