<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["Reset"] = "Réinitialiser";

$_t["fr"]["Username"] = "Nom d'utilisateur";

$_t["fr"]["Email Address"] = "Adresse Email";

$_t["fr"]["Less"] = "Moins";

$_t["fr"]["Clear"] = "Effacer";

$_t["fr"]["required"] = "requis";

$_t["fr"]["Update"] = "Mise à jour";

$_t["fr"]["Submit"] = "Soumettre";

$_t["fr"]["Password"] = "Mot de passe";

$_t["fr"]["Create"] = "Créer";

$_t["fr"]["More"] = "Plus";

?>