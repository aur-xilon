<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["Your session id is invalid."] = "Votre identifiant de session n'est pas valide.";

$_t["fr"]["If this problem persists, please contact the site administrator."] = "Si ce problème persiste, contactez s'il vous plaît l'administrateur du site.";

?>