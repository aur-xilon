<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["Statistics"] = "Statistiques";

$_t["fr"]["Remember to vote for your favourite packages!"] = "Pensez à voter pour vos paquets favoris!";

$_t["fr"]["Error looking up username, %s."] = "Erreur en recherchant l'utilisateur, %s.";

$_t["fr"]["Packages in unsupported"] = "Paquets dans non supportés";

$_t["fr"]["The most popular packages will be provided as binary packages in [community]."] = "Les paquets les plus populaires seront mis à disposition sous forme de paquets binaires dans [community].";

$_t["fr"]["Trusted Users"] = "Utilisateurs de Confiance";

$_t["fr"]["You must supply a username."] = "Vous devez fournir un nom d'utilisateur.";

$_t["fr"]["Packages added or updated in the past 7 days"] = "Paquets ajoutés ou mis à jour durant les 7 derniers jours";

$_t["fr"]["Email discussion about the AUR takes place on the %sTUR Users List%s."] = "Les discussions par email au sujet d'AUR ont lieu sur la %sliste des Utilisateurs de Confiance%s.";

$_t["fr"]["Packages in unsupported and flagged as safe"] = "Paquets dans non supportés et marqués comme sains";

$_t["fr"]["Though we can't vouch for their contents, we provide a %hlist of user repositories%h for your convenience."] = "Bien que nous ne puissions garantir leur contenu, nous fournissons une %hliste de dépôts d'utilisateurs%h à votre convenance.";

$_t["fr"]["Packages in [community]"] = "Paquets dans [community]";

$_t["fr"]["Recent Updates"] = "Mises à jour récentes";

$_t["fr"]["Your account has been suspended."] = "Votre compte a été suspendu.";

$_t["fr"]["Username:"] = "Nom d'utilisateur:";

$_t["fr"]["Error trying to generate session id."] = "Erreur en essayant de générer un identifiant de session.";

$_t["fr"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information."] = "Bienvenu(e) sur AUR! Lisez, s'il vous plaît, le %hGuide Utilisateur AUR%h et le %hGuide des Utilisateurs de Confiance%h pour plus d'informations.";

$_t["fr"]["Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "Les PKGBUILDs portés à contribution <b>doivent</b> se conformer aux %hStandards de l'empaquetage Arch%h ou sinon ils seront supprimés!";

$_t["fr"]["Login"] = "Se connecter";

$_t["fr"]["If you have feedback about the AUR, please leave it in %hFlyspray%h."] = "Si vous avez des remarques à propos d'AUR, laissez-les s'il vous plaît dans %hFlyspray%h.";

$_t["fr"]["You must supply a password."] = "Vous devez fournir un mot de passe.";

$_t["fr"]["Password:"] = "Mot de passe:";

$_t["fr"]["Registered Users"] = "Utilisateurs enregistrés";

$_t["fr"]["Logged-in as: %h%s%h"] = " Connecté en tant que: %h%s%h";

$_t["fr"]["Incorrect password for username, %s."] = "Mot de passe incorrect pour l'utilisateur, %s.";

$_t["fr"]["Safe"] = "Sain";

$_t["fr"]["Out-of-date"] = "Périmé";

$_t["fr"]["User Statistics"] = "Statistiques Utilisateur";

?>