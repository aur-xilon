<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["Missing package ID."] = "ID de paquet manquant.";

$_t["fr"]["Invalid category ID."] = "ID de catégorie non valide.";

$_t["fr"]["Enter your comment below."] = "Entrez votre commentaire ci-dessous.";

$_t["fr"]["You are not allowed to delete this comment."] = "Vous n'êtes pas autorisé(e) à supprimer ce commentaire.";

$_t["fr"]["Missing comment ID."] = "ID de commentaire manquant.";

$_t["fr"]["Package category updated."] = "Catégorie du paquet mise à jour.";

$_t["fr"]["You must be logged in before you can edit package information."] = "Vous devez être authentifié(e) avant de pouvoir éditer les informations du paquet.";

$_t["fr"]["Comment has been deleted."] = "Le commentaire a été supprimé.";

$_t["fr"]["You've found a bug if you see this...."] = "Vous avez trouvé un bug si vous voyez ceci....";

$_t["fr"]["Comment has been added."] = "Le commentaire a été ajouté.";

$_t["fr"]["Select new category"] = "Sélectionnez une nouvelle catégorie";

?>