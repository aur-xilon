<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["Category"] = "Catégorie";

$_t["fr"]["Search by"] = "Rechercher par";

$_t["fr"]["Comments"] = "Commentaires";

$_t["fr"]["orphan"] = "orphelin";

$_t["fr"]["Votes"] = "Votes";

$_t["fr"]["First Submitted"] = "Première Soumission";

$_t["fr"]["Tarball"] = "Tarball";

$_t["fr"]["Be careful! The above files may contain malicious code that can damage your system."] = "Attention! Les fichiers ci-dessus peuvent contenir du code malicieux qui pourrait endommager votre système.";

$_t["fr"]["Voted"] = "Voté";

$_t["fr"]["Location"] = "Dépôt";

$_t["fr"]["Flag Safe"] = "Marquer comme Sain";

$_t["fr"]["Unflag Package Safe To Use"] = "Retirer l'étiquette Sain du paquet";

$_t["fr"]["Unflag Out-of-date"] = "Retirer l'étiquette Périmé";

$_t["fr"]["Go back to %hpackage details view%h."] = "Retourner à la %hvue des détails du paquet%h.";

$_t["fr"]["Error retrieving package details."] = "Erreur en recherchant les détails du paquet.";

$_t["fr"]["Leave the password fields blank to keep your same password."] = "Laissez les champs du mot de passe vides pour conserver le même mot de passe.";

$_t["fr"]["Description"] = "Description";

$_t["fr"]["My Packages"] = "Mes paquets";

$_t["fr"]["Safe"] = "Sain";

$_t["fr"]["You have been successfully logged out."] = "Vous avez été déconnecté avec succès.";

$_t["fr"]["Ascending"] = "Ascendant";

$_t["fr"]["Delete comment"] = "Effacer le commentaire";

$_t["fr"]["No New Comment Notification"] = "Pas d'annonce de Nouveau Commentaire";

$_t["fr"]["Dependencies"] = "Dépendances";

$_t["fr"]["Descending"] = "Descendant";

$_t["fr"]["Per page"] = "Par page";

$_t["fr"]["Package Listing"] = "Liste des Paquets";

$_t["fr"]["Package details could not be found."] = "Les détails du paquet ne peuvent pas être trouvés.";

$_t["fr"]["You must log in to view user information."] = "Vous devez vous authentifier pour voir les informations de l'utilisateur.";

$_t["fr"]["Package Details"] = "Détails du Paquet";

$_t["fr"]["Error retrieving package list."] = "Erreur en recherchant la liste des paquets.";

$_t["fr"]["Files"] = "Fichiers";

$_t["fr"]["Name"] = "Nom";

$_t["fr"]["Last Updated"] = "Dernière mise à jour";

$_t["fr"]["The above files have been verified (by %s) and are safe to use."] = "Les fichiers ci-dessus ont été vérifiés (par %s) et sont sains.";

$_t["fr"]["Could not retrieve information for the specified user."] = "Impossible de retrouver l'information pour l'utilisateur spécifié.";

$_t["fr"]["UnNotify"] = "Ne plus annoncer";

$_t["fr"]["You do not have permission to edit this account."] = "Vous n'avez pas la permission d'éditer ce compte.";

$_t["fr"]["Keywords"] = "Mots clés";

$_t["fr"]["Age"] = "Age";

$_t["fr"]["Use this form to search existing accounts."] = "Utilisez ce formulaire pour rechercher des comptes existants.";

$_t["fr"]["Submitter"] = "Contributeur";

$_t["fr"]["Use this form to create an account."] = "Utilisez ce formulaire pour créer un compte.";

$_t["fr"]["Un-Vote"] = "Retirer mon vote";

$_t["fr"]["change category"] = "changer de catégorie";

$_t["fr"]["Vote"] = "Voter";

$_t["fr"]["Use this form to update your account."] = "Utiliser ce formulaire pour mettre à jour votre compte.";

$_t["fr"]["Delete Packages"] = "Supprimer des Paquets";

$_t["fr"]["Maintainer"] = "Mainteneur";

$_t["fr"]["Add Comment"] = "Ajouter un Commentaire";

$_t["fr"]["Comment by: %h%s%h on %h%s%h"] = "Commenté par: %h%s%h sur %h%s%h";

$_t["fr"]["Flag Out-of-date"] = "Marqué comme Périmé";

$_t["fr"]["You are not allowed to access this area."] = "Vous n'êtes pas authorisé à accéder à cet espace.";

$_t["fr"]["Manage"] = "Gérer";

$_t["fr"]["Sort by"] = "Trier par";

$_t["fr"]["Flag Package Safe To Use"] = "Marquer le paquet comme sains";

$_t["fr"]["Actions"] = "Actions";

$_t["fr"]["Unflag Safe"] = "Retirer l'étiquette Sain";

$_t["fr"]["Disown Packages"] = "Abandonner les paquets";

$_t["fr"]["Sources"] = "Sources";

$_t["fr"]["Yes"] = "Oui";

$_t["fr"]["Search Criteria"] = "Critères de recherche";

$_t["fr"]["Notify"] = "Annoncer";

$_t["fr"]["Go"] = "Aller";

$_t["fr"]["O%hut-of-Date"] = "P%hérimé";

$_t["fr"]["Go back to %hsearch results%h."] = "Retourner aux %hrésultats de la recherche%h.";

$_t["fr"]["Adopt Packages"] = "Adopter des paquets";

$_t["fr"]["New Comment Notification"] = "Annonce de Nouveau Commentaire";

$_t["fr"]["Sort order"] = "Ordre de tri";

$_t["fr"]["Orphans"] = "Orphelins";

$_t["fr"]["Any"] = "Tous";

$_t["fr"]["No packages matched your search criteria."] = "Aucun paquet ne correspond à vos critères de recherche.";

$_t["fr"]["Status"] = "Etat";

$_t["fr"]["License"] = "Licence";

$_t["fr"]["All"] = "Tout";

$_t["fr"]["Unsafe"] = "Non sûr";

$_t["fr"]["unknown"] = "inconnu";

?>