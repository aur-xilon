<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["None of the selected packages could be deleted."] = "Aucun des paquets sélectionnés ne pourrait être effacé.";

$_t["fr"]["Your votes have been removed from the selected packages."] = "Vos votes ont été retirés des paquets sélectionnés.";

$_t["fr"]["Couldn't flag package safe."] = "Ce paquet ne peut être marqué comme sain.";

$_t["fr"]["You did not select any packages to un-vote for."] = "Vous n'avez sélectionné aucun paquet dont le vote doit être retiré.";

$_t["fr"]["The selected packages have been unflagged."] = "Les paquets sélectionnés ont été \"dé-marqués\".";

$_t["fr"]["You did not select any packages to adopt."] = "Vous n'avez pas sélectionné de paquet à adopter.";

$_t["fr"]["You must be logged in before you can flag packages."] = "Vous devez être authentifié avant de pouvoir marquer des paquets.";

$_t["fr"]["You must be logged in before you can get notifications on comments."] = "Vous devez être authentifié avant de pouvoir recevoir des annonces de commentaires.";

$_t["fr"]["You must be logged in before you can vote for packages."] = "Vous devez être authentifié avant de pouvoir voter pour des paquets.";

$_t["fr"]["The selected packages have been flagged out-of-date."] = "Les paquets sélectionnés ont été marqués  comme périmés.";

$_t["fr"]["The selected packages have been deleted."] = "Les paquets sélectionnés ont été supprimés.";

$_t["fr"]["You did not select any packages to vote for."] = "Vous n'avez sélectionné aucun paquet pour lequel vous souhaitez voter.";

$_t["fr"]["You must be logged in before you can disown packages."] = "Vous devez être authentifié avant de pouvoir abandonner des paquets.";

$_t["fr"]["Error trying to retrieve package details."] = "Erreur en essayant de retrouvÃer les détails du paquets.";

$_t["fr"]["The selected packages have been adopted."] = "Les paquets sélectionnés ont été adoptés.";

$_t["fr"]["You have been removed from the comment notification list."] = "Vous avez été retiré de la liste de notification des commentaires.";

$_t["fr"]["Your votes have been cast for the selected packages."] = "Vos votes ont été adistribué aux paquets sélectionnés.";

$_t["fr"]["The selected packages have been unflagged safe."] = "Les paquets sélectionnés ont été \"dé-marqués\" comme sains.";

$_t["fr"]["You must be logged in before you can cancel notification on comments."] = "Vous devez être authentifié avant de pouvoir annuler les annonces sur les commentaires.";

$_t["fr"]["You must be logged in before you can adopt packages."] = "Vous devez être authentifié avant de pouvoir adopter des paquets.";

$_t["fr"]["You have been added to the comment notification list."] = "Vous avez été ajouté à la liste de notification des commentaires.";

$_t["fr"]["You did not select any packages to disown."] = "Vous n'avez sélectionné aucun paquet à abandonner.";

$_t["fr"]["You must be logged in before you can un-vote for packages."] = "Vous devez être authentifié avant de pouvoir retiré votre vote sur des paquets.";

$_t["fr"]["You must be logged in before you can unflag packages."] = "Vous devez être authentifié avant de pouvoir \"dé-marquer\" des paquets.";

$_t["fr"]["You did not select any packages to unflag."] = "Vous n'avez sélectionné aucun paquet à \"dé-marquer\".";

$_t["fr"]["Couldn't unflag package safe."] = "Ce paquet ne peut être \"dé-marqué\" come sain.";

$_t["fr"]["You did not select any packages to delete."] = "Vous n'avez sélectionné aucun paquet à supprimer.";

$_t["fr"]["Couldn't add to notification list."] = " Ajout impossible à la liste de notification.";

$_t["fr"]["You did not select any packages to flag."] = "Vous n'avez sélectionné aucun paquet à marquer.";

$_t["fr"]["The selected packages have been disowned."] = "Les paquets sélectionnés ont été abandonné.";

$_t["fr"]["The selected packages have been flagged safe."] = "Les paquets sélectionnés ont été marqué comme sains.";

$_t["fr"]["Couldn't remove from notification list."] = "Suppression impossible de la liste de notification.";

?>