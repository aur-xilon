<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["Missing build function in PKGBUILD."] = "Fonction build manquante dans le PKGBUILD.";

$_t["fr"]["Could not change directory to %s."] = "Ne peut changer le répertoire vers %s.";

$_t["fr"]["No"] = "Non";

$_t["fr"]["Missing pkgdesc variable in PKGBUILD."] = "La variable pkgdesc est manquante dans le PKGBUILD.";

$_t["fr"]["Error trying to upload file - please try again."] = "Erreur en essayant de transmettre le fichier - s'il vous plaît, essayez encore.";

$_t["fr"]["Error exec'ing the mv command."] = "Erreur en essayant d'exécuter la commande mv.";

$_t["fr"]["You must create an account before you can upload packages."] = "Vous devez créer un compte avant de pouvoir transférer des paquets.";

$_t["fr"]["Package upload successful."] = "Transfert du paquet réussi.";

$_t["fr"]["Overwrite existing package?"] = "Ecraser le paquet existant?";

$_t["fr"]["Binary packages and filelists are not allowed for upload."] = "Les paquets binaires et les listes de fichiers ne sont pas autoriÃsés à être transferés.";

$_t["fr"]["You did not specify a package name."] = "Vous n'avez pas spécifié de nom de paquet.";

$_t["fr"]["Error trying to unpack upload - PKGBUILD does not exist."] = "Erreur en essayant de décompresser le fichier tranféré - PKGBUILD n'existe pas.";

$_t["fr"]["Could not create incoming directory: %s."] = "Impossible de créer le répertoire entrant: %s.";

$_t["fr"]["Upload package file"] = "Fichier àdu paquet à tranférer";

$_t["fr"]["Package Location"] = " Chemin du paquet";

$_t["fr"]["Missing url variable in PKGBUILD."] = "La variable url est manquante dans le PKGBUILD.";

$_t["fr"]["Package names do not match."] = "Les noms de paquets ne correspondent pas.";

$_t["fr"]["Package Category"] = "Catégorie du paquet";

$_t["fr"]["Could not change to directory %s."] = "Impossible de changer de répertoire vers %s.";

$_t["fr"]["You did not tag the 'overwrite' checkbox."] = "Vous n'avez pas coché la case 'Ecraser'.";

$_t["fr"]["Invalid name: only lowercase letters are allowed."] = "Nom invalide: seules les lettres minuscules sont autorisées.";

$_t["fr"]["Missing pkgver variable in PKGBUILD."] = "La variable pkgver est manquante dans le PKGBUILD.";

$_t["fr"]["Package name"] = "Nom du paquet";

$_t["fr"]["Upload"] = "Transfert";

$_t["fr"]["Missing md5sums variable in PKGBUILD."] = "La variable md5sums est manquante dans le PKGBUILD.";

$_t["fr"]["Missing pkgrel variable in PKGBUILD."] = "La variable pkgrel est manquante dans le PKGBUILD.";

$_t["fr"]["Missing pkgname variable in PKGBUILD."] = "La variable pkgname est manquante dans le PKGBUILD.";

$_t["fr"]["Error - No file uploaded"] = "Erreur - Aucun fichier transféré";

$_t["fr"]["Package URL is missing a protocol (ie. http:// ,ftp://)"] = "L'URL du paquet ne contient pas de protocole (ex. http:// ,ftp://)";

$_t["fr"]["You are not allowed to overwrite the %h%s%h package."] = "Vous n'êtes pas autorisé(e) à écraser le paquet %h%s%h.";

$_t["fr"]["Select Location"] = "Sélectionnez un emplacement";

$_t["fr"]["Select Category"] = "Sélectionnez une catégorie";

$_t["fr"]["Comment"] = "Commentaire";

$_t["fr"]["Could not create directory %s."] = "Impossible de créer le répertoire %s.";

$_t["fr"]["Unknown file format for uploaded file."] = "Format de fichier inconnu pour le fichier transféré.";

$_t["fr"]["Missing source variable in PKGBUILD."] = "Variable source manquante dans le PKGBUILD.";

$_t["fr"]["Could not re-tar"] = "Impossible de 'exécuter à nouveau la commande tar";

$_t["fr"]["Sorry, uploads are not permitted by this server."] = "Désolé, les transferts ne sont pas permis par ce serveur.";

$_t["fr"]["You must supply a comment for this upload/change."] = "Vous devez fournir un commentaire pour ce transfert/changement.";

$_t["fr"]["Yes"] = "Oui";

?>