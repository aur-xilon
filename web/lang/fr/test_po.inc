<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["Select your language here: %h%s%h, %h%s%h, %h%s%h, %h%s%h."] = "Choisissez votre langue ici: %h%s%h, %h%s%h, %h%s%h, %h%s%h.";

$_t["fr"]["Hello, world!"] = "Salut tout le monde!";

$_t["fr"]["Hello, again!"] = "Salut encore!";

$_t["fr"]["My current language tag is: '%s'."] = "Mon choix actuel de langue est: '%s'.";

?>