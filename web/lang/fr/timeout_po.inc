<?php
# French (Français) translation
# Translator: Morgan LEFIEUX <comete@archlinuxfr.org>

include_once("translator.inc");
global $_t;

$_t["fr"]["Click on the Home link above to log in."] = "Cliquez sur le lien ci-dessus pour vous authentifier.";

$_t["fr"]["Your session has timed out.  You must log in again."] = "Votre session a expiré. Vous devez vous ré-authentifier.";

?>