<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/hacker_po.inc");

include_once("pl/hacker_po.inc");

include_once("it/hacker_po.inc");

include_once("ca/hacker_po.inc");

include_once("pt/hacker_po.inc");

include_once("es/hacker_po.inc");

include_once("de/hacker_po.inc");

include_once("ru/hacker_po.inc");

include_once("fr/hacker_po.inc");

?>