<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["Use this form to update your account."] = "Utilizzare questo modulo per aggiornare un account.";

$_t["it"]["Leave the password fields blank to keep your same password."] = "Lasciare vuoti i campi relativi alla password per mantenerla invariata.";

$_t["it"]["You are not allowed to access this area."] = "Non si dispone dei permessi necessari per accedere.";

$_t["it"]["Could not retrieve information for the specified user."] = "Non è stato possibile trovare le informazioni sull'utente specificato.";

$_t["it"]["Use this form to search existing accounts."] = "Utilizzare questo modulo per cercare account esistenti.";

$_t["it"]["You do not have permission to edit this account."] = "Non si dispone dei permessi necessari per modificare questo account.";

$_t["it"]["Use this form to create an account."] = "Utilizzare questo modulo per creare un account.";

$_t["it"]["You must log in to view user information."] = "Bisogna essere autenticati per poter visualizzare le informazioni sull'utente.";

?>