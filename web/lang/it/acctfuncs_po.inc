<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["Missing a required field."] = "Manca un campo obbligatorio.";

$_t["it"]["Search'"] = "Cerca'";

$_t["it"]["The account, %h%s%h, has been successfully created."] = "L'account %h%s%h è stato creato con successo.";

$_t["it"]["Error trying to modify account, %h%s%h: %s."] = "Errore durante la modifica dell'account %h%s%h: %s.";

$_t["it"]["The email address is invalid."] = "L'indirizzo email non risulta valido.";

$_t["it"]["Error trying to create account, %h%s%h: %s."] = "Errore durante la creazione dell'account %h%s%h: %s.";

$_t["it"]["The username, %h%s%h, is already in use."] = "Il nome utente %h%s%h è già in uso.";

$_t["it"]["Account Type"] = "Tipo di account";

$_t["it"]["The account, %h%s%h, has been successfully modified."] = "L'account %h%s%h è stato modificato con successo.";

$_t["it"]["Account Suspended"] = "Account sospeso";

$_t["it"]["Status"] = "Stato";

$_t["it"]["New Package Notify"] = "Notifica dei nuovi pacchetti";

$_t["it"]["IRC Nick"] = "Nick IRC";

$_t["it"]["Trusted user"] = "Trusted user";

$_t["it"]["No results matched your search criteria."] = "Nessun pacchetto corrisponde ai criteri della ricerca.";

$_t["it"]["Normal user"] = "Utente normale";

$_t["it"]["Never"] = "Mai";

$_t["it"]["User"] = "Utente";

$_t["it"]["Active"] = "Attivo";

$_t["it"]["Last Voted"] = "Ultimo votato";

$_t["it"]["Real Name"] = "Nome reale";

$_t["it"]["Edit Account"] = "Modificare l'account";

$_t["it"]["Password fields do not match."] = "I campi password non corrispondono.";

$_t["it"]["Language"] = "Lingua";

$_t["it"]["A Trusted User cannot assign Developer status."] = "Un Trusted User non può assegnare lo stato di sviluppatore.";

$_t["it"]["The address, %h%s%h, is already in use."] = "L'indirizzo %h%s%h è già in uso.";

$_t["it"]["No more results to display."] = "Non vi sono ulteriori risultati da visualizzare.";

$_t["it"]["Type"] = "Tipo";

$_t["it"]["Click on the Home link above to login."] = "Fare clic su Inizio per autenticarsi.";

$_t["it"]["Sort by"] = "Ordina per";

$_t["it"]["Re-type password"] = "Riscrivere la password";

$_t["it"]["Language is not currently supported."] = "Lingua attualmente non supportata.";

$_t["it"]["Any type"] = "Qualsiasi tipo";

$_t["it"]["Last vote"] = "Ultimo voto";

$_t["it"]["Suspended"] = "Sospeso";

$_t["it"]["Trusted User"] = "Trusted User";

$_t["it"]["Missing User ID"] = "Manca l'User ID";

$_t["it"]["Developer"] = "Sviluppatore";

$_t["it"]["View this user's packages"] = "Visualizza i pacchetti di quest'utente";

?>