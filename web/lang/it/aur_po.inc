<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["Home"] = "Inizio";

$_t["it"]["%s: An ArchLinux project"] = "%s: Un progetto ArchLinux";

$_t["it"]["Packages"] = "Pacchetti";

$_t["it"]["Accounts"] = "Account";

$_t["it"]["Logout"] = "Esci";

$_t["it"]["%s: %sAn ArchLinux project%s"] = "%s: %sUn progetto ArchLinux%s";

$_t["it"]["Discussion"] = "Lista di discussione";

$_t["it"]["Bugs"] = "Bug";

$_t["it"]["My Packages"] = "I miei pacchetti";

?>