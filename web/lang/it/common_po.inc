<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["Reset"] = "Cancella";

$_t["it"]["Username"] = "Nome utente";

$_t["it"]["Email Address"] = "Indirizzo email";

$_t["it"]["Less"] = "Precedente";

$_t["it"]["Clear"] = "Pulisci";

$_t["it"]["required"] = "obbligatorio";

$_t["it"]["Update"] = "Aggiorna";

$_t["it"]["Submit"] = "Invia";

$_t["it"]["Password"] = "Password";

$_t["it"]["Create"] = "Crea";

$_t["it"]["More"] = "Successivo";

?>