<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["Your session id is invalid."] = "L'id di sessione non è valido.";

$_t["it"]["If this problem persists, please contact the site administrator."] = "Se questo problema dovesse persistere, contattare l'amministratore del sito.";

?>