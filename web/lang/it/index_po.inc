<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["You must supply a password."] = "E' necessario inserire una password.";

$_t["it"]["You must supply a username."] = "E' necessario inserire il nome utente.";

$_t["it"]["After that, this can be filled in with more meaningful text."] = "Dopotutto, questo potrebbe essere riempito con un messaggio più significativo.";

$_t["it"]["Logged-in as: %h%s%h"] = "Autenticato con il nome utente: %h%s%h";

$_t["it"]["Your account has been suspended."] = "L'account è stato sospeso.";

$_t["it"]["Password:"] = "Password:";

$_t["it"]["Username:"] = "Nome utente:";

$_t["it"]["Welcome to the AUR! If you're a newcomer, you may want to read the %hGuidelines%h."] = "Benvenuto in AUR! Se sei un nuovo utente, dovresti leggere le %hGuidelines%h.";

$_t["it"]["This is where the intro text will go."] = "Qui ci andrà il testo di introduzione.";

$_t["it"]["Error trying to generate session id."] = "Errore durante la generazione dell'id di sessione.";

$_t["it"]["For now, it's just a place holder."] = "Per adesso, è solo un segnaposto.";

$_t["it"]["It's more important to get the login functionality finished."] = "E' più importante avere la funzionalità di autenticazione completata.";

$_t["it"]["Error looking up username, %s."] = "Errore durante la ricerca del nome utente %s.";

$_t["it"]["Login"] = "Entra";

$_t["it"]["Though we can't vouch for their contents, we provide a %hlist of user repositories%h for your convenience."] = "Nonostante che non possiamo assicurarvi il loro contenuto, mettiamo a disposizione una %hlista di repositories degli utenti%h per vostra comodità";

$_t["it"]["If you have feedback about the AUR, please leave it in %hFlyspray%h."] = "Se avete delle osservazioni da fare in merito al sistema AUR, potete lasciarle nel %hFlyspray%h.";

$_t["it"]["Incorrect password for username, %s."] = "Password errata per il nome utente %s.";

$_t["it"]["Latest Packages:"] = "Ultimi pacchetti:";

$_t["it"]["Discussion about the AUR takes place on the %sTUR Users List%s."] = "Le discussioni su AUR avvengono nella %sTUR Users List%s.";

$_t["it"]["Email discussion about the AUR takes place on the %sTUR Users List%s."] = "Email per le discussioni su AUR che avvengono nella %sTUR Users List%s.";

$_t["it"]["Recent Updates"] = "Aggiornamenti recenti";

$_t["it"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information. Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "Benvenuto in AUR! Per ottenere maggiori informazioni, leggere %hAUR User Guidelines%h e %hAUR TU Guidelines%h. I PKGBUILD inviati <b>devono</b> essere conformi agli %hArch Packaging Standards%h altrimenti saranno cancellati!";

$_t["it"]["Community"] = "Community";

$_t["it"]["Package Counts"] = "Conteggio dei pacchetti";

$_t["it"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information."] = "Benvenuto in AUR! Per ottenere maggiori informazioni, leggere %hAUR User Guidelines%h e %hAUR TU Guidelines%h.";

$_t["it"]["Unsupported"] = "Unsupported";

$_t["it"]["Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "I PKGBUILD inviati <b>devono</b> essere conformi agli %hArch Packaging Standards%h altrimenti saranno cancellati!";

$_t["it"]["Statistics"] = "Statistiche";

$_t["it"]["Registered Users"] = "Utenti registrati";

$_t["it"]["Trusted Users"] = "Trusted Users";

$_t["it"]["Packages in unsupported"] = "Pacchetti in unsupported";

$_t["it"]["Packages in [community]"] = "Pacchetti in [community]";

$_t["it"]["Remember to vote for your favourite packages! The most popular packages are provided as binary packages in [community]."] = "Ricordate di votare i vostri pacchetti preferiti! I pacchetti più votati sono disponibili in [community] come precompilati.";

$_t["it"]["Remember to vote for your favourite packages!"] = "Ricordate di votare i vostri pacchetti preferiti!";

$_t["it"]["The most popular packages will be provided as binary packages in [community]."] = "I pacchetti più votati saranno disponibili in [community] come precompilati.";

$_t["it"]["Packages added or updated in the past 7 days"] = "Pacchetti aggiunti o aggiornati negli ultimi 7 giorni";

$_t["it"]["Packages in unsupported and flagged as safe"] = "Pacchetti in unsupported considerati sicuri";

$_t["it"]["Safe"] = "Sicuri";

$_t["it"]["Out-of-date"] = "Non aggiornati";

$_t["it"]["User Statistics"] = "Statistiche dell'utente";

$_t["it"]["Flagged as safe by me"] = "Segnato come sicuro da me";

$_t["it"]["Flagged as safe"] = "Segnato come sicuro";

$_t["it"]["My Statistics"] = "Le mie statistiche";

?>