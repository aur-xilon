<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["Missing package ID."] = "Manca l'ID del pacchetto.";

$_t["it"]["Invalid category ID."] = "ID della categoria non valido.";

$_t["it"]["Enter your comment below."] = "Inserire un commento qui sotto.";

$_t["it"]["You are not allowed to delete this comment."] = "Non è possibile cancellare questo commento.";

$_t["it"]["Missing comment ID."] = "Manca l'ID del commento.";

$_t["it"]["Package category updated."] = "La categoria del pacchetto è stata aggiornata.";

$_t["it"]["You must be logged in before you can edit package information."] = "Bisogna autenticarsi prima di poter modificare le informazioni sul pacchetto.";

$_t["it"]["Comment has been deleted."] = "Il commento è stato cancellato.";

$_t["it"]["You've found a bug if you see this...."] = "Se vedi questo, hai appena trovato un bug....";

$_t["it"]["Comment has been added."] = "Il commento è stato aggiunto.";

$_t["it"]["Select new category"] = "Selezionare una nuova categoria";

?>