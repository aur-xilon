<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["Category"] = "Categoria";

$_t["it"]["Votes"] = "Voti";

$_t["it"]["Comment by: %h%s%h on %h%s%h"] = "Commento lasciato da: %h%s%h il %h%s%h";

$_t["it"]["Location"] = "Posizione";

$_t["it"]["Delete comment"] = "Eliminare il commento";

$_t["it"]["Go"] = "Cerca";

$_t["it"]["Unflag Out-of-date"] = "Segnare come aggiornato";

$_t["it"]["Go back to %hpackage details view%h."] = "Ritornare alla %hvista dettagli del pacchetto%h.";

$_t["it"]["Error retrieving package details."] = "Errore nel recupero dei dettagli del pacchetto.";

$_t["it"]["Description"] = "Descrizione";

$_t["it"]["My Packages"] = "I miei pacchetti";

$_t["it"]["Keywords"] = "Parola chiave";

$_t["it"]["Dependencies"] = "Dipendenze";

$_t["it"]["Disown Packages"] = "Abbandonare i pacchetti";

$_t["it"]["Package details could not be found."] = "Impossibile trovare i dettagli del pacchetto.";

$_t["it"]["Package Details"] = "Dettagli del pacchetto";

$_t["it"]["Error retrieving package list."] = "Errore nel recupero della lista dei pacchetti.";

$_t["it"]["Files"] = "File";

$_t["it"]["None"] = "Nessuno";

$_t["it"]["Name"] = "Nome";

$_t["it"]["Per page"] = "Per pagina";

$_t["it"]["Go back to %hsearch results%h."] = "Ritornare ai %hrisultati della ricerca%h.";

$_t["it"]["No packages matched your search criteria."] = "Nessun pacchetto corrisponde ai criteri della ricerca.";

$_t["it"]["O%hrphan"] = "O%hrfano";

$_t["it"]["orphan"] = "orfano";

$_t["it"]["Un-Vote"] = "Rimuovere il voto";

$_t["it"]["change category"] = "Cambiare categoria";

$_t["it"]["UnNotify"] = "Togliere la notifica";

$_t["it"]["Delete Packages"] = "Eliminare i pacchetti";

$_t["it"]["Maintainer"] = "Responsabile";

$_t["it"]["Add Comment"] = "Aggiungere un commento";

$_t["it"]["Tarball"] = "Archivio";

$_t["it"]["Flag Out-of-date"] = "Segnare come non aggiornato";

$_t["it"]["Manage"] = "Organizza";

$_t["it"]["Sort by"] = "Ordina per";

$_t["it"]["Actions"] = "Azioni";

$_t["it"]["Sources"] = "Sorgenti";

$_t["it"]["Search Criteria"] = "Criteri di ricerca";

$_t["it"]["Notify"] = "Notifica";

$_t["it"]["O%hut-of-Date"] = "N%hon aggiornato";

$_t["it"]["Vote"] = "Vota";

$_t["it"]["Adopt Packages"] = "Adottare i pacchetti";

$_t["it"]["Yes"] = "Si";

$_t["it"]["Package Listing"] = "Lista dei pacchetti";

$_t["it"]["Orphans"] = "Orfani";

$_t["it"]["Any"] = "Qualsiasi";

$_t["it"]["Voted"] = "Votato";

$_t["it"]["No New Comment Notification"] = "Nessuna notifica di nuovo commento";

$_t["it"]["New Comment Notification"] = "Notifica di nuovo commento";

$_t["it"]["Comments"] = "Commenti";

$_t["it"]["The above files have been verified (by %s) and are safe to use."] = "I file sono stati verificati (da %s) ed il loro utilizzo è da considerarsi sicuro.";

$_t["it"]["Be careful! The above files may contain malicious code that can damage your system."] = "Attenzione! I file potrebbero contenere codice pericoloso in grado di danneggiare il vostro sistema.";

$_t["it"]["Flag Safe"] = "Segnare come sicuro";

$_t["it"]["Flag Package Safe To Use"] = "Segnare il pacchetto come sicuro da utilizzare";

$_t["it"]["Unflag Safe"] = "Segnare come non sicuro";

$_t["it"]["Unflag Package Safe To Use"] = "Segnare il pacchetto come non sicuro da utilizzare";

$_t["it"]["Safe"] = "Sicuri";

$_t["it"]["Age"] = "Data";

$_t["it"]["First Submitted"] = "Primo invio";

$_t["it"]["Last Updated"] = "Ultimo aggiornamento";

$_t["it"]["Sort order"] = "Ordina in modo";

$_t["it"]["Ascending"] = "Ascendente";

$_t["it"]["Descending"] = "Discendente";

$_t["it"]["Search by"] = "Cerca per";

$_t["it"]["Submitter"] = "Contributore";

$_t["it"]["Leave the password fields blank to keep your same password."] = "Lasciare vuoti i campi relativi alla password per mantenerla invariata.";

$_t["it"]["You have been successfully logged out."] = "Disconnesso.";

$_t["it"]["You must log in to view user information."] = "Bisogna autenticarsi per visualizzare queste informazioni.";

$_t["it"]["Could not retrieve information for the specified user."] = "Non è stato possibile trovare le informazioni sull'utente specificato.";

$_t["it"]["You do not have permission to edit this account."] = "Non si dispone dei permessi necessari per modificare questo account.";

$_t["it"]["Use this form to search existing accounts."] = "Utilizzare questo modulo per cercare account esistenti.";

$_t["it"]["Use this form to create an account."] = "Utilizzare questo modulo per creare un account.";

$_t["it"]["Use this form to update your account."] = "Utilizzare questo modulo per aggiornare il vostro account.";

$_t["it"]["You are not allowed to access this area."] = "Non si dispone dei permessi necessari per accedere.";

$_t["it"]["Status"] = "Stato";

$_t["it"]["unknown"] = "sconosciuta";

$_t["it"]["License"] = "Licenza";

$_t["it"]["All"] = "Tutti";

$_t["it"]["Unsafe"] = "Non sicuri";

?>