<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["None of the selected packages could be deleted."] = "Nessuno dei pacchetti selezionati può essere cancellato.";

$_t["it"]["Your votes have been removed from the selected packages."] = "I voti sono stati rimossi dai pacchetti selezionati.";

$_t["it"]["You did not select any packages to un-vote for."] = "Non è stato selezionato nessun pacchetto a cui rimuovere il voto.";

$_t["it"]["The selected packages have been unflagged."] = "I pacchetti selezionati non sono più contrassegnati.";

$_t["it"]["You did not select any packages to adopt."] = "Non è stato selezionato nessun pacchetto da adottare.";

$_t["it"]["You must be logged in before you can flag packages."] = "Bisogna autenticarsi prima di poter contrassegnare i pacchetti.";

$_t["it"]["You must be logged in before you can get notifications on comments."] = "Bisogna autenticarsi prima di poter ricevere le notifiche sui commenti.";

$_t["it"]["You must be logged in before you can vote for packages."] = "Bisogna autenticarsi prima di poter votare i pacchetti.";

$_t["it"]["The selected packages have been flagged out-of-date."] = "I pacchetti selezionati sono stati contrassegnati come non aggiornati.";

$_t["it"]["The selected packages have been deleted."] = "I pacchetti selezionati sono stati eliminati.";

$_t["it"]["You did not select any packages to vote for."] = "Non è stato selezionato nessun pacchetto a cui assegnare il voto.";

$_t["it"]["You must be logged in before you can disown packages."] = "Bisogna autenticarsi prima di poter abbandonare i pacchetti.";

$_t["it"]["Error trying to retrieve package details."] = "Errore durante il recupero dei dettagli del pacchetto.";

$_t["it"]["The selected packages have been adopted."] = "I pacchetti selezionati sono stati adottati.";

$_t["it"]["You have been removed from the comment notification list."] = "Sei stato rimosso dalla lista di notifica dei commenti.";

$_t["it"]["Your votes have been cast for the selected packages."] = "I voti sono stati assegnati ai pacchetti selezionati.";

$_t["it"]["You must be logged in before you can cancel notification on comments."] = "Bisogna autenticarsi prima di poter cancellare le notifiche sui commenti.";

$_t["it"]["You must be logged in before you can adopt packages."] = "Bisogna autenticarsi prima di poter adottare dei pacchetti.";

$_t["it"]["You have been added to the comment notification list."] = "Sei stato aggiunto alla lista di notifica dei commenti.";

$_t["it"]["You did not select any packages to disown."] = "Non è stato selezionato nessun pacchetto da abbandonare.";

$_t["it"]["You must be logged in before you can un-vote for packages."] = "Bisogna autenticarsi prima di poter rimuovere il voto ai pacchetti.";

$_t["it"]["You must be logged in before you can unflag packages."] = "Bisogna autenticarsi prima di poter rimuovere il contrassegno ai pacchetti.";

$_t["it"]["You did not select any packages to unflag."] = "Non è stato selezionato nessun pacchetto da segnare come aggiornato.";

$_t["it"]["You did not select any packages to delete."] = "Non è stato selezionato nessun pacchetto da eliminare.";

$_t["it"]["Couldn't add to notification list."] = "Impossibile aggiungere alla lista di notifica.";

$_t["it"]["You did not select any packages to flag."] = "Non è stato selezionato nessun pacchetto da segnare come non aggiornato.";

$_t["it"]["The selected packages have been disowned."] = "I pacchetti selezionati sono stati abbandonati.";

$_t["it"]["Couldn't remove from notification list."] = "Impossibile rimuovere dalla lista di notifica.";

$_t["it"]["The selected packages have been flagged safe."] = "I pacchetti selezionati sono stati contrassegnati come sicuri.";

$_t["it"]["Couldn't flag package safe."] = "Impossibile contrassegnare il pacchetto come sicuro.";

$_t["it"]["The selected packages have been unflagged safe."] = "I pacchetti selezionati sono stati contrassegnati come non sicuri.";

$_t["it"]["Couldn't unflag package safe."] = "Impossibile contrassegnare il pacchetto come non sicuro.";

?>