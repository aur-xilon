<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["Missing build function in PKGBUILD."] = "Manca la funzione build nel PKGBUILD.";

$_t["it"]["Could not change directory to %s."] = "Impossibile cambiare la directory in %s.";

$_t["it"]["No"] = "No";

$_t["it"]["Missing pkgdesc variable in PKGBUILD."] = "Manca la variabile pkgdesc nel PKGBUILD.";

$_t["it"]["Error trying to upload file - please try again."] = "Errore durante l'invio del file. Riprovare.";

$_t["it"]["Error exec'ing the mv command."] = "Errore nell'esecuzione del comando mv.";

$_t["it"]["You must create an account before you can upload packages."] = "Bisogna creare un account prima di poter inviare i pacchetti.";

$_t["it"]["Package upload successful."] = "Invio completato con successo.";

$_t["it"]["Overwrite existing package?"] = "Sovrascrivere il pacchetto esistente?";

$_t["it"]["You did not specify a package name."] = "Non è stato specificato un nome per il pacchetto.";

$_t["it"]["Error trying to unpack upload - PKGBUILD does not exist."] = "Errore nello scompattare il file inviato. Non esiste il PKGBUILD.";

$_t["it"]["Could not create incoming directory: %s."] = "Impossibile creare la directory %s.";

$_t["it"]["Upload package file"] = "Pacchetto da inviare";

$_t["it"]["Package Location"] = "Posizione del pacchetto";

$_t["it"]["Missing url variable in PKGBUILD."] = "Manca la variabile url nel PKGBUILD.";

$_t["it"]["Package names do not match."] = "Il nome del pacchetto non corrisponde.";

$_t["it"]["Package Category"] = "Categoria del pacchetto";

$_t["it"]["Could not change to directory %s."] = "Impossibile cambiare la directory in %s.";

$_t["it"]["You did not tag the 'overwrite' checkbox."] = "Non è stata selezionata l'opzione 'sovrascrivi'.";

$_t["it"]["Invalid name: only lowercase letters are allowed."] = "Nome non valido: sono consentite solo lettere minuscole.";

$_t["it"]["Missing pkgver variable in PKGBUILD."] = "Manca la variabile pkgver nel PKGBUILD.";

$_t["it"]["Package name"] = "Nome del pacchetto";

$_t["it"]["Upload"] = "Invia";

$_t["it"]["Missing md5sums variable in PKGBUILD."] = "Manca la variabile md5sums nel PKGBUILD.";

$_t["it"]["Missing pkgrel variable in PKGBUILD."] = "Manca la variabile pkgrel nel PKGBUILD.";

$_t["it"]["Missing pkgname variable in PKGBUILD."] = "Manca la variabile pkgname nel PKGBUILD.";

$_t["it"]["Error - No file uploaded"] = "Errore, nessun file inviato.";

$_t["it"]["You are not allowed to overwrite the %h%s%h package."] = "Non si dispone dei permessi per poter sovrascrivere il pacchetto %h%s%h.";

$_t["it"]["Select Location"] = "Selezionare la posizione";

$_t["it"]["Select Category"] = "Selezionare la categoria";

$_t["it"]["Comment"] = "Commento";

$_t["it"]["Could not create directory %s."] = "Impossibile creare la directory %s.";

$_t["it"]["Unknown file format for uploaded file."] = "Il file inviato ha un formato sconosciuto.";

$_t["it"]["Missing source variable in PKGBUILD."] = "Manca la variabile source nel PKGBUILD.";

$_t["it"]["Sorry, uploads are not permitted by this server."] = "Spiacente, gli invii non sono consentiti da questo server.";

$_t["it"]["You must supply a comment for this upload/change."] = "Bisogna aggiungere un commento per questo cambiamento/invio.";

$_t["it"]["Yes"] = "Si";

$_t["it"]["Package URL is missing a protocol (ie. http:// ,ftp://)"] = "Manca un protocollo nell'url del pacchetto (es. http:// ,ftp://)";

$_t["it"]["Could not re-tar"] = "Impossibile ri-archiviare";

$_t["it"]["Binary packages and filelists are not allowed for upload."] = "Non è consentito inviare pacchetti contenenti binari e filelist.";

$_t["it"]["Missing license variable in PKGBUILD."] = "Nel PKGBUILD manca la variabile license.";

$_t["it"]["Missing arch variable in PKGBUILD."] = "Nel PKGBUILD manca la variabile arch.";

?>