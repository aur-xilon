<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["Select your language here: %h%s%h, %h%s%h, %h%s%h, %h%s%h."] = "Scegli la tua lingua qui: %h%s%h, %h%s%h, %h%s%h, %h%s%h.";

$_t["it"]["Hello, world!"] = "Ciao, mondo!";

$_t["it"]["Hello, again!"] = "Ciao, di nuovo!";

$_t["it"]["My current language tag is: '%s'."] = "Il tag della mia lingua corrente è: '%s'.";

?>