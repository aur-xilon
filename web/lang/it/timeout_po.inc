<?php
# Italian (Italiano) translation
# Translators: Giovanni Scafora <linuxmania@gmail.com> and Pierluigi Picciau <pierluigi88@gmail.com>

include_once("translator.inc");
global $_t;

$_t["it"]["Click on the Home link above to log in."] = "Fare clic su Inizio per autenticarsi.";

$_t["it"]["Your session has timed out.  You must log in again."] = "La sessione è scaduta. Autenticarsi di nuovo.";

?>