<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/logout_po.inc");

include_once("pl/logout_po.inc");

include_once("it/logout_po.inc");

include_once("ca/logout_po.inc");

include_once("pt/logout_po.inc");

include_once("es/logout_po.inc");

include_once("de/logout_po.inc");

include_once("ru/logout_po.inc");

?>