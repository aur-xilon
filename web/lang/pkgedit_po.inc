<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/pkgedit_po.inc");

include_once("pl/pkgedit_po.inc");

include_once("it/pkgedit_po.inc");

include_once("ca/pkgedit_po.inc");

include_once("pt/pkgedit_po.inc");

include_once("es/pkgedit_po.inc");

include_once("de/pkgedit_po.inc");

include_once("ru/pkgedit_po.inc");

include_once("fr/pkgedit_po.inc");

?>