<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/pkgfuncs_po.inc");

include_once("pl/pkgfuncs_po.inc");

include_once("it/pkgfuncs_po.inc");

include_once("ca/pkgfuncs_po.inc");

include_once("pt/pkgfuncs_po.inc");

include_once("es/pkgfuncs_po.inc");

include_once("de/pkgfuncs_po.inc");

include_once("ru/pkgfuncs_po.inc");

include_once("fr/pkgfuncs_po.inc");

?>