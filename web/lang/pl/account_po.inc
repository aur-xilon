<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["Under construction..."] = "Prace trwają...";
$_t["pl"]["Account Suspended:"] = "Konto zablokowane:";
$_t["pl"]["Notify:"] = "Powiadomienie:";
$_t["pl"]["Trusted User"] = "Zaufany Użytkownik";
$_t["pl"]["Normal User"] = "Zwykły użytkownik";
$_t["pl"]["Password:"] = "Hasło:";
$_t["pl"]["Email Address:"] = "Adres e-mail:";
$_t["pl"]["Confirm:"] = "Potwierdź:";
$_t["pl"]["Real Name:"] = "Imię i nazwisko:";
$_t["pl"]["Account Type:"] = "Rodzaj konta:";
$_t["pl"]["IRC Nick:"] = "Nick na IRC-u:";
$_t["pl"]["Language:"] = "Język:";
$_t["pl"]["Developer"] = "Developer";
$_t["pl"]["New Package Notify:"] = "Powiadamiaj o nowych pakietach:";
$_t["pl"]["Password fields do not match."] = "Hasła nie zgadzają się.";
$_t["pl"]["Error trying to create account, %h%s%h: %s."] = "Błąd podczas tworzenia konta %h%s%h: %s.";
$_t["pl"]["Missing a required field."] = "Brakuje wymaganego pola.";
$_t["pl"]["This address is already in use."] = "Ten adres jest już używany.";
$_t["pl"]["Language is not currently supported."] = "Język nie jest obecnie obsługiwany.";
$_t["pl"]["The email address is invalid."] = "Adres e-mail jest nieprawidłowy.";
$_t["pl"]["Re-type password:"] = "Hasło (ponownie):";
$_t["pl"]["The account, %h%s%h, has been successfully created."] = "Konto %h%s%h zostało pomyślnie utworzone.";
$_t["pl"]["Click on the Home link above to login."] = "Kliknij na linku Start powyżej aby się zalogować.";
$_t["pl"]["The address, %h%s%h, is already in use."] = "Adres %h%s%h jest już używany.";
$_t["pl"]["Trusted user"] = "Zaufany Użytkownik";
$_t["pl"]["Normal user"] = "Zwykły użytkownik";
$_t["pl"]["Any type"] = "Dowolny rodzaj";
$_t["pl"]["No results matched your search criteria."] = "Wyszukiwanie nie przyniosło rezultatu.";
$_t["pl"]["Never"] = "Nigdy";
$_t["pl"]["Active"] = "Aktywne";
$_t["pl"]["Suspended"] = "Zablokowane";
$_t["pl"]["The username, %h%s%h, is already in use."] = "Nazwa użytkownika %h%s%h jest już używana.";
$_t["pl"]["Type"] = "Rodzaj";
$_t["pl"]["Status"] = "Status";
$_t["pl"]["IRC Nick"] = "Nick na IRC-u";
$_t["pl"]["Last Voted"] = "Ostatni głos";
$_t["pl"]["Real Name"] = "Imię i nazwisko";
$_t["pl"]["Username:"] = "Nazwa użytkownika:";
$_t["pl"]["Sort by"] = "Sortuj według";
$_t["pl"]["Account Type"] = "Rodzaj konta";
$_t["pl"]["Account Suspended"] = "Konto zablokowane";
$_t["pl"]["Email address"] = "Adres e-mail";
$_t["pl"]["Last vote"] = "Ostatni głos";
$_t["pl"]["Regular users can edit their own account."] = "Zwykli użytkownicy mogą edytować swoje konto.";
$_t["pl"]["Edit Account"] = "Edytuj konto";
$_t["pl"]["Use this form to search existing accounts."] = "Przy użyciu tego formularza możesz przeszukać istniejące konta.";
$_t["pl"]["You are not allowed to access this area."] = "Nie masz uprawnień do oglądania tej strony.";
$_t["pl"]["Use this form to create an account."] = "Przy użyciu tego formularza możesz utworzyć konto.";
$_t["pl"]["Use this form to update your account."] = "Przy użyciu tego formularza możesz uaktualnić swoje konto.";
$_t["pl"]["Leave the password fields blank to keep your same password."] = "Pozostaw pola z hasłem puste aby nie zmieniać swojego hasła.";
$_t["pl"]["Could not retrieve information for the specified user."] = "Uzyskanie informacji o podanym użytkowniku nie powiodło się.";
$_t["pl"]["You do not have permission to edit this account."] = "Nie masz uprawnień do edycji tego konta.";

$_t["pl"]["You must log in to view user information."] = "Musisz być zalogowany aby móc oglądać informacje o użytkownikach.";

?>
