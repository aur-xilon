<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["Missing a required field."] = "Brakuje wymaganego pola.";
$_t["pl"]["The account, %h%s%h, has been successfully created."] = "Konto %h%s%h zostało pomyślnie utworzone.";
$_t["pl"]["Error trying to modify account, %h%s%h: %s."] = "Błąd podczas modyfikacji konta %h%s%h: %s.";
$_t["pl"]["The email address is invalid."] = "Adres e-mail jest nieprawidłowy.";
$_t["pl"]["Error trying to create account, %h%s%h: %s."] = "Błąd podczas tworzenia konta %h%s%h: %s.";
$_t["pl"]["The username, %h%s%h, is already in use."] = "Nazwa użytkownika %h%s%h jest już używana.";
$_t["pl"]["Account Type"] = "Rodzaj konta";
$_t["pl"]["The account, %h%s%h, has been successfully modified."] = "Konto %h%s%h zostało pomyślnie zaktualizowane.";
$_t["pl"]["Account Suspended"] = "Konto zablokowane";
$_t["pl"]["New Package Notify"] = "Powiadamiaj o nowych pakietach";
$_t["pl"]["IRC Nick"] = "Nick na IRC-u";
$_t["pl"]["Trusted user"] = "Zaufany Użytkownik";
$_t["pl"]["Normal user"] = "Zwykły użytkownik";
$_t["pl"]["Real Name"] = "Imię i nazwisko";
$_t["pl"]["Password fields do not match."] = "Hasła nie zgadzają się.";
$_t["pl"]["Language"] = "Język";
$_t["pl"]["The address, %h%s%h, is already in use."] = "Adres %h%s%h jest już używany.";
$_t["pl"]["Click on the Home link above to login."] = "Kliknij na linku Start powyżej aby się zalogować.";
$_t["pl"]["Re-type password"] = "Hasło (ponownie)";
$_t["pl"]["Language is not currently supported."] = "Język nie jest obecnie obsługiwany.";
$_t["pl"]["Missing User ID"] = "Brakuje ID użytkownika";
$_t["pl"]["Developer"] = "Developer";
$_t["pl"]["Search'"] = "Szukaj'";
$_t["pl"]["Status"] = "Status";
$_t["pl"]["No results matched your search criteria."] = "Wyszukiwanie nie przyniosło rezultatu.";
$_t["pl"]["Never"] = "Nigdy";
$_t["pl"]["Active"] = "Aktywne";
$_t["pl"]["Last Voted"] = "Ostatni głos";
$_t["pl"]["Edit Account"] = "Edytuj konto";
$_t["pl"]["Email address"] = "Adres e-mail";
$_t["pl"]["Type"] = "Rodzaj";
$_t["pl"]["Sort by"] = "Sortuj według";
$_t["pl"]["Any type"] = "Dowolny rodzaj";
$_t["pl"]["Last vote"] = "Ostatni głos";
$_t["pl"]["Suspended"] = "Zablokowane";
$_t["pl"]["No more results to display."] = "Brak rezultatów do wyświetlenia.";
$_t["pl"]["A Trusted User cannot assign Developer status."] = "Zaufany Użytkownik nie może nadać statusu Developera.";

$_t["pl"]["User"] = "Użytkownik";

$_t["pl"]["Trusted User"] = "Zaufany Użytkownik";

$_t["pl"]["View this user's packages"] = "Wyświetl pakiety tego użytkownika";

?>