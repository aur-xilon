<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["ArchLinux User-community Repository"] = "Repozytorium Społeczności Użytkowników ArchLinux";
$_t["pl"]["AUR: An ArchLinux project"] = "AUR: Projekt ArchLinux";
$_t["pl"]["Logout"] = "Wyloguj się";
$_t["pl"]["Manage"] = "Zarządzaj";
$_t["pl"]["Submit"] = "Wyślij";
$_t["pl"]["%s: An ArchLinux project"] = "%s: Projekt ArchLinux";
$_t["pl"]["Accounts"] = "Konta";
$_t["pl"]["Vote"] = "Głosuj";
$_t["pl"]["Home"] = "Start";
$_t["pl"]["Packages"] = "Pakiety";
$_t["pl"]["Trusted user"] = "Zaufany Użytkownik";
$_t["pl"]["User"] = "Użytkownik";
$_t["pl"]["Developer"] = "Developer";

$_t["pl"]["%s: %sAn ArchLinux project%s"] = "%s: %sProjekt ArchLinux%s";

$_t["pl"]["Discussion"] = "Dyskusja";

$_t["pl"]["Bugs"] = "Błędy";

$_t["pl"]["My Packages"] = "Moje pakiety";

?>