<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["Password"] = "Hasło";
$_t["pl"]["Clear"] = "Wyczyść";
$_t["pl"]["required"] = "wymagane";
$_t["pl"]["Email Address"] = "Adres e-mail";
$_t["pl"]["Submit"] = "Wyślij";
$_t["pl"]["Reset"] = "Wyczyść";
$_t["pl"]["Create"] = "Utwórz";
$_t["pl"]["Update"] = "Aktualizuj";
$_t["pl"]["Less"] = "Poprzednie";
$_t["pl"]["More"] = "Następne";
$_t["pl"]["Username"] = "Użytkownik";

?>