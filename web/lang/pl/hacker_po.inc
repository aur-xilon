<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["Your session id is invalid."] = "Twój identyfikator sesji jest nieprawidłowy.";
$_t["pl"]["If this problem persists, please contact the site administrator."] = "Jeżeli ten problem się powtarza, skontaktuj się z administratorem strony.";

?>