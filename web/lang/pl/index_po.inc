<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["Hi, this is worth reading!"] = "Witaj, warto to przeczytać!";
$_t["pl"]["You must supply a password."] = "Musisz podać hasło.";
$_t["pl"]["You must supply a username."] = "Musisz podać nazwę użytkownika.";
$_t["pl"]["Incorrect password for username %s."] = "Nieprawidłowe hasło dla użytkownika %s.";
$_t["pl"]["After that, this can be filled in with more meaningful text."] = "Potem można tu wstawić bardziej wyczerpujący tekst.";
$_t["pl"]["Your account has been suspended."] = "Twoje konto zostało zablokowane.";
$_t["pl"]["Password:"] = "Hasło:";
$_t["pl"]["Username:"] = "Użytkownik:";
$_t["pl"]["It's more important to get the login functionality finished."] = "Ważniejsze jest dokończyć logowanie.";
$_t["pl"]["Currently logged in as: %h%s%h"] = "Obecnie zalogowany jako: %h%s%h";
$_t["pl"]["For now, it's just a place holder."] = "Narazie to tylko wypełnia przestrzeń.";
$_t["pl"]["This is where the intro text will go."] = "Tutaj będzie tekst powitalny.";
$_t["pl"]["Error trying to generate session id."] = "Błąd podczas generowania identyfikatora sesji.";
$_t["pl"]["Login"] = "Zaloguj się";
$_t["pl"]["Email Address:"] = "Adres e-mail:";
$_t["pl"]["You must supply an email address."] = "Musisz podać adres e-mail.";
$_t["pl"]["Incorrect password for email address, %s."] = "Nieprawidłowe hasło dla adresu %s.";
$_t["pl"]["Incorrect password for username, %s."] = "Nieprawidłowe hasło dla użytkownika %s.";
$_t["pl"]["Logged in as: %h%s%h"] = "Zalogowany jako: %h%s%h";
$_t["pl"]["Logged-in as: %h%s%h"] = "Zalogowany jako: %h%s%h";
$_t["pl"]["Error looking up username, %s."] = "Błąd podczas wyszukiwania użytkownika %s.";
$_t["pl"]["Welcome to the AUR! If you're a newcomer, you may want to read the %hGuidelines%h."] = "Witamy w AUR! Jeżeli jesteś tu po raz pierwszy, być może zechcesz przeczytać %hInstrukcję%h.";
$_t["pl"]["If you have feedback about the AUR, please leave it in %hFlyspray%h."] = "Jeżeli masz uwagi lub pomysły odnośnie AUR, %hFlyspray%h jest odpowiednim miejscem do ich pozostawienia.";
$_t["pl"]["Though we can't vouch for their contents, we provide a %hlist of user repositories%h for your convenience."] = "Dla Twojej wygody podajemy %hlistę repozytoriów użytkowników%h, ale uwaga - nie odpowiadamy za ich zawartość.";

$_t["pl"]["Email discussion about the AUR takes place on the %sTUR Users List%s."] = "Dyskusja na temat AUR ma miejsce na liście pocztowej %sTUR Users%s.";

$_t["pl"]["Recent Updates"] = "Ostatnie aktualizacje";

$_t["pl"]["Statistics"] = "Statystyki";

$_t["pl"]["Remember to vote for your favourite packages!"] = "Nie zapomnij głosować na swoje ulubione pakiety!";

$_t["pl"]["Packages in unsupported"] = "Pakietów w unsupported";

$_t["pl"]["Packages in unsupported and flagged as safe"] = "Pakietów w unsupported oznaczonych jako bezpieczne";

$_t["pl"]["The most popular packages will be provided as binary packages in [community]."] = "Najbardziej popularne pakiety zostaną umieszczone w formie binarnej w [community].";

$_t["pl"]["Trusted Users"] = "Zaufanych Użytkowników";

$_t["pl"]["Packages added or updated in the past 7 days"] = "Pakietów dodanych lub zmienionych w ostatnich 7 dniach";

$_t["pl"]["Packages in [community]"] = "Pakietów w [community]";

$_t["pl"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information."] = "Witamy w AUR! Aby uzyskać więcej informacji, przeczytaj %hInstrukcję Użytkownika%h oraz %hInstrukcję Zaufanego Użytkownika%h.";

$_t["pl"]["Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "Pliki PKGBUILD <b>muszą</b> być zgodne ze %hStandardami Pakietów Archa%h, inaczej będą usuwane!";

$_t["pl"]["Registered Users"] = "Zarejestrowanych użytkowników";

$_t["pl"]["Safe"] = "Bezpieczne";

$_t["pl"]["Out-of-date"] = "Nieaktualnych";

$_t["pl"]["User Statistics"] = "Statystyki użytkowników";

$_t["pl"]["Flagged as safe by me"] = "Oznaczonych jako bezpieczne przeze mnie";

$_t["pl"]["Flagged as safe"] = "Oznaczonych jako bezpieczne";

$_t["pl"]["My Statistics"] = "Moje statystyki";

?>