<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["You must be logged in before you can edit package information."] = "Musisz być zalogowany aby móc edytować informacje o pakiecie.";
$_t["pl"]["Missing package ID."] = "Brakuje identyfikatora pakietu.";
$_t["pl"]["Comment has been deleted."] = "Komentarz został usunięty.";
$_t["pl"]["You are not allowed to delete this comment."] = "Nie masz uprawnień do usunięcia tego komentarza.";
$_t["pl"]["Missing comment ID."] = "Brakuje identyfikatora komentarza.";
$_t["pl"]["Comment has been added."] = "Komentarz został dodany.";
$_t["pl"]["Enter your comment below."] = "Napisz swój komentarz poniżej.";
$_t["pl"]["Submit"] = "Wyślij";
$_t["pl"]["Reset"] = "Wyczyść";
$_t["pl"]["Package category updated."] = "Kategoria pakietu zmieniona.";
$_t["pl"]["Invalid category ID."] = "Nieprawidłowy identyfikator kategorii.";
$_t["pl"]["Select new category"] = "Wybierz nową kategorię";
$_t["pl"]["You've found a bug if you see this...."] = "Jeżeli to widzisz, to znalazłeś błąd...";

?>