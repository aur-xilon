<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["Go back to %hpackage details view%h."] = "Powrót do %hinformacji o pakiecie%h.";
$_t["pl"]["None"] = "Brak";
$_t["pl"]["change category"] = "zmień kategorię";
$_t["pl"]["Delete comment"] = "Usuń komentarz";
$_t["pl"]["Comment by: %h%s%h on %h%s%h"] = "Komentarz - autor: %h%s%h, data: %h%s%h";
$_t["pl"]["Add Comment"] = "Dodaj komentarz";
$_t["pl"]["Comments"] = "Komentarze";
$_t["pl"]["Sources"] = "Źródła";
$_t["pl"]["Dependencies"] = "Zależności";
$_t["pl"]["Package Details"] = "Informacje o pakiecie";
$_t["pl"]["Category"] = "Kategoria";
$_t["pl"]["Maintainer"] = "Opiekun";
$_t["pl"]["Name"] = "Nazwa";
$_t["pl"]["Per page"] = "Na stronie";
$_t["pl"]["Popularity"] = "Popularność";
$_t["pl"]["Sort by"] = "Sortuj według";
$_t["pl"]["Sort order"] = "Porządek";
$_t["pl"]["Ascending"] = "Rosnąco";
$_t["pl"]["Descending"] = "Malejąco";
$_t["pl"]["Search Criteria"] = "Kryteria wyszukiwania";
$_t["pl"]["Location"] = "Lokacja";
$_t["pl"]["Keywords"] = "Słowa kluczowe";
$_t["pl"]["Any"] = "Dowolna";
$_t["pl"]["Votes"] = "Głosów";
$_t["pl"]["Description"] = "Opis";
$_t["pl"]["No packages matched your search criteria."] = "Żaden pakiet nie spełnia podanych kryteriów.";
$_t["pl"]["My Packages"] = "Moje pakiety";
$_t["pl"]["Go"] = "Szukaj";
$_t["pl"]["Out-of-date"] = "Nieaktualny";
$_t["pl"]["Flag Out-of-date"] = "Zaznacz jako nieaktualny";
$_t["pl"]["Actions"] = "Działania";
$_t["pl"]["Adopt Packages"] = "Przejmij pakiety";
$_t["pl"]["Disown Packages"] = "Porzuć pakiety";
$_t["pl"]["Delete Packages"] = "Usuń pakiety";
$_t["pl"]["You did not select any packages to delete."] = "Nie wybrałeś żadnych pakietów do usunięcia.";
$_t["pl"]["None of the selected packages could be deleted."] = "Żaden z wybranych pakietów nie mógł być usunięty.";
$_t["pl"]["The selected packages have been deleted."] = "Wybrane pakiety zostały usunięte.";
$_t["pl"]["Vote"] = "Głosuj";
$_t["pl"]["Voted"] = "Głos";
$_t["pl"]["Error retrieving package details."] = "Błąd podczas pobierania informacji o pakiecie.";
$_t["pl"]["Package details could not be found."] = "Nie odnaleziono informacji o pakiecie.";
$_t["pl"]["Error retrieving package list."] = "Błąd podczas pobierania listy pakietów.";
$_t["pl"]["Go back to %hsearch results%h."] = "Powrót do %hwyników wyszukiwania%h.";
$_t["pl"]["Manage"] = "Zarządzaj";
$_t["pl"]["Un-flag Out-of-date"] = "Usuń flagę nieaktualności";
$_t["pl"]["Unflag Out-of-date"] = "Usuń flagę nieaktualności";
$_t["pl"]["Un-Vote"] = "Anuluj głos";
$_t["pl"]["Yes"] = "Tak";
$_t["pl"]["Orphans"] = "Bez opiekuna";
$_t["pl"]["Tarball"] = "Archiwum";
$_t["pl"]["Files"] = "Pliki";
$_t["pl"]["O%hrphan"] = "B%hez opiekuna";
$_t["pl"]["orphan"] = "bez opiekuna";
$_t["pl"]["O%hut-of-Date"] = "N%hieaktualne";
$_t["pl"]["Package Listing"] = "Lista pakietów";

$_t["pl"]["No New Comment Notification"] = "Brak powiadomień o nowych komentarzach.";

$_t["pl"]["UnNotify"] = "Rezygnuj z powiadamiania";

$_t["pl"]["Notify"] = "Włącz powiadamianie";

$_t["pl"]["New Comment Notification"] = "Powiadomienie o nowym komentarzu.";

$_t["pl"]["The above files have been verified (by %s) and are safe to use."] = "Powyższe pliki zostały sprawdzone (przez: %s) i możesz ich bezpiecznie uzywać.";

$_t["pl"]["Be careful! The above files may contain malicious code that can damage your system."] = "Zachowaj ostrożność! Powyższe pliki mogą zawierać szkodliwy kod zagrażający twojemu systemowi.";

$_t["pl"]["Flag Safe"] = "Zaznacz jako bezpieczny";

$_t["pl"]["Flag Package Safe To Use"] = "Zaznacz pakiet jako bezpieczny w użyciu";

$_t["pl"]["Unflag Safe"] = "Usuń flagę bezpieczeństwa";

$_t["pl"]["Unflag Package Safe To Use"] = "Usuń flagę oznaczającą pakiet jako bezpieczny w użyciu";

$_t["pl"]["Safe"] = "Bezpieczny";

$_t["pl"]["First Submitted"] = "Wysłany";

$_t["pl"]["Last Updated"] = "Ostatnia aktualizacja";

$_t["pl"]["Age"] = "Wiek";

$_t["pl"]["Search by"] = "Szukaj według";

$_t["pl"]["Leave the password fields blank to keep your same password."] = "Pozostaw pole z hasłem puste aby nie zmieniać swojego hasła.";

$_t["pl"]["You have been successfully logged out."] = "Zostałeś pomyślnie wylogowany.";

$_t["pl"]["You must log in to view user information."] = "Musisz być zalogowany aby móc oglądać informacje o użytkownikach.";

$_t["pl"]["Could not retrieve information for the specified user."] = "Uzyskanie informacji o podanym użytkowniku nie powiodło się.";

$_t["pl"]["You do not have permission to edit this account."] = "Nie masz uprawnień do edycji tego konta.";

$_t["pl"]["Use this form to search existing accounts."] = "Przy użyciu tego formularza możesz przeszukać istniejące konta.";

$_t["pl"]["Submitter"] = "Nadesłał";

$_t["pl"]["Use this form to create an account."] = "Przy użyciu tego formularza możesz utworzyć konto.";

$_t["pl"]["Use this form to update your account."] = "Przy użyciu tego formularza możesz uaktualnić swoje konto.";

$_t["pl"]["You are not allowed to access this area."] = "Nie masz uprawnień do oglądania tej strony.";

$_t["pl"]["Status"] = "Status";

$_t["pl"]["All"] = "Wszystkie";

$_t["pl"]["Unsafe"] = "Nie bezpieczne";

$_t["pl"]["License"] = "Licencja";

$_t["pl"]["unknown"] = "nieznana";

?>