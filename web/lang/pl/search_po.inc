<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["Under construction..."] = "Prace trwają...";
$_t["pl"]["You must be logged in before you can vote for packages."] = "Musisz być zalogowany aby móc głosować na pakiety.";
$_t["pl"]["You do not have access to disown packages."] = "Nie masz uprawnień do porzucania pakietów.";
$_t["pl"]["You must be logged in before you can flag packages."] = "Musisz być zalogowany aby móc zmieniać flagi pakietów.";
$_t["pl"]["You do not have access to adopt packages."] = "Nie masz uprawnień do przejmowania pakietów.";
$_t["pl"]["You must be logged in before you can disown packages."] = "Musisz być zalogowany aby móc porzucać pakiety.";
$_t["pl"]["Error trying to retrieve package details."] = "Błąd podczas pobierania informacji o pakiecie.";
$_t["pl"]["You must be logged in before you can adopt packages."] = "Musisz być zalogowany aby móc przejmować pakiety.";
$_t["pl"]["The selected packages have been flagged out-of-date."] = "Wybrane pakiety zostały zaznaczone jako nieaktualne.";
$_t["pl"]["You did not select any packages to flag."] = "Nie wybrałeś żadnych pakietów do zaznaczenia.";
$_t["pl"]["The selected packages have been unflagged."] = "Wybrane pakiety zostały odznaczone.";
$_t["pl"]["You must be logged in before you can unflag packages."] = "Musisz być zalogowany aby móc odznaczać pakiety.";
$_t["pl"]["You did not select any packages to unflag."] = "Nie wybrałeś żadnych pakietów do odznaczenia.";
$_t["pl"]["You did not select any packages to adopt."] = "Nie wybrałeś żadnych pakietów do przejęcia.";
$_t["pl"]["You did not select any packages to disowned."] = "Nie wybrałeś żadnych pakietów do porzucenia.";
$_t["pl"]["The selected packages have been adopted."] = "Wybrane pakiety zostały przejęte.";
$_t["pl"]["The selected packages have been disowned."] = "Wybrane pakiety zostały porzucone.";
$_t["pl"]["You must be logged in before you can un-vote for packages."] = "Musisz być zalogowany aby móc anulować głosy na pakiety.";
$_t["pl"]["Your votes have been removed from the selected packages."] = "Twoje głosy zostały odebrane wybranym pakietom.";
$_t["pl"]["You did not select any packages to vote for."] = "Nie wybrałeś żadnych pakietów do oddania głosów.";
$_t["pl"]["You did not select any packages to un-vote for."] = "Nie wybrałeś żadnych pakietów do anulowania głosów.";
$_t["pl"]["Your votes have been cast for the selected packages."] = "Twoje głosy zostały przyznane wybranym pakietom.";

$_t["pl"]["None of the selected packages could be deleted."] = "Żaden z wybranych pakietów nie mógł być usunięty.";

$_t["pl"]["You must be logged in before you can get notifications on comments."] = "Musisz być zalogowany aby móc otrzymywać powiadomienia o komentarzach.";

$_t["pl"]["The selected packages have been deleted."] = "Wybrane pakiety zostały usunięte.";

$_t["pl"]["You have been removed from the comment notification list."] = "Zostałeć usunięty z listy powiadamiania o komentarzach.";

$_t["pl"]["You must be logged in before you can cancel notification on comments."] = "Musisz być zalogowany aby móc zrezygnować z powiadamiania o komentarzach.";

$_t["pl"]["You have been added to the comment notification list."] = "Zostałeś dodany do listy powiadamiania o komentarzach.";

$_t["pl"]["You did not select any packages to disown."] = "Nie wybrałeś żadnych pakietów do porzucenia.";

$_t["pl"]["You did not select any packages to delete."] = "Nie wybrałeś żadnych pakietów do usunięcia.";

$_t["pl"]["Couldn't add to notification list."] = "Dodanie do listy powiadamiania nie powiodło się.";

$_t["pl"]["Couldn't remove from notification list."] = "Usunięcie z listy powiadamiania nie powiodło się.";

$_t["pl"]["The selected packages have been flagged safe."] = "Wybrane pakiety zostały zaznaczone jako bezpieczne.";

$_t["pl"]["Couldn't flag package safe."] = "Zaznaczenie pakietu jako bezpieczny nie powiodło się.";

$_t["pl"]["The selected packages have been unflagged safe."] = "Wybrane pakiety zostały pozbawione flagi bezpieczeństwa.";

$_t["pl"]["Couldn't unflag package safe."] = "Usunięcie flagi bezpieczeństwaego nie powiodło się.";

?>