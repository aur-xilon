<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["Under construction..."] = "Prace trwają...";
$_t["pl"]["Sorry, uploads are not permitted by this server."] = "Przepraszam, ale ładowanie plików nie jest obsługiwane przez ten serwer.";
$_t["pl"]["You must create an account before you can upload packages."] = "Musisz utworzyć konto aby móc dodawać pakiety.";
$_t["pl"]["Upload package"] = "Dodaj pakiet";
$_t["pl"]["Upload"] = "Dodaj";
$_t["pl"]["No"] = "Nie";
$_t["pl"]["Error trying to upload file - please try again."] = "Błąd podczas próby ładowania pliku - proszę spróbować ponownie.";
$_t["pl"]["Yes"] = "Tak";
$_t["pl"]["Overwrite existing package?"] = "Czy nadpisać istniejący pakiet?";
$_t["pl"]["Missing build function in PKGBUILD."] = "W pliku PKGBUILD brakuje funkcji build.";
$_t["pl"]["Could not create incoming directory: %s."] = "Nie udało się utworzyć katalogu dla pakietu: %s.";
$_t["pl"]["Comment"] = "Komentarz";
$_t["pl"]["Missing pkgdesc variable in PKGBUILD."] = "W pliku PKGBUILD brakuje zmiennej pkgdesc.";
$_t["pl"]["Error exec'ing the mv command."] = "Błąd podczas wykonywania polecenia mv.";
$_t["pl"]["You did not specify a package name."] = "Nie podałeś nazwy pakietu.";
$_t["pl"]["Error trying to unpack upload - PKGBUILD does not exist."] = "Błąd podczas próby rozpakowania archiwum - plik PKGBUILD nie istnieje.";
$_t["pl"]["You are not allowed to overwrite the %h%s%h package."] = "Nie masz uprawnień do nadpisania pakietu %h%s%h.";
$_t["pl"]["Upload package file"] = "Plik do załadowania";
$_t["pl"]["Missing url variable in PKGBUILD."] = "W pliku PKGBUILD brakuje zmiennej url.";
$_t["pl"]["Missing pkgver variable in PKGBUILD."] = "W pliku PKGBUILD brakuje zmiennej pkgver.";
$_t["pl"]["Could not change to directory %s."] = "Nie udało się przejść do katalogu %s.";
$_t["pl"]["You did not tag the 'overwrite' checkbox."] = "Nie zaznaczyłeś pola 'nadpisz'.";
$_t["pl"]["Could not change directory to %s."] = "Nie udało się przejść do katalogu %s.";
$_t["pl"]["Invalid name: only lowercase letters are allowed."] = "Nieprawidłowa nazwa: tylko małe litery są dozwolone.";
$_t["pl"]["Package names do not match."] = "Nazwy pakietu nie zgadzają się.";
$_t["pl"]["Package name"] = "Nazwa pakietu";
$_t["pl"]["Missing md5sums variable in PKGBUILD."] = "W pliku PKGBUILD brakuje zmiennej md5sums.";
$_t["pl"]["Missing pkgrel variable in PKGBUILD."] = "W pliku PKGBUILD brakuje zmiennej pkgrel.";
$_t["pl"]["Missing pkgname variable in PKGBUILD."] = "W pliku PKGBUILD brakuje zmiennej pkgname.";
$_t["pl"]["Could not create directory %s."] = "Nie udało się utworzyć katalogu %s.";
$_t["pl"]["Unknown file format for uploaded file."] = "Nieznany format ładowanego pliku.";
$_t["pl"]["Missing source variable in PKGBUILD."] = "W pliku PKGBUILD brakuje zmiennej source.";
$_t["pl"]["Package Location"] = "Lokacja pakietu";
$_t["pl"]["Package Category"] = "Kategoria pakietu";
$_t["pl"]["Select Location"] = "Wybierz lokację";
$_t["pl"]["Select Category"] = "Wybierz kategorię";
$_t["pl"]["You must supply a comment for this upload/change."] = "Musisz napisać komentarz do tego pakietu.";
$_t["pl"]["Package upload successful."] = "Pakiet został dodany pomyślnie.";

$_t["pl"]["Error - No file uploaded"] = "Błąd - plik nie został załadowany.";

$_t["pl"]["Package URL is missing a protocol (ie. http:// ,ftp://)"] = "Adres URL nie zawiera protokołu (np. http// czy ftp://).";

$_t["pl"]["Could not re-tar"] = "Przepakowanie pakietu nie powiodło się.";

$_t["pl"]["Binary packages and filelists are not allowed for upload."] = "Nie można wysyłać pakietów binarnych i plików filelist.";

$_t["pl"]["Missing license variable in PKGBUILD."] = "W pliku PKGBUILD brakuje zmiennej license.";

$_t["pl"]["Missing arch variable in PKGBUILD."] = "W pliku PKGBUILD brakuje zmiennej arch.";

?>