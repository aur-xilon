<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["Select your language here: %h%s%h, %h%s%h, %h%s%h, %h%s%h."] = "Wybierz swój język: %h%s%h, %h%s%h, %h%s%h, %h%s%h.";
$_t["pl"]["Hello, world!"] = "Witaj, świecie!";
$_t["pl"]["Hello, again!"] = "Witaj, ponownie!";
$_t["pl"]["My current language tag is: '%s'."] = "Etykieta mojego obecnego języka to: '%s'.";

?>