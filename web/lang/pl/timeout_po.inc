<?php
# Polish (Polski) translation
# Translator: Jaroslaw Swierczynski <swiergot@gmail.com>

include_once("translator.inc");
global $_t;

$_t["pl"]["Click on the Home link above to log in."] = "Kliknij na linku Start powyżej aby się zalogować.";
$_t["pl"]["Your session has timed out.  You must log in again."] = "Twoja sesja wygasła. Musisz zalogować się ponownie.";

?>