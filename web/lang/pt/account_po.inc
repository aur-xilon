<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["Use this form to update your account."] = "Use este formulário para atualizar sua conta.";

$_t["pt"]["Leave the password fields blank to keep your same password."] = "Deixe o campo de senha em branco para manter sua senha atual.";

$_t["pt"]["You are not allowed to access this area."] = "Você não está autorizado a acessar esta área.";

$_t["pt"]["Could not retrieve information for the specified user."] = "Não foi possível recuperar as informações do usuário especificado.";

$_t["pt"]["Use this form to search existing accounts."] = "Use este formulário para pesquisar as contas existentes.";

$_t["pt"]["You do not have permission to edit this account."] = "Você não tem permissão para editar este conta.";

$_t["pt"]["Use this form to create an account."] = "Use este formulário para criar uma conta.";

$_t["pt"]["You must log in to view user information."] = "Você precisa efetuar o login para visualizar as informações do usuário.";

?>