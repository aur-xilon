<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["Missing a required field."] = "Um campo requerido não foi informado.";

$_t["pt"]["Search'"] = "Pesquisa'";

$_t["pt"]["The account, %h%s%h, has been successfully created."] = "A conta, %h%s%h, foi criada com sucesso.";

$_t["pt"]["Error trying to modify account, %h%s%h: %s."] = "Erro ao tentar modificar a conta, %h%s%h: %s.";

$_t["pt"]["The email address is invalid."] = "O endereço de email é inválido.";

$_t["pt"]["Error trying to create account, %h%s%h: %s."] = "Erro ao tentar criar a conta, %h%s%h: %s.";

$_t["pt"]["The username, %h%s%h, is already in use."] = "O nome de usuário, %h%s%h, já está em uso.";

$_t["pt"]["Account Type"] = "Tipo da Conta";

$_t["pt"]["The account, %h%s%h, has been successfully modified."] = "A contar, %h%s%h, foi modificada com sucesso.";

$_t["pt"]["Account Suspended"] = "Conta Suspensa";

$_t["pt"]["Status"] = "Estado";

$_t["pt"]["New Package Notify"] = "Notificação de novos Pacotes";

$_t["pt"]["IRC Nick"] = "Nick de IRC";

$_t["pt"]["Trusted user"] = "Usuário Confiável";

$_t["pt"]["No results matched your search criteria."] = "Seu critério de pesquisa não retornou nenhum resultado.";

$_t["pt"]["Normal user"] = "Usuário Normal";

$_t["pt"]["Never"] = "Nunca";

$_t["pt"]["User"] = "Usuário";

$_t["pt"]["Active"] = "Ativo";

$_t["pt"]["Last Voted"] = "Último Votado";

$_t["pt"]["Real Name"] = "Nome Real";

$_t["pt"]["Edit Account"] = "Editar Conta";

$_t["pt"]["Password fields do not match."] = "As senhas informadas não conferem.";

$_t["pt"]["Language"] = "Idioma";

$_t["pt"]["A Trusted User cannot assign Developer status."] = "Um Usuário Confiável não pode atribuir o estado de Desenvolvedor.";

$_t["pt"]["The address, %h%s%h, is already in use."] = "O endereço, %h%s%h, já está em uso.";

$_t["pt"]["No more results to display."] = "Sem mais resultados para exibir.";

$_t["pt"]["Type"] = "Tipo";

$_t["pt"]["Click on the Home link above to login."] = "Clique no link Inicial acima para efetuar o login.";

$_t["pt"]["Sort by"] = "Ordenar por";

$_t["pt"]["Re-type password"] = "Confirme a senha";

$_t["pt"]["Language is not currently supported."] = "Idioma atualmente não suportado.";

$_t["pt"]["Any type"] = "Qualquer tipo";

$_t["pt"]["Last vote"] = "Último voto";

$_t["pt"]["Suspended"] = "Suspenso";

$_t["pt"]["Trusted User"] = "Usuário Confiável";

$_t["pt"]["Missing User ID"] = "ID de usuário não encontrada";

$_t["pt"]["Developer"] = "Desenvolvedor";

$_t["pt"]["View this user's packages"] = "Visualizar os pacotes deste usuário";

?>