<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["Home"] = "Inicial";

$_t["pt"]["%s: An ArchLinux project"] = "%s: Um projeto ArchLinux";

$_t["pt"]["Packages"] = "Pacotes";

$_t["pt"]["Accounts"] = "Contas";

$_t["pt"]["Logout"] = "Sair";

$_t["pt"]["%s: %sAn ArchLinux project%s"] = "%s: %sUm projeto ArchLinux%s";

$_t["pt"]["Discussion"] = "Discussão";

$_t["pt"]["Bugs"] = "Falhas";

?>
