<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["Reset"] = "Resetar";

$_t["pt"]["Username"] = "Nome de Usuário";

$_t["pt"]["Email Address"] = "Endereço de Email";

$_t["pt"]["Less"] = "Menor";

$_t["pt"]["Clear"] = "Limpar";

$_t["pt"]["required"] = "requerido";

$_t["pt"]["Update"] = "Atualizar";

$_t["pt"]["Submit"] = "Enviar";

$_t["pt"]["Password"] = "Senha";

$_t["pt"]["Create"] = "Criar";

$_t["pt"]["More"] = "Mais";

?>