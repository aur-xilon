<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["Your session id is invalid."] = "Seu id de sessão é inválido.";

$_t["pt"]["If this problem persists, please contact the site administrator."] = "Se o problema persistir, por favor, contacte o administrador do site.";

?>