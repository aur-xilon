<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["You must supply a password."] = "Você deve informar uma senha.";

$_t["pt"]["You must supply a username."] = "Você deve informar um usuário.";

$_t["pt"]["After that, this can be filled in with more meaningful text."] = "Depois, isso pode ter um texto mais significativo.";

$_t["pt"]["Logged-in as: %h%s%h"] = "Logado como: %h%s%h";

$_t["pt"]["Your account has been suspended."] = "Sua conta foi suspensa.";

$_t["pt"]["Password:"] = "Senha:";

$_t["pt"]["Username:"] = "Usuário:";

$_t["pt"]["Welcome to the AUR! If you're a newcomer, you may want to read the %hGuidelines%h."] = "Bem-vindo ao AUR! Se você é um(a) novato(a), você deveria ler as %hRegras%h.";

$_t["pt"]["This is where the intro text will go."] = "É aqui que o texto de introdução deverá estar.";

$_t["pt"]["Error trying to generate session id."] = "Erro ao tentar gerar um id de sessão.";

$_t["pt"]["For now, it's just a place holder."] = "Por agora, é apenas um place holder.";

$_t["pt"]["It's more important to get the login functionality finished."] = "É mais importante terminar a funcionalidade de login.";

$_t["pt"]["Error looking up username, %s."] = "Erro ao pesquisar o usuário, %s.";

$_t["pt"]["Login"] = "Login";

$_t["pt"]["Though we can't vouch for their contents, we provide a %hlist of user repositories%h for your convenience."] = "Embora nós não temos controle sobre seus conteúdos, nós provemos uma %hlista de repositórios de usuários%h para sua conveniência.";

$_t["pt"]["If you have feedback about the AUR, please leave it in %hFlyspray%h."] = "Se você tiver algum comentário sobre o AUR, por favor, deixe-o no %hFlyspray%h.";

$_t["pt"]["Incorrect password for username, %s."] = "Senha incorreta para o usuário, %s.";

$_t["pt"]["Latest Packages:"] = "Últimos Pacotes:";

$_t["pt"]["Discussion about the AUR takes place on the %sTUR Users List%s."] = "Discurssões sobre o AUR são feitas através da %slista de usuários TUR%s.";

$_t["pt"]["Email discussion about the AUR takes place on the %sTUR Users List%s."] = "Discurssões por Email sobre o AUR são feitas através da %slista de usuários TUR%s.";

$_t["pt"]["Recent Updates"] = "Atualizações Recentes";

$_t["pt"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information. Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "Bem-vindo ao AUR! Por favor leia as %hregras de usuário do AUR%h e as %hregras de TU do AUR%h para maiores informações.PKGBUILDs contribuídos <b>devem</b> seguir os %hpadrões Arch de Empacotamento%h caso contrário, eles serão excluídos!";

$_t["pt"]["Community"] = "Comunidade";

$_t["pt"]["Package Counts"] = "Contagem de Pacotes";

$_t["pt"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information."] = "Bem-vindo ao AUR! Por favor leia as %hregras de usuário do AUR%h e as %hregras de TU do AUR%h para maiores informações.";

$_t["pt"]["Unsupported"] = "Sem Suporte";

$_t["pt"]["Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "PKGBUILDs contribuídos <b>devem</b> seguir os %hpadrões Arch de Empacotamento%h caso contrário, eles serão excluídos!";

$_t["pt"]["Statistics"] = "Estatísticas";

$_t["pt"]["User Statistics"] = "Estatísticas do Usuário";

$_t["pt"]["Registered Users"] = "Usuários Registrados";

$_t["pt"]["Trusted Users"] = "Usuários Confiáveis";

$_t["pt"]["Packages in unsupported"] = "Pacotes no repositório unsupported";

$_t["pt"]["Packages in unsupported and flagged as safe"] = "Pacotes no repositório unsupported e marcados como seguro";

$_t["pt"]["Packages in [community]"] = "Pacotes no repositório [community]";

$_t["pt"]["Remember to vote for your favourite packages! The most popular packages are provided as binary packages in [community]."] = "Lembre-se de votar nos seus pacotes favoritos! Os pacotes mais populares serão disponibilizados como pacotes binários no repositório [community].";

$_t["pt"]["Remember to vote for your favourite packages!"] = "Lembre-se de votar nos seus pacotes favoritos!";

$_t["pt"]["The most popular packages will be provided as binary packages in [community]."] = "Os pacotes mais populares serão disponibilizados como pacotes binários no repositório [community].";

$_t["pt"]["Packages added or updated in the past 7 days"] = "Pacotes adicionados ou atualizados nos últimos 7 dias";

$_t["pt"]["Out-of-date"] = "Desatualizados";

$_t["pt"]["My Statistics"] = "Minhas Estatísticas";

$_t["pt"]["Flagged as safe by me"] = "Pacotes que marquei como seguros";

?>