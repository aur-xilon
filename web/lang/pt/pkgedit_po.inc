<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["Missing package ID."] = "ID do pacote não encontrada.";

$_t["pt"]["Invalid category ID."] = "ID inválido de categoria .";

$_t["pt"]["Enter your comment below."] = "Deixe seu comentário abaixo.";

$_t["pt"]["You are not allowed to delete this comment."] = "Você não está autorizado a apagar este comentário.";

$_t["pt"]["Missing comment ID."] = "ID de comentário não encontrada.";

$_t["pt"]["Package category updated."] = "Categoria de pacotes atualizada.";

$_t["pt"]["You must be logged in before you can edit package information."] = "Você deve ter efetuado o login antes de poder editar as informações do pacote.";

$_t["pt"]["Comment has been deleted."] = "Comentário foi excluído.";

$_t["pt"]["You've found a bug if you see this...."] = "Você encontrou um problema (bug) se você ver isso...";

$_t["pt"]["Comment has been added."] = "Comentário foi adicionado.";

$_t["pt"]["Select new category"] = "Selecione a nova categoria";

?>