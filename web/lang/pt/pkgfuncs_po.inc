<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["Category"] = "Categoria";

$_t["pt"]["Votes"] = "Votos";

$_t["pt"]["Comment by: %h%s%h on %h%s%h"] = "Comentário por: %h%s%h on %h%s%h";

$_t["pt"]["Location"] = "Localização";

$_t["pt"]["Delete comment"] = "Excluir Comentário";

$_t["pt"]["Go"] = "Ir";

$_t["pt"]["Unflag Out-of-date"] = "Retirar marcador de Desatualizado";

$_t["pt"]["Go back to %hpackage details view%h."] = "Retornar para a %hvisualização dos detalhes do pacote%h.";

$_t["pt"]["Error retrieving package details."] = "Erro ao retornar os detalhes do pacote.";

$_t["pt"]["Description"] = "Descrição";

$_t["pt"]["My Packages"] = "Meus Pacotes";

$_t["pt"]["Keywords"] = "Palavras Chave";

$_t["pt"]["Dependencies"] = "Dependências";

$_t["pt"]["Disown Packages"] = "Abandonar Pacotes";

$_t["pt"]["Package details could not be found."] = "Detalhes do Pacote não foram encontrados.";

$_t["pt"]["Package Details"] = "Detalhes do Pacote";

$_t["pt"]["Error retrieving package list."] = "Erro ao retornar a lista de pacotes.";

$_t["pt"]["Files"] = "Arquivos";

$_t["pt"]["None"] = "Nenhum";

$_t["pt"]["Name"] = "Nome";

$_t["pt"]["Per page"] = "Por página";

$_t["pt"]["Go back to %hsearch results%h."] = "Retorne para os %hresultados da pesquisa%h.";

$_t["pt"]["No packages matched your search criteria."] = "Seu critério de pesquisa não retornou nenhum pacote.";

$_t["pt"]["O%hrphan"] = "O%hrfão";

$_t["pt"]["orphan"] = "orfão";

$_t["pt"]["Un-Vote"] = "Retirar Voto";

$_t["pt"]["change category"] = "mudar categoria";

$_t["pt"]["UnNotify"] = "Retirar Notificação";

$_t["pt"]["Delete Packages"] = "Excluir Pacote";

$_t["pt"]["Maintainer"] = "Mantenedor";

$_t["pt"]["Add Comment"] = "Adicionar Comentário";

$_t["pt"]["Tarball"] = "Tarball";

$_t["pt"]["Flag Out-of-date"] = "Marcar como Desatualizado";

$_t["pt"]["Manage"] = "Gerenciar";

$_t["pt"]["Sort by"] = "Ordenar por";

$_t["pt"]["Sort order"] = "Ordem";

$_t["pt"]["Ascending"] = "Ascendente";

$_t["pt"]["Descending"] = "Decrescente";

$_t["pt"]["Actions"] = "Ações";

$_t["pt"]["Sources"] = "Fontes";

$_t["pt"]["Search Criteria"] = "Critério de Pesquisa";

$_t["pt"]["Notify"] = "Notificar";

$_t["pt"]["O%hut-of-Date"] = "Desat%hualizado";

$_t["pt"]["Vote"] = "Votar";

$_t["pt"]["Adopt Packages"] = "Adotar Pacotes";

$_t["pt"]["Yes"] = "Sim";

$_t["pt"]["Package Listing"] = "Listagem de Pacotes";

$_t["pt"]["Orphans"] = "Orfãos";

$_t["pt"]["Any"] = "Qualquer";

$_t["pt"]["Voted"] = "Votado";

$_t["pt"]["No New Comment Notification"] = "Sem Notificações de Comentário";

$_t["pt"]["New Comment Notification"] = "Nova Notificação de Comentário";

$_t["pt"]["Comments"] = "Comentários";

$_t["pt"]["The above files have been verified (by %s) and are safe to use."] = "Os arquivos acima foram verificados (por %s) e são seguros para uso.";

$_t["pt"]["Be careful! The above files may contain malicious code that can damage your system."] = "Tome cuidado! Os arquivos acima podem ter código malicioso que pode danificar seu sistema.";

$_t["pt"]["Flag Safe"] = "Marcar como Seguro";

$_t["pt"]["Flag Package Safe To Use"] = "Marcar Pacote como Seguro para Uso";

$_t["pt"]["Unflag Safe"] = "Retirar marcador de Seguro";

$_t["pt"]["Unflag Package Safe To Use"] = "Retirar marcador de Pacote Seguro para Uso";

$_t["pt"]["Safe"] = "Seguro";

$_t["pt"]["Age"] = "Idade";

$_t["pt"]["First Submitted"] = "Submetido pela primeira vez";

$_t["pt"]["Last Updated"] = "Últimos Pacotes Atualizados";

$_t["pt"]["Search by"] = "Pesquisar por";

$_t["pt"]["Submitter"] = "Colaborador";

$_t["pt"]["All"] = "Todos";

$_t["pt"]["Unsafe"] = "Não Seguro";

$_t["pt"]["Status"] = "Status";

$_t["pt"]["License"] = "Licença";

$_t["pt"]["unknown"] = "Desconhecida";

?>