<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["None of the selected packages could be deleted."] = "Nenhum dos pacotes selecionados pode ser excluído.";

$_t["pt"]["Your votes have been removed from the selected packages."] = "Seus votos foram removidos dos pacotes selecionados.";

$_t["pt"]["You did not select any packages to un-vote for."] = "Você não selecionou nenhum pacote para retirar os votos.";

$_t["pt"]["The selected packages have been unflagged."] = "Os pacotes selecionados tiveram seus marcadores retirados.";

$_t["pt"]["You did not select any packages to adopt."] = "Você não selecionou nenhum pacote para adotar.";

$_t["pt"]["You must be logged in before you can flag packages."] = "Você deve efetuar o login antes de poder marcar pacotes.";

$_t["pt"]["You must be logged in before you can get notifications on comments."] = "Você deve efetuar o login antes de receber notificações dos comentários.";

$_t["pt"]["You must be logged in before you can vote for packages."] = "Você deve efetuar o login antes de poder votar nos pacotes.";

$_t["pt"]["The selected packages have been flagged out-of-date."] = "Os pacotes selecionados foram marcados como desatualizados.";

$_t["pt"]["The selected packages have been deleted."] = "Os pacotes selecionados foram excluídos.";

$_t["pt"]["You did not select any packages to vote for."] = "Você não selecionou nenhum pacote para votar.";

$_t["pt"]["You must be logged in before you can disown packages."] = "Você deve efetuar o login antes de poder abandonar pacotes";

$_t["pt"]["Error trying to retrieve package details."] = "Erro ao tentar retornar os detalhes do pacote.";

$_t["pt"]["The selected packages have been adopted."] = "Os pacotes selecionados foram adotados.";

$_t["pt"]["You have been removed from the comment notification list."] = "Você foi removido da lista de notificação de comentários.";

$_t["pt"]["Your votes have been cast for the selected packages."] = "Os pacotes selecionados foram votados.";

$_t["pt"]["You must be logged in before you can cancel notification on comments."] = "Você deve efetuar o login antes de poder cancelar notificações de comentários";

$_t["pt"]["You must be logged in before you can adopt packages."] = "Você deve efetuar o login antes de poder adotar pacotes.";

$_t["pt"]["You have been added to the comment notification list."] = "Você foi adicionado para a lista de notificação.";

$_t["pt"]["You did not select any packages to disown."] = "Você não selecionou nenhum pacote para abandonar.";

$_t["pt"]["You must be logged in before you can un-vote for packages."] = "Você deve efetuar o login antes de poder retirar seu voto de um pacote.";

$_t["pt"]["You must be logged in before you can unflag packages."] = "Você deve efetuar o login antes de poder desmarcar pacotes.";

$_t["pt"]["You did not select any packages to unflag."] = "Você não selecionou nenhum pacote para desmarcar.";

$_t["pt"]["You did not select any packages to delete."] = "Você não selecionou nenhum pacote para excluir.";

$_t["pt"]["Couldn't add to notification list."] = "Não foi possível adicionar para a lista de notificação.";

$_t["pt"]["You did not select any packages to flag."] = "Você não selecionou nenhum pacote para marcar.";

$_t["pt"]["The selected packages have been disowned."] = "Os pacotes selecionados foram abandonados.";

$_t["pt"]["Couldn't remove from notification list."] = "Não foi possível remover da lista de notificação.";

$_t["pt"]["The selected packages have been flagged safe."] = "Os pacotes selecionados foram marcados como seguros.";

$_t["pt"]["Couldn't flag package safe."] = "Não foi possível marcar o pacote como pacote seguro.";

$_t["pt"]["The selected packages have been unflagged safe."] = "Foi retirado o marcador de pacote seguro dos pacotes selecionados.";

$_t["pt"]["Couldn't unflag package safe."] = "Não foi possível retirar marcador de pacote seguro.";

?>