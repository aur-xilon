<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["Missing build function in PKGBUILD."] = "Função build não encontrada no arquivo PKGBUILD.";

$_t["pt"]["Could not change directory to %s."] = "Não foi possível mudar o diretório para %s.";

$_t["pt"]["No"] = "Não";

$_t["pt"]["Missing pkgdesc variable in PKGBUILD."] = "Variável pkgdesc não encontrada no arquivo PKGBUILD.";

$_t["pt"]["Error trying to upload file - please try again."] = "Erro ao tentar enviar o arquivo - por favor, tente novamente.";

$_t["pt"]["Error exec'ing the mv command."] = "Erro ao executar o comando mv.";

$_t["pt"]["You must create an account before you can upload packages."] = "Você deve criar uma conta antes para poder enviar Pacotes.";

$_t["pt"]["Package upload successful."] = "Sucesso ao enviar Pacote.";

$_t["pt"]["Overwrite existing package?"] = "Sobreescrever Pacote existente ?";

$_t["pt"]["You did not specify a package name."] = "Você não especificou um nome para o Pacote.";

$_t["pt"]["Error trying to unpack upload - PKGBUILD does not exist."] = "Erro ao tentar descompactar Pacote enviado - o arquivo PKGBUILD não existe.";

$_t["pt"]["Could not create incoming directory: %s."] = "Não foi possível criar diretório: %s.";

$_t["pt"]["Upload package file"] = "Enviar arquivo de Pacote";

$_t["pt"]["Package Location"] = "Local onde está o Pacote";

$_t["pt"]["Missing url variable in PKGBUILD."] = "Variável url não encontrada no arquivo PKGBUILD.";

$_t["pt"]["Package names do not match."] = "Nomes do Pacote não conferem.";

$_t["pt"]["Package Category"] = "Categoria do Pacote";

$_t["pt"]["Could not change to directory %s."] = "Não foi possível mudar o diretório para %s.";

$_t["pt"]["You did not tag the 'overwrite' checkbox."] = "Você não marcou a tag 'overwrite' no checkbox.";

$_t["pt"]["Invalid name: only lowercase letters are allowed."] = "Nome inválido: somente letras minúsculas são permitidas.";

$_t["pt"]["Missing pkgver variable in PKGBUILD."] = "Variável pkgver não encontrada no arquivo PKGBUILD.";

$_t["pt"]["Package name"] = "Nome do Pacote";

$_t["pt"]["Upload"] = "Enviar";

$_t["pt"]["Missing md5sums variable in PKGBUILD."] = "Variável md5sums não encontrada no arquivo PKGBUILD.";

$_t["pt"]["Missing pkgrel variable in PKGBUILD."] = "Variável pkgrel não encontrada no arquivo PKGBUILD.";

$_t["pt"]["Missing pkgname variable in PKGBUILD."] = "Variável pkgname não encontrada no arquivo PKGBUILD.";

$_t["pt"]["Error - No file uploaded"] = "Erro - Nenhum arquivo enviado";

$_t["pt"]["You are not allowed to overwrite the %h%s%h package."] = "Você não esta autorizado a sobreescrever o pacote %h%s%h.";

$_t["pt"]["Select Location"] = "Selecionar local do arquivo";

$_t["pt"]["Select Category"] = "Selecionar Categoria";

$_t["pt"]["Comment"] = "Comentário";

$_t["pt"]["Could not create directory %s."] = "Não foi possível criar o diretório %s.";

$_t["pt"]["Unknown file format for uploaded file."] = "Formato de arquivo desconhecido para o arquivo enviado.";

$_t["pt"]["Missing source variable in PKGBUILD."] = "Variável source não encontrada no arquivo PKGBUILD.";

$_t["pt"]["Sorry, uploads are not permitted by this server."] = "Desculpe, envio de arquivos não é permitido nesse servidor.";

$_t["pt"]["You must supply a comment for this upload/change."] = "Você precisa informar um comentário para este envio/mudança.";

$_t["pt"]["Yes"] = "Sim";

$_t["pt"]["Package URL is missing a protocol (ie. http:// ,ftp://)"] = "A URL do Pacote não contém o protocolo (ex. http:// ,ftp://)";

$_t["pt"]["Could not re-tar"] = "Não foi possível recompactar";

$_t["pt"]["Binary packages and filelists are not allowed for upload."] = "Pacotes binários e filelists não são permitidos para envio.";

$_t["pt"]["Missing arch variable in PKGBUILD."] = "Variável arch não encontrada no arquivo PKGBUILD.";

$_t["pt"]["Missing license variable in PKGBUILD."] = "Variável license não encontrada no arquivo PKGBUILD.";


?>