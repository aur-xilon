<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["Select your language here: %h%s%h, %h%s%h, %h%s%h, %h%s%h."] = "Selecione seu idioma aqui: %h%s%h, %h%s%h, %h%s%h, %h%s%h.";

$_t["pt"]["Hello, world!"] = "Olá, mundo!";

$_t["pt"]["Hello, again!"] = "Olá, novamente!";

$_t["pt"]["My current language tag is: '%s'."] = "Meu identificador atual de idioma é: '%s'.";

?>