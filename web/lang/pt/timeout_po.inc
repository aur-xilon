<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

$_t["pt"]["Click on the Home link above to log in."] = "Clique no link Inicial acima para efetuar o login.";

$_t["pt"]["Your session has timed out.  You must log in again."] = "Sua sessão expirou. Você deve efetuar o login novamente.";

?>