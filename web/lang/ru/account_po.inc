<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["Use this form to update your account."] = "Используйте эту форму для изменения вашей учетной записи.";

$_t["ru"]["Leave the password fields blank to keep your same password."] = "Оставьте поля для ввода пароля пустыми если не хотите его изменять.";

$_t["ru"]["You are not allowed to access this area."] = "У вас нет доступа к этой части.";

$_t["ru"]["Could not retrieve information for the specified user."] = "Невозможно получить информацию об указанном пользователе.";

$_t["ru"]["Use this form to search existing accounts."] = "Используйте эту форму для поиска существующей учетной записи.";

$_t["ru"]["You do not have permission to edit this account."] = "У вас нет прав для изменения этой учетной записи.";

$_t["ru"]["Use this form to create an account."] = "Используйте эту форму для создания новой учетной записи.";

$_t["ru"]["You must log in to view user information."] = "Вы должны представиться для просмотра информации о пользователе.";

?>