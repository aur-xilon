<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["Missing a required field."] = "Отсутствует обязательное значение.";

$_t["ru"]["Search'"] = "Поиск'";

$_t["ru"]["The account, %h%s%h, has been successfully created."] = "Учетная запись %h%s%h успешно создана.";

$_t["ru"]["Error trying to modify account, %h%s%h: %s."] = "Ошибка изменения учетной записи, %h%s%h: %s.";

$_t["ru"]["The email address is invalid."] = "Неправильный адрес электронной почты.";

$_t["ru"]["Error trying to create account, %h%s%h: %s."] = "Ошибка создания учетной записи, %h%s%h: %s.";

$_t["ru"]["The username, %h%s%h, is already in use."] = "Имя %h%s%h уже используется.";

$_t["ru"]["Account Type"] = "Тип учетной записи.";

$_t["ru"]["The account, %h%s%h, has been successfully modified."] = "Учетная запись %h%s%h успешно изменена.";

$_t["ru"]["Account Suspended"] = "Действие учетной записи приостановлено.";

$_t["ru"]["Status"] = "Статус";

$_t["ru"]["New Package Notify"] = "Извещать о новых пакетах";

$_t["ru"]["IRC Nick"] = "IRC Ник";

$_t["ru"]["Trusted user"] = "Доверенный пользователь";

$_t["ru"]["No results matched your search criteria."] = "По вашему запросу ничего не найдено.";

$_t["ru"]["Normal user"] = "Обычный пользователь";

$_t["ru"]["Never"] = "Никогда";

$_t["ru"]["User"] = "Пользователь";

$_t["ru"]["Active"] = "Активный";

$_t["ru"]["Last Voted"] = "Последний проголосовавший";

$_t["ru"]["Real Name"] = "Настоящее имя";

$_t["ru"]["Edit Account"] = "Изменить учетную запись";

$_t["ru"]["Password fields do not match."] = "Пароли не совпадают.";

$_t["ru"]["View this user's packages"] = "Посмотреть пакеты этого пользователя";

$_t["ru"]["Language"] = "Язык";

$_t["ru"]["A Trusted User cannot assign Developer status."] = "Доверенный пользователь не может устанавливать статус Разработчик.";

$_t["ru"]["The address, %h%s%h, is already in use."] = "Адрес %h%s%h уже используется.";

$_t["ru"]["No more results to display."] = "Больше нет результатов.";

$_t["ru"]["Type"] = "Тип";

$_t["ru"]["Click on the Home link above to login."] = "Щелкните ссылку Home чтобы представиться.";

$_t["ru"]["Sort by"] = "Сортировать по";

$_t["ru"]["Re-type password"] = "Введите пароль еще раз";

$_t["ru"]["Language is not currently supported."] = "Язык пока не поддерживается.";

$_t["ru"]["Any type"] = "Любой тип";

$_t["ru"]["Last vote"] = "Последнее голосование";

$_t["ru"]["Suspended"] = "Приостановлена";

$_t["ru"]["Trusted User"] = "Доверенный пользователь";

$_t["ru"]["Missing User ID"] = "Отсутствует идентификатор пользователя";

$_t["ru"]["Developer"] = "Разработчик";

?>