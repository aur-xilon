<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["%s: %sAn ArchLinux project%s"] = "%s: %sПроект ArchLinux%s";

$_t["ru"]["Logout"] = "Выход";

$_t["ru"]["Discussion"] = "Обсуждение";

$_t["ru"]["Bugs"] = "Ошибки";

$_t["ru"]["Accounts"] = "Учетная запись";

$_t["ru"]["Home"] = "Домой";

$_t["ru"]["Packages"] = "Пакеты";

$_t["ru"]["My Packages"] = "Мои пакеты";

?>