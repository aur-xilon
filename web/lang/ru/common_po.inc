<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["Reset"] = "Очистить";

$_t["ru"]["Username"] = "Имя пользователя";

$_t["ru"]["Email Address"] = "Адрес электронной почты";

$_t["ru"]["Less"] = "Назад";

$_t["ru"]["Clear"] = "Очистить";

$_t["ru"]["required"] = "необходимо";

$_t["ru"]["Update"] = "Обновить";

$_t["ru"]["Submit"] = "Прислать";

$_t["ru"]["Password"] = "Пароль";

$_t["ru"]["Create"] = "Создать";

$_t["ru"]["More"] = "Далее";

?>