<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["Your session id is invalid."] = "Неверный идентификатор сессии.";

$_t["ru"]["If this problem persists, please contact the site administrator."] = "Если проблема не решена, обратитесь к администратору сайта.";

?>