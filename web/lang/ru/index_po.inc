<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["Statistics"] = "Статистика";

$_t["ru"]["Remember to vote for your favourite packages!"] = "Не забывайте голосовать за полюбившиеся вам пакеты!";

$_t["ru"]["Error looking up username, %s."] = "Ошибка поиска имени пользователя, %s.";

$_t["ru"]["Packages in unsupported"] = "Пакетов в [unsupported]";

$_t["ru"]["The most popular packages will be provided as binary packages in [community]."] = "Наиболее популярные пакеты будут добавлены в [community] в собраном виде.";

$_t["ru"]["Trusted Users"] = "Доверенных пользователей";

$_t["ru"]["You must supply a username."] = "Вы должны задать имя пользователя.";

$_t["ru"]["Packages added or updated in the past 7 days"] = "Пакетов добавлено/обновлено за последние 7 дней";

$_t["ru"]["Email discussion about the AUR takes place on the %sTUR Users List%s."] = "Используйте %sTUR Users List%s для обсуждения AUR.";

$_t["ru"]["Packages in unsupported and flagged as safe"] = "Безопасных пакетов в [unsupported]";

$_t["ru"]["Though we can't vouch for their contents, we provide a %hlist of user repositories%h for your convenience."] = "Хотя мы не можем  поручиться за содержимое, мы предоставляем %hсписок пользовательских репозитариев%h для удобства.";

$_t["ru"]["Packages in [community]"] = "Пакетов в [community]";

$_t["ru"]["Recent Updates"] = "Последние обновления";

$_t["ru"]["Your account has been suspended."] = "Действие вашей учетной записи приостановлено.";

$_t["ru"]["Username:"] = "Имя пользователя:";

$_t["ru"]["Error trying to generate session id."] = "Ошибка генерации идентификатора сессии.";

$_t["ru"]["Welcome to the AUR! Please read the %hAUR User Guidelines%h and %hAUR TU Guidelines%h for more information."] = "Добро пожаловать в AUR! Пожалуйста прочитайте %hAUR User Guidelines%h и %hAUR TU Guidelines%h, чтобы получить больше информации.";

$_t["ru"]["Contributed PKGBUILDs <b>must</b> conform to the %hArch Packaging Standards%h otherwise they will be deleted!"] = "Присланые PKGBUILD <b>должны</b> соответствовать %hArch Packaging Standards%h или будут удалены!";

$_t["ru"]["Login"] = "Войти";

$_t["ru"]["If you have feedback about the AUR, please leave it in %hFlyspray%h."] = "Замечания и пожелания о AUR оставляйте в %hFlyspray%h.";

$_t["ru"]["You must supply a password."] = "Вы должны ввести пароль.";

$_t["ru"]["Password:"] = "Пароль:";

$_t["ru"]["Registered Users"] = "Зарегистрированных пользователей";

$_t["ru"]["Logged-in as: %h%s%h"] = "Вошли как: %h%s%h";

$_t["ru"]["Incorrect password for username, %s."] = "Неверный пароль для пользователя %s.";

$_t["ru"]["Safe"] = "Безопасный";

$_t["ru"]["Out-of-date"] = "Устарел";

$_t["ru"]["User Statistics"] = "Статистика пользователя";

$_t["ru"]["Flagged as safe by me"] = "Отмечено мной как безопасный";

$_t["ru"]["Flagged as safe"] = "Отмечено как безопасный";

$_t["ru"]["My Statistics"] = "Моя статистика";

?>