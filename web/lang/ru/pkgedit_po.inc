<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["Missing package ID."] = "Идентификатор пакета отсутствует.";

$_t["ru"]["Invalid category ID."] = "Неверный идентификатор категории.";

$_t["ru"]["Enter your comment below."] = "Введите ваш коментарий ниже.";

$_t["ru"]["You are not allowed to delete this comment."] = "У вас нет прав для удаления этого коментария.";

$_t["ru"]["Missing comment ID."] = "Идентификатор коментария отсутствует.";

$_t["ru"]["Package category updated."] = "Категория пакета обновлена.";

$_t["ru"]["You must be logged in before you can edit package information."] = "Вы должны представиться прежде чем редактировать информацию о пакете.";

$_t["ru"]["Comment has been deleted."] = "Коментарий удален.";

$_t["ru"]["You've found a bug if you see this...."] = "Вы нашли ошибку, если видите это....";

$_t["ru"]["Comment has been added."] = "Коментарий добавлен";

$_t["ru"]["Select new category"] = "Выберите новую категорию.";

?>