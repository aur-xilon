<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["Category"] = "Категория";

$_t["ru"]["Search by"] = "Искать по";

$_t["ru"]["Delete comment"] = "Удалить коментарий";

$_t["ru"]["orphan"] = "сирота";

$_t["ru"]["Votes"] = "Голосов";

$_t["ru"]["First Submitted"] = "Впервые послан";

$_t["ru"]["Tarball"] = "Архив";

$_t["ru"]["Be careful! The above files may contain malicious code that can damage your system."] = "Будьте осторожны! Данные файлы могут содержать код, который способен повредить вашу систему.";

$_t["ru"]["Voted"] = "Мой голос";

$_t["ru"]["Location"] = "Местонахождение";

$_t["ru"]["Flag Safe"] = "Флаг Безопасно";

$_t["ru"]["Go"] = "Поехали";

$_t["ru"]["Unflag Out-of-date"] = "Убрать флаг Устаревший";

$_t["ru"]["Go back to %hpackage details view%h."] = "Вернуться к %hинформации о пакете%h.";

$_t["ru"]["Error retrieving package details."] = "Ошибка получения информации о пакете.";

$_t["ru"]["Description"] = "Описание";

$_t["ru"]["My Packages"] = "Мои пакеты";

$_t["ru"]["Safe"] = "Безопасно";

$_t["ru"]["Sort order"] = "Порядок сортировки";

$_t["ru"]["Ascending"] = "По возрастанию";

$_t["ru"]["Keywords"] = "Ключевые слова";

$_t["ru"]["No New Comment Notification"] = "Нет новых извещений о коментариях";

$_t["ru"]["Dependencies"] = "Зависимости";

$_t["ru"]["Descending"] = "По убыванию";

$_t["ru"]["Per page"] = "Постранично";

$_t["ru"]["Package Listing"] = "Список пакетов";

$_t["ru"]["Package details could not be found."] = "Не найдена информация о пакете.";

$_t["ru"]["Package Details"] = "Информация о пакете";

$_t["ru"]["Error retrieving package list."] = "Ошибка получения списка пакетов.";

$_t["ru"]["Files"] = "Файлы";

$_t["ru"]["Name"] = "Имя";

$_t["ru"]["Last Updated"] = "Последнее обновление";

$_t["ru"]["The above files have been verified (by %s) and are safe to use."] = "Данные файлы проверены (%s) и могут быть безопасно использованы.";

$_t["ru"]["Unflag Package Safe To Use"] = "Снять флаг Безопасно";

$_t["ru"]["Go back to %hsearch results%h."] = "Вернуться к %hрезультатам поиска%h.";

$_t["ru"]["Age"] = "Возраст";

$_t["ru"]["Comments"] = "Коментарии";

$_t["ru"]["Submitter"] = "Автор";

$_t["ru"]["Un-Vote"] = "Убрать мой голос";

$_t["ru"]["change category"] = "изменить категорию";

$_t["ru"]["UnNotify"] = "Не извещать";

$_t["ru"]["Delete Packages"] = "Удалить пакеты";

$_t["ru"]["Maintainer"] = "Ответственный";

$_t["ru"]["Add Comment"] = "Добавить коментарий";

$_t["ru"]["Comment by: %h%s%h on %h%s%h"] = "Коментарии: %h%s%h к %h%s%h";

$_t["ru"]["Flag Out-of-date"] = "Пометить как Устаревший";

$_t["ru"]["Manage"] = "Управлять";

$_t["ru"]["Sort by"] = "Сортировать по";

$_t["ru"]["Flag Package Safe To Use"] = "Пометить флагом Безопасно";

$_t["ru"]["Actions"] = "Действия";

$_t["ru"]["Unflag Safe"] = "Снять флаг Безопасно";

$_t["ru"]["Sources"] = "Исходники";

$_t["ru"]["Yes"] = "Да";

$_t["ru"]["Search Criteria"] = "Критерий поиска";

$_t["ru"]["Notify"] = "Извещать";

$_t["ru"]["O%hut-of-Date"] = "У%hстарел";

$_t["ru"]["Vote"] = "Голосовать";

$_t["ru"]["Adopt Packages"] = "Усыновить пакеты";

$_t["ru"]["New Comment Notification"] = "Извещение о новом коментарии";

$_t["ru"]["Disown Packages"] = "Бросить пакеты";

$_t["ru"]["Orphans"] = "Сироты";

$_t["ru"]["Any"] = "Любой";

$_t["ru"]["No packages matched your search criteria."] = "Нет пакетов по выбранному критерию поиска.";

$_t["ru"]["Status"] = "Статус";

$_t["ru"]["Leave the password fields blank to keep your same password."] = "Оставьте по�арол�и пустыми, чтобы сохранить ваш старый пароль.";

$_t["ru"]["unknown"] = "неизвестно";

$_t["ru"]["You have been successfully logged out."] = "Сеанс успешно завершен";

$_t["ru"]["You must log in to view user information."] = "Вы должны представиться для прос�мотра информации о пользователе.";

$_t["ru"]["License"] = "Лицензия";

$_t["ru"]["Could not retrieve information for the specified user."] = "Невозможно получить информацию об указанном пользователе.";

$_t["ru"]["You do not have permission to edit this account."] = "Вы не имеете прав для редактирования этой учетной записи.";

$_t["ru"]["Use this form to search existing accounts."] = "Используйте эту форму для поиска существующи�х учетных записей.";

$_t["ru"]["All"] = "Все";

$_t["ru"]["Use this form to create an account."] = "Исполь�щ�зуйте эту� форму для создания учетной записи.";

$_t["ru"]["Use this form to update your account."] = "Используйте эту форму для �изменения вашей учетной записи.";

$_t["ru"]["You are not allowed to access this area."] = "В�ДосДоступ сюда вам запрещен.";

$_t["ru"]["Unsafe"] = "Небезопаный";

?>