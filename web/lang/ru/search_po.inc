<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["None of the selected packages could be deleted."] = "Ниодин выбранный пакет не может быть удален.";

$_t["ru"]["Your votes have been removed from the selected packages."] = "Ваш голос убран с выбранного пакета.";

$_t["ru"]["Couldn't flag package safe."] = "Невозможно пометить пакет как безопасный.";

$_t["ru"]["You did not select any packages to un-vote for."] = "Вы не выбрали ниодного пакета для снятия голоса.";

$_t["ru"]["The selected packages have been unflagged."] = "С выбранных пакетов пометка снята.";

$_t["ru"]["You did not select any packages to adopt."] = "Вы не выбрали ниодного пакета для усыновления.";

$_t["ru"]["You must be logged in before you can flag packages."] = "Вы должны войти прежде чем расставлять флажки на пакеты.";

$_t["ru"]["You must be logged in before you can get notifications on comments."] = "Вы должны войти прежде чем получать извещения о коментариях.";

$_t["ru"]["You must be logged in before you can vote for packages."] = "Вы должны войти прежде чем голосовать.";

$_t["ru"]["The selected packages have been flagged out-of-date."] = "Выбраные пакеты помечены как устаревшие.";

$_t["ru"]["The selected packages have been deleted."] = "Выбраные пакеты удалены.";

$_t["ru"]["You did not select any packages to vote for."] = "Вы не выбрали ниодного пакета для голосования.";

$_t["ru"]["You must be logged in before you can disown packages."] = "Вы должны войти прежде чем бросать пакеты.";

$_t["ru"]["Error trying to retrieve package details."] = "Ошибка получения информации о пакете.";

$_t["ru"]["The selected packages have been adopted."] = "Выбраные пакеты усыновлены.";

$_t["ru"]["You have been removed from the comment notification list."] = "Вы удалены из списка получателей извещений.";

$_t["ru"]["Your votes have been cast for the selected packages."] = "Вы проголосовали за выбранные пакеты.";

$_t["ru"]["The selected packages have been unflagged safe."] = "С выбраных пакетов снят флаг Безопасно.";

$_t["ru"]["You must be logged in before you can cancel notification on comments."] = "Вы должны войти прежде чем отменять извещения о коментариях.";

$_t["ru"]["You must be logged in before you can adopt packages."] = "Вы должны войти прежде чем усыновлять пакеты.";

$_t["ru"]["You have been added to the comment notification list."] = "Вы добавлены в список получателей извещений о коментариях.";

$_t["ru"]["You did not select any packages to disown."] = "Вы не выбрали ниодного пакета чтобы бросить.";

$_t["ru"]["You must be logged in before you can un-vote for packages."] = "Вы должны войти прежде чем снимать голос с пакета.";

$_t["ru"]["You must be logged in before you can unflag packages."] = "Вы должны войти прежде чем снимать флажки.";

$_t["ru"]["You did not select any packages to unflag."] = "Вы не выбрали ниодного пакета для снятия пометки.";

$_t["ru"]["Couldn't unflag package safe."] = "Нельзя безопасно снять флажок.";

$_t["ru"]["You did not select any packages to delete."] = "Вы не выбрали ниодного пакета для удаления.";

$_t["ru"]["Couldn't add to notification list."] = "Невозможно добавить в список получателей извещений.";

$_t["ru"]["You did not select any packages to flag."] = "Вы не выбрали ниодного пакета для пометки.";

$_t["ru"]["The selected packages have been disowned."] = "Выбранные пакеты брошены.";

$_t["ru"]["The selected packages have been flagged safe."] = "Выбранные пакеты помечены как безопасные.";

$_t["ru"]["Couldn't remove from notification list."] = "Невозможно удалить из списка получателей извещений.";

?>