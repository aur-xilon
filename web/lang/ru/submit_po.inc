<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["Missing build function in PKGBUILD."] = "Отсутствует функция build в PKGBUILD.";

$_t["ru"]["Could not change directory to %s."] = "Невозможно сменить директорию в %s.";

$_t["ru"]["No"] = "Нет";

$_t["ru"]["Missing pkgdesc variable in PKGBUILD."] = "Отсутствует переменная pkgdesc в PKGBUILD.";

$_t["ru"]["Error trying to upload file - please try again."] = "Ошибка загрузки файла - попробуйте еще раз.";

$_t["ru"]["Error exec'ing the mv command."] = "Ошибка выполнения команды mv.";

$_t["ru"]["You must create an account before you can upload packages."] = "Вы должны создать учетную запись для загрузки пакетов.";

$_t["ru"]["Package upload successful."] = "Пакет успешно загружен.";

$_t["ru"]["Overwrite existing package?"] = "Перезаписать существующий пакет?";

$_t["ru"]["Binary packages and filelists are not allowed for upload."] = "Бинарные пакеты и списки файлов запрещены для загрузки.";

$_t["ru"]["You did not specify a package name."] = "Вы не указали имя пакета.";

$_t["ru"]["Error trying to unpack upload - PKGBUILD does not exist."] = "Ошибка во время распаковки - PKGBUILD отсутствует.";

$_t["ru"]["Could not create incoming directory: %s."] = "Невозможно создать каталог : %s.";

$_t["ru"]["Upload package file"] = "Файл пакета";

$_t["ru"]["Package Location"] = "Местонахождение пакета.";

$_t["ru"]["Missing url variable in PKGBUILD."] = "Отсутствует переменная url в PKGBUILD.";

$_t["ru"]["Package names do not match."] = "Имена пакета не совпадают.";

$_t["ru"]["Package Category"] = "Категория пакета.";

$_t["ru"]["Could not change to directory %s."] = "Невозможно сменить каталог на %s.";

$_t["ru"]["You did not tag the 'overwrite' checkbox."] = "Вы не установили переключатель 'overwrite'.";

$_t["ru"]["Invalid name: only lowercase letters are allowed."] = "Неверное имя: только нижний регистр допустим.";

$_t["ru"]["Missing pkgver variable in PKGBUILD."] = "Отсутствует переменная pkgver в PKGBUILD.";

$_t["ru"]["Package name"] = "Имя пакета.";

$_t["ru"]["Upload"] = "Загрузить";

$_t["ru"]["Missing md5sums variable in PKGBUILD."] = "Отсутствует переменная md5sums в PKGBUILD.";

$_t["ru"]["Missing pkgrel variable in PKGBUILD."] = " Отсутствует переменная pkgrel в PKGBUILD.";

$_t["ru"]["Missing pkgname variable in PKGBUILD."] = "Отсутствует переменная pkgname в PKGBUILD.";

$_t["ru"]["Error - No file uploaded"] = "Ошибка - нет загруженного файла";

$_t["ru"]["Package URL is missing a protocol (ie. http:// ,ftp://)"] = "Не указан протокол в URL (т.е.  http:// ,ftp://)";

$_t["ru"]["You are not allowed to overwrite the %h%s%h package."] = "Вы не имеете доступа для перезаписи пакета %h%s%h.";

$_t["ru"]["Select Location"] = "Выберите местонахождение";

$_t["ru"]["Select Category"] = "Выберите категорию";

$_t["ru"]["Comment"] = "Коментарий";

$_t["ru"]["Could not create directory %s."] = "Нельзя создать директорию %s.";

$_t["ru"]["Unknown file format for uploaded file."] = "Неизвестный формат загруженного файла.";

$_t["ru"]["Missing source variable in PKGBUILD."] = "Отсутствует переменная source  в PKGBUILD.";

$_t["ru"]["Could not re-tar"] = "Невозможно переархивировать";

$_t["ru"]["Sorry, uploads are not permitted by this server."] = "Извините, загрузки не разрешены этим сервером.";

$_t["ru"]["You must supply a comment for this upload/change."] = "Вы должны ввести коментарий к загрузке/изменению.";

$_t["ru"]["Yes"] = "Да";

$_t["ru"]["Missing license variable in PKGBUILD."] = "Не указана лицензия в PKGBUILD";

$_t["ru"]["Missing arch variable in PKGBUILD."] = "Не указана архитектура в PKGBUILD";

?>