<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["Select your language here: %h%s%h, %h%s%h, %h%s%h, %h%s%h."] = "Выберите ваш язык здесь: %h%s%h, %h%s%h, %h%s%h, %h%s%h.";

$_t["ru"]["Hello, world!"] = "Всем привет!";

$_t["ru"]["Hello, again!"] = "Привет еще раз!";

$_t["ru"]["My current language tag is: '%s'."] = "Мой текущий язык: '%s'.";

?>