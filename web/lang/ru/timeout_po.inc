<?php
# Russian (Русский) translation
# Translator: Sergej Pupykin <ps@lx-ltd.ru>

include_once("translator.inc");
global $_t;

$_t["ru"]["Click on the Home link above to log in."] = "Щелкните по ссылке Домой вверху, чтобы представиться.";

$_t["ru"]["Your session has timed out.  You must log in again."] = "Время вашей сессии истекло. Вам нужно представиться снова.";

?>