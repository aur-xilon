<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/search_po.inc");

include_once("pl/search_po.inc");

include_once("it/search_po.inc");

include_once("ca/search_po.inc");

include_once("pt/search_po.inc");

include_once("es/search_po.inc");

include_once("de/search_po.inc");

include_once("ru/search_po.inc");

include_once("fr/search_po.inc");

?>