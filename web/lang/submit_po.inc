<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/submit_po.inc");

include_once("pl/submit_po.inc");

include_once("it/submit_po.inc");

include_once("ca/submit_po.inc");

include_once("pt/submit_po.inc");

include_once("es/submit_po.inc");

include_once("de/submit_po.inc");

include_once("ru/submit_po.inc");

include_once("fr/submit_po.inc");

?>