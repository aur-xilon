<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/template_po.inc");

include_once("pl/template_po.inc");

include_once("it/template_po.inc");

include_once("ca/template_po.inc");

include_once("pt/template_po.inc");

include_once("es/template_po.inc");

include_once("de/template_po.inc");

include_once("ru/template_po.inc");

include_once("fr/template_po.inc");

?>