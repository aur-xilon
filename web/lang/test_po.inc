<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/test_po.inc");

include_once("pl/test_po.inc");

include_once("it/test_po.inc");

include_once("ca/test_po.inc");

include_once("pt/test_po.inc");

include_once("es/test_po.inc");

include_once("de/test_po.inc");

include_once("ru/test_po.inc");

include_once("fr/test_po.inc");

?>