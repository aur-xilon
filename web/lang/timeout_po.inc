<?php
# INSTRUCTIONS TO TRANSLATORS
#
# This file contains the i18n translations for a subset of the
# Arch Linux User-community Repository (AUR).  This is a PHP
# script, and as such, you MUST pay great attention to the syntax.
# If your text contains any double-quotes ("), you MUST escape
# them with the backslash character (\).
#

include_once("translator.inc");
global $_t;

include_once("en/timeout_po.inc");

include_once("pl/timeout_po.inc");

include_once("it/timeout_po.inc");

include_once("ca/timeout_po.inc");

include_once("pt/timeout_po.inc");

include_once("es/timeout_po.inc");

include_once("de/timeout_po.inc");

include_once("ru/timeout_po.inc");

include_once("fr/timeout_po.inc");

?>